import { NgModule } from '@angular/core';
import { Routes, RouterModule, ActivatedRouteSnapshot } from '@angular/router';
const routes: Routes = [
  {
    path: 'account',
    loadChildren: './account-info/account-info.module#AccountInfoModule'
  },
  {
    path: 'product',
    loadChildren: () => import('./product/product.module').then(mod => mod.ProductModule)
  },
  {
    path: 'cart',
    loadChildren: './cart/cart.module#CartModule'
  },
  {
    path: 'wishlist',
    loadChildren: './wishlist/wishlist.module#WishlistModule'
  },
  {
    path: 'home',
    loadChildren: './home/home.module#HomeModule'
  },
  {
    path: 'brand',
    loadChildren: './brand/brand.module#BrandModule'
  },
  {
    path: 'info',
    loadChildren: './info/info.module#InfoModule'
  },
 {
    path: '',
    redirectTo: 'home/welcome',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'top' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
