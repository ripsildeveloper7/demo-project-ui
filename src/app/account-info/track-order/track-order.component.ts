import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AccountService } from '../account.service';

@Component({
  selector: 'app-track-order',
  templateUrl: './track-order.component.html',
  styleUrls: ['./track-order.component.css']
})
export class TrackOrderComponent implements OnInit {
  awbNo;
  orderId;
  awbDetails = [{ 'AwbNo' :'7770014065',
  "BookingDate":"18-Nov-2016",
  "Consignee":"CHESSA SUTARIA",
  "ConsigneeAddress":"102 SILVER SPRINGS APARTMENT 1",
  "Consignor":"MAIL BOX MART",
  "Destination":"INDIA",
  "Error":null,
  "Message":"",
  "Origin":"USA",
  "Packages":"1",
  "Status":"IN TRANSIT",
  "TrackingMovement":[
    {"Activities":"Shipment Booked at NEW YORK","Date":"19-Nov2016","RcRemarks":"","Time":"12:54"},
    {"Activities":"Shipment Forwarded from NEW YORK to MUMBAI","Date":"19-Nov2016","RcRemarks":"","Time":"14:38"},
    {"Activities":"Shipment Received at MUMBAI from NEW YORK","Date":"23-Nov2016","RcRemarks":"","Time":"10:50"},
    {"Activities":"Shipment Forwarded from MUMBAI to BOMBINO AHMADABAD","Date":"25Nov-2016","RcRemarks":"","Time":"00:07"},
    {"Activities":"Shipment Received at BOMBINO AHMADABAD from MUMBAI","Date":"25-Nov-2016","RcRemarks":"","Time":"15:05"}],
    "Weight":"12.000"}];
  constructor(private accountService: AccountService, private router: Router, private route: ActivatedRoute) {
    this.awbNo = this.route.snapshot.params.id;
    this.orderId = this.route.snapshot.params.orderid;
   }

ngOnInit() {
this.trackOrders();
}
trackOrders() {
  console.log('awb no', this.awbNo);
  console.log(this.orderId, 'order Ids');
  this.accountService.trackAWB(this.awbNo).subscribe(data => {
    console.log(data);
  }, err => {
    console.log('err', err);
  });
}
}
