import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RegModel } from './registration/registration.model';
import { CardDetailModel } from './card-details/cardDetails.model';
import { Cart } from './../shared/model/cart.model';
import { Product } from './../shared/model/product.model';
import { Order } from './../shared/model/order.model';
import { AddressModel } from './address/address.model';
import { ProfileModel } from './profile/profile.model';
import { SignIn } from './signin/signIn.model';
import { AppSetting } from './../config/appSetting';
import { Observable, from } from 'rxjs';
import { Review } from './review-product/review.mode';
@Injectable({
  providedIn: 'root'
})
export class AccountService {
  serviceUrl = AppSetting.customerServiceUrl;
  productServiceUrl = AppSetting.productServiceUrl;
  commerceServiceUrl: string = AppSetting.commerceOrderServiceUrl;
  constructor(private http: HttpClient) { }

  getregForm(holder): Observable<RegModel> {
    const urlway = this.serviceUrl + 'createcustomer';
    return this.http.post<RegModel>(urlway, holder);
  }

  getcardDetails(cardHolder, userId): Observable<CardDetailModel> {
    const urlcard = this.serviceUrl + 'updatecarddetails/' + userId;
    return this.http.put<CardDetailModel>(urlcard, cardHolder);
  }
  getaddressDetails(addressHolder, userId): Observable<AddressModel> {
    const urladdress = this.serviceUrl + 'updateaddressdetails/' + userId;
    return this.http.put<AddressModel>(urladdress, addressHolder);
  }

  addToCart(cart): Observable<Cart> {
    const cartUrl = 'cart';
    const url: string = this.productServiceUrl + cartUrl;
    return this.http.post<Cart>(url, cart);
  }

  getprofileDetails(profileHolder, userId): Observable<RegModel[]> {
    const urlprofile = this.serviceUrl + 'updateprofiledetails/' + userId;
    return this.http.put<RegModel[]>(urlprofile, profileHolder);
  }
  getCustomerDetails(userId): Observable<RegModel> {
    const urlprofile = this.serviceUrl + 'getcustomerprofile/' + userId;
    return this.http.get<RegModel>(urlprofile);
  }
  customerAddressDelete(userId, addressId): Observable<RegModel> {
    const urlprofile = this.serviceUrl + 'deletecustomeraddress/' + userId + '/delete/' + addressId;
    return this.http.delete<RegModel>(urlprofile);
  }
  customerAddressUpdate(userId, addressId, updateDetails): Observable<any> {
    const urlprofile = this.serviceUrl + 'editcustomeraddress/' + userId + '/update/' + addressId;
    return this.http.put<any>(urlprofile, updateDetails);
  }
  customerCardDelete(userId, cardId): Observable<RegModel> {
    const urlprofile = this.serviceUrl + 'deletecustomercard/' + userId + '/delete/' + cardId;
    return this.http.delete<RegModel>(urlprofile);
  }
  signIn(data: SignIn): Observable<any> {
    const signInurl = 'customerlogin';
    const url: string = this.serviceUrl + signInurl;
    return this.http.post<SignIn>(url, data);
  }
  getCustomerOrderDetails(userId): Observable<any> {
    const cartUrl = 'customersorders/';
    const url: string = this.commerceServiceUrl + cartUrl + userId;
    return this.http.get<any>(url);
  }
  getSingleProductsWithBrand(id): Observable<any> {
    const categoryUrl = 'productsingle/';
    const url: string = this.productServiceUrl + categoryUrl + id;
    return this.http.get<Product>(url);
  }
  createReview(data): Observable<any> {
    const pathUrl = 'createproductreview';
    const url: string = this.productServiceUrl + pathUrl;
    return this.http.post<Review>(url, data);
  }
  getReviewForVerification(): Observable<any> {
    const productUrl = 'getreviewforverification';
    const url: string = this.productServiceUrl + productUrl;
    return this.http.get<Review>(url);
  }
  shoppingUser(userId) {
    const shoppingUrl = 'findcart/';
    const url: string = this.productServiceUrl + shoppingUrl + userId;
    return this.http.get<Cart>(url);
  }
  updatePassword(holder, id): Observable<RegModel> {
    const urlway = this.serviceUrl + 'resetpassword/' + id;
    return this.http.post<RegModel>(urlway, holder);
  }

  setPassword(holder, id): Observable<RegModel> {
    const urlway = this.serviceUrl + 'setpassword/' + id;
    return this.http.post<RegModel>(urlway, holder);
  }
  getMeasurementByUser(id): Observable<any> {
    const urlway = this.serviceUrl + 'getmeasurementforuser/' + id;
    return this.http.get<any>(urlway);
  }
  getSelctedMeasurementByUser(id): Observable<any> {
    const productUrl = 'getselectedmeasurement/';
    const url: string = this.serviceUrl + productUrl + id;
    return this.http.get<any>(url);
  }
  deleteMeasurement(id): Observable<any> {
    const productUrl = 'deletemeasurement/';
    const url: string = this.serviceUrl + productUrl + id;
    return this.http.delete<any>(url);
  }
/*   addToCartCheckout(cart): Observable<Cart> {
    const cartUrl = 'cart/';
    const url: string = this.serviceUrl + cartUrl;
    return this.http.post<Cart>(url, cart);
  }


  addToCartDecrement(cart): Observable<Cart> {
    const cartUrl = 'findcartproduct/';
    const url: string = this.serviceUrl + cartUrl;
    return this.http.post<Cart>(url, cart);
  }
  addToCart(cart): Observable<Cart> {
    const cartUrl = 'cart';
    const url: string = this.serviceUrl + cartUrl;
    return this.http.post<Cart>(url, cart);
  }
 */
 /*  deleteToCart(userid, proId) {
    const cartUrl = 'deletecart/';
    const productUrl = '/itemId/';
    const url: string = this.serviceUrl + cartUrl + userid + productUrl + proId;
    return this.http.delete<Cart>(url);
  }
  deleteAllCart(carId) {
    const cartUrl = 'deletecart/';
    const url: string = this.serviceUrl + cartUrl + carId;
    return this.http.delete<Cart>(url);
  }
  shoppingUser(userId) {
    const shoppingUrl = 'findcart/';
    const url: string = this.serviceUrl + shoppingUrl + userId;
    return this.http.get<Cart>(url);
  }
  shoppingCart() {
    const shoppingUrl = 'shopping/';
    const url: string = this.serviceUrl + shoppingUrl;
    return this.http.get<Product>(url);
  }
  addToCartMinus(cart) {
    const cartUrl = 'cart/';
    const productUrl = '/decproduct/';
    const url: string = this.serviceUrl + cartUrl + cart.userId + productUrl + cart.product.productId;
    return this.http.put<Product>(url, cart);
  }
  confirmOrder(order) {
    const orderUrl = 'order/';
    const url: string = this.serviceUrl + orderUrl;
    return this.http.put<Order>(url, order);
  }
  confirmQtyOrder(order) {
    const orderUrl = 'updateqtyproduct/';
    const url: string = this.serviceUrl + orderUrl + order.orderId;
    return this.http.put<Order>(url, order);
  }
 */
// track the order

trackAWB(awbNo) {
  const shoppingUrl = 'http://api.bombinoexp.in/bombinoapi.svc/Tracking?AccountId=ECOM0164&UserId=infobanasuri@gmail.com&Password=123&AwbNo='+awbNo  ;
  const url: string =  shoppingUrl;
  return this.http.get<Cart>(url);
}

}
