import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormControl, FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AccountService } from './../account.service';
import { RegModel } from './registration.model';
import { AddressModel } from '../address/address.model';
/* import { mobileNumber } from './../../shared/validation'; */
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit, AfterViewInit {
  hide = true;
  holder: RegModel;
  regForm: FormGroup;
  showPassword = false;
  showMobileView = false;
  emailId = new FormControl('', [Validators.required, Validators.email]);



  constructor(private fb: FormBuilder, private accountService: AccountService, private router: Router) { }

  ngOnInit() {
    this.regForm = this.fb.group({
      emailId: ['', Validators.email],
      password: ['', [Validators.required, Validators.minLength(6)]],
      mobileNumber: ['']

    });
    if (window.screen.width > 900) {
      this.showMobileView = false;
    } else {
      this.showMobileView = true;
    }
  }
  ngAfterViewInit() {
    this.getWindowSize();
  }
  getWindowSize() {
    if (window.screen.width > 900) {
      this.showMobileView = false;
    } else {
      this.showMobileView = true;
    }
  }




  onSubmit() {
    this.holder = new RegModel();
    this.holder.emailId = this.regForm.controls.emailId.value;
    this.holder.mobileNumber = this.regForm.controls.mobileNumber.value;
    this.holder.password = this.regForm.controls.password.value;
    this.accountService.getregForm(this.holder).subscribe(data => {
      if (data.result) {
        this.holder = data;
      } else {
        sessionStorage.setItem('login', 'true');
        sessionStorage.setItem('userId', data.userId);
        this.router.navigate(['account/profile']);
      }
    }, error => {
      console.log(error);
    });
    /* console.log(this.regForm); */


  }
  getReset() {
    this.regForm.reset();
  }
  checkPassword() {
    this.showPassword = !this.showPassword;
  }
}
