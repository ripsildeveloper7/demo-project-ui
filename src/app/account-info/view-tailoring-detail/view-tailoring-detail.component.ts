import { Component, OnInit, Inject, Optional, Input } from '@angular/core';
/* import {SalesService} from '../sales.service';
import {Order} from '../orders/order.model'; */
import { Route, ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import {AppSetting} from '../../config/appSetting';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';


@Component({
  selector: 'app-view-tailoring-detail',
  templateUrl: './view-tailoring-detail.component.html',
  styleUrls: ['./view-tailoring-detail.component.css']
})
export class ViewTailoringDetailComponent implements OnInit {

  constructor(@Optional() @Inject(MAT_DIALOG_DATA) public data: any,
              public dialogRef: MatDialogRef<ViewTailoringDetailComponent>) { }

  ngOnInit() {
  }
  close() {
    if (this.dialogRef) {
      this.dialogRef.close();
    }
  }
}
