import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AccountService } from './../account.service';
import { RegModel } from './../registration/registration.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-view-measurement',
  templateUrl: './view-measurement.component.html',
  styleUrls: ['./view-measurement.component.css']
})
export class ViewMeasurementComponent implements OnInit {
  userId: any;
  holder;
  showNoData: boolean;

  constructor(private accountService: AccountService, private router: Router) {
    this.userId = sessionStorage.getItem('userId');
    this.getMeasurement();
  }

  ngOnInit() {
  }
  getMeasurement() {
    this.accountService.getMeasurementByUser(this.userId).subscribe(data => {
      this.holder = data;
      console.log(data);
      if (this.holder.length === 0) {
        this.showNoData = true;
      } else {
        this.showNoData = false;
      }
    }, error => {
      console.log(error);
    });
  }
  getView(id) {
    this.router.navigate(['account/viewSingleMeasurement/', id]);
  }
  deleteMeasurement(id) {
    this.accountService.deleteMeasurement(id).subscribe(data => {
      this.holder = data;
    }, error => {
      console.log(error);
    });
  }
}
