import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';

import {AccountService} from '../account.service';
import { Cart } from './../../shared/model/cart.model';
import { Product } from './../../shared/model/product.model';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
  regForm: FormGroup;
  tabItems = [{item: 'Login'}, {item: 'Registration'}];
  selectedItemTab = this.tabItems[0].item;
  constructor(private fb: FormBuilder) { }

  ngOnInit() {
this.createForm();
  }
createForm() {

}

selectedTab(tab) {
  this.selectedItemTab = tab;
}
}
