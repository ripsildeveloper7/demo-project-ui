interface AppSettingType {
    customerServiceUrl: string;
    productServiceUrl: string;
    productImageUrl: string;
    brandImageUrl: string;
    commerceOrderServiceUrl: string;
    contentServiceUrl: string;
    subCategoryImageUrl: string;
    categoryImageUrl: string;
    sizeGuideImageUrl: string;
    instagramUrl: string;
    marketingServiceUrl: string;
    categoryBannerImageUrl: string;
    measurementImageUrl: string;
    howToMeasureImageUrl: string;
}
