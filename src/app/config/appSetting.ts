import {environment} from '../../environments/environment';

export const AppSetting: AppSettingType = {
    productServiceUrl: environment.productServiceUrl,
    productImageUrl: environment.productImageUrl,
    customerServiceUrl: environment.customerServiceUrl,
    commerceOrderServiceUrl: environment.commerceOrderServiceUrl,
    contentServiceUrl: environment.contentServiceUrl,
    subCategoryImageUrl: environment.subCategoryImageUrl,
    categoryImageUrl:  environment.categoryImageUrl,
    brandImageUrl: environment.brandImageUrl,
    sizeGuideImageUrl: environment.sizeGuideImageUrl,
    instagramUrl: environment.instagramUrl,
    marketingServiceUrl: environment.marketingServiceUrl,
    categoryBannerImageUrl: environment.categoryBannerImageUrl,
    measurementImageUrl: environment.measurementImageUrl,
    howToMeasureImageUrl: environment.howToMeasureImageUrl
};
