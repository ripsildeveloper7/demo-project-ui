export class Filter {
    categoryFilter: boolean;
    subCategoryFilter: boolean;
    brandProductFilter: boolean;
    brandFilter: boolean;
    colorFilter: boolean;
    sizeFilter: boolean;
}

