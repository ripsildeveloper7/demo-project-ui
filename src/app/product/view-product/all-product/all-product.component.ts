import { Component, OnInit, OnDestroy, AfterViewInit, IterableDiffers } from '@angular/core';
import { AppSetting } from './../../../config/appSetting';
import { ProductService } from './../../../product/product.service';
import { Product } from './../../../shared/model/product.model';
import { MainCategory } from './../../../shared/model/mainCategory.model';
import { BrandModel } from './../../../shared/model/brand.model';
import { Color } from './../../../shared/model/colorSetting.model';
import { ProductSettings } from './../../../shared/model/product-setting.model';
import { Filter } from './../all-product/filter.model';
import { CommonFilter } from './../all-product/commonFilter.model';
import {
  ActivatedRoute, Router, NavigationEnd, NavigationStart,
  PRIMARY_OUTLET, RoutesRecognized, Params, ParamMap, Data, Event, ActivatedRouteSnapshot
} from '@angular/router';
import { HttpParams } from '@angular/common/http';
import { SuperCategory } from 'src/app/shared/model/superCategory.model';
import { ProductOption } from 'src/app/shared/model/product-option.model';
import { SubCategory } from 'src/app/shared/model/sub-category.model';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { Price } from '../filter/filter-options/price.model';
import {COMMA, ENTER} from '@angular/cdk/keycodes';

import {MatChipInputEvent} from '@angular/material/chips';
import { isNgTemplate } from '@angular/compiler';
import { element } from 'protractor';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { MobileFilterComponent } from '../filter/mobile-filter/mobile-filter.component';
import { FieldAttribute } from './../side-bar/filterAtribute.model';

@Component({
  selector: 'app-all-product',
  templateUrl: './all-product.component.html',
  styleUrls: ['./all-product.component.css']
})
export class AllProductComponent implements OnInit, OnDestroy, AfterViewInit {
  visible = true;
  selectable = true;
  removable = true;
  colorModel:any;
  displayAttributeList:Array<any> = [];
  addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  productImageUrl: string = AppSetting.productImageUrl;
  productModel: Product;
  productOptionModel: ProductOption;
  productCommonModel: Product[];
  childParamSubscription: Subscription;
  childQueryParamSubscription: Subscription;
  mainCategory: MainCategory;
  superCategory: SuperCategory[];
  singleSuperCategory: SuperCategory;
  subCategory: SubCategory;
  brandModel: BrandModel;
  productSettings: ProductSettings;
  color: Color[];
  colorSchema: Array<any> = [];
  nullColor: any;
  filter: Filter;
  public pageSize = 50;
  public currentPage = 0;
  public totalSize = 0;
  public array: any;
  cat: {
    catId?: string, mainCatId?: string, subCatId?: string, brandId?: string,
    collectionId?: string, promotionId?: string, recentProductId?: string, searchID?: string
  };
  promotionData: { title: string, description: string, promotion: boolean };
  recentProduct: { title: string, recent: boolean };
  breadcrumbs;
  commonCategory: any;
  mainCatId: string;
  subCatId: string;
  id: string;
  private paramSubscription: any;
  attributeModel: Array<any> = [];
  showMobileView = false;
  showMobileView1 = false;
  showMobileView2 = false;
  isDispatch = false;
  brandId: string;
  collectionId: string;
  promotionId: string;
  searchID: string;
  tagData;
  displayAttribute:any=[];

  selectedValue: any=[];
  promotionName;
  promotionDesc;
  promotion = false;
  filterOptions: Array<any> = [];
  fieldAttributePush: Array<any> = [];
  result: any;
  breadCrumbDetails: string;
  filterOptionsData: any;
  showFilter = false;
  commonResult: CommonFilter;
  navigationSubscription;
  isOpen = false;
  category: any;
  routedFullAttribute: any[];
  display='none';
  routedAttribute: any;
  constructor(private dialog: MatDialog,private productService: ProductService,  private activatedRoute: ActivatedRoute, private router: Router) {
    this.id = this.activatedRoute.snapshot.paramMap.get('catid');
    this.cat = {
      catId: this.id,
      
      /* mainCatId: this.activatedRoute.firstChild.snapshot.,
      subCatId: this.activatedRoute.firstChild.snapshot.params.get('subid'),
      brandId: this.activatedRoute.firstChild.snapshot.params.get('brandid'),
      collectionId: this.activatedRoute.firstChild.snapshot.params.get('collectionid'),
      promotionId: this.activatedRoute.firstChild.snapshot.params.get('promotionid'),
      recentProductId: this.activatedRoute.firstChild.snapshot.params.get('recentid'),
      searchID: this.activatedRoute.firstChild.snapshot.params.get('id'), */
    };
     /* router.events.subscribe((event: Event) => {
       if (event instanceof NavigationEnd) {
        
       }
     }); */
     this.activatedRoute.paramMap.subscribe((params: ParamMap) => {
      this.id = params.get('catid');
    });
    
    this.childParamSubscription = this.activatedRoute.firstChild.paramMap.subscribe(
      (params: ParamMap): void => {
        this.fieldAttributePush = new Array();
        this.commonResult = new CommonFilter(); 
        this.result = {};
        console.log(this.activatedRoute.snapshot);
        console.log(params, 'test 8')
        this.cat = {
          catId: params.get('catid') === '' ? this.id: this.id,
          mainCatId: params.get('maincatid'),
          subCatId: params.get('subid'),
          brandId: params.get('brandid'),
          collectionId: params.get('collectionid'),
          promotionId: params.get('promotionid'),
          recentProductId: params.get('recentid'),
          searchID: params.get('id'),
        };
        this.viewSingleCategory();
      });
  }
  ngOnDestroy() {
    this.childParamSubscription.unsubscribe();
  }
  ngOnInit() {
    this.activatedRoute.data.subscribe((data: Data) => {
      this.activatedRoute.children[0].paramMap.subscribe((params: ParamMap) => {
        this.cat = {
          catId: params.get('catid') === '' ? this.id: this.id ,
          mainCatId: params.get('maincatid'),
          subCatId: params.get('subid'),
          /* attributeId: params.get('attribute'),
          attributefiledId: params.get('attributefiledId') */
          brandId: params.get('brandid'),
          collectionId: params.get('collectionid'),
          promotionId: params.get('promotionid'),
          recentProductId: params.get('recentid'),
          searchID: params.get('id'),
        };
      });
  /*     console.log(this.activatedRoute.children[0].paramMap.subscribe((params: ParamMap) => {
        this.id = params.get('catid');
      }), 'new local'); */
         this.fieldAttributePush = new Array();
        this.commonResult = new CommonFilter();
      this.productModel = new Product();
      this.mainCategory = new MainCategory();
      this.singleSuperCategory = new SuperCategory();
      this.subCategory = new SubCategory();
      // console.log('chkkkkkkkkkk', data);
      this.superCategory = data.common.category;
      this.filterOptions = data.common.filterOption;
      // this.filterOptions[0].filterDispatchValue = this.filterOptions[0].filterDispatchValue.sort((a,b)=> a.day-b.day);
      //  this.filterOptions[0].filterOptionDiscountValue = this.filterOptions[0].filterOptionDiscountValue.sort((a,b)=> a.minDiscount-b.minDiscount);
      // console.log('aaa',this.filterOptions);
      
      this.attributeModel = data.common.filterAttribute.sort((a,b) => 
        a.sortOrder - b.sortOrder
      );
      let first;
      let second;
      let holder;
      if (this.attributeModel.find(e => e.fieldName === 'size')) {
        holder = this.attributeModel.find(e => e.fieldName === 'size');
        const index = holder.fieldValue.map(e => e.fieldAttributeValue).indexOf('44');
        if (index !== -1) {
        first = holder.fieldValue.slice(0, index);
        second = holder.fieldValue.slice(index);
        console.log('first', first);
        console.log('second', second);
        console.log('index', index);
        this.attributeModel.forEach( a => {
          if (a.fieldName === 'size') {
            a.fieldValue = first;
          }
        });
        const plus = {
          _id: 100,
          fieldName: 'plusSize',
          fieldType: holder.fieldType,
          fieldSetting: 'PlusSize',
          fieldValue: second
        };
        this.attributeModel.push(plus);
      }
      }
      
      // console.log('product', this.attributeModel);
     

      /* this.viewSingleCategory(); */
      /* this.commonCategory = data.common; */
      this.id = this.activatedRoute.snapshot.paramMap.get('catid');
      
     /*  this.cat = {
        catId: this.id,
        mainCatId: this.activatedRoute.firstChild.snapshot,
        subCatId: this.activatedRoute.firstChild.snapshot.paramMap.get('subid'),
        brandId: this.activatedRoute.firstChild.snapshot.paramMap.get('brandid'),
        collectionId: this.activatedRoute.firstChild.snapshot.paramMap.get('collectionid'),
        promotionId: this.activatedRoute.firstChild.snapshot.paramMap.get('promotionid'),
        recentProductId: this.activatedRoute.firstChild.snapshot.paramMap.get('recentid'),
        searchID: this.activatedRoute.firstChild.snapshot.paramMap.get('id'),
      }; */
      this.viewSingleCategory();
      this.getColor();
      /* console.log(this.commonCategory, 'test 3'); */
    });

    this.checkRoute();
    /*   setTimeout(() => {
        this.getQueryParams();
      }); */
    /*  if (this.cat.promotionId) {
       this.promotionData = {
         title: this.activatedRoute.snapshot.queryParams.name,
         description: this.activatedRoute.snapshot.queryParams.desc,
         promotion: true
       };
     } */
  }
  checkRoute() {
    this.activatedRoute.queryParamMap.subscribe((queryparams: ParamMap): void => {
   
      const data: any = Object.values({...queryparams.keys});
      const localTest = data.filter(el => el);
      /* localTest.forEach(test => {
        this.getFilterAttribute = queryparams.get(test);
        this.filterAttributepush.push({fieldName: test, fieldValue: this.getFilterAttribute});
      }) */
      this.routedFullAttribute = new Array();
      if (localTest.length > 0) {
        localTest.forEach(el => {
         
          this.routedAttribute = new FieldAttribute();
          this.routedAttribute.fieldName =  el;
          this.routedAttribute.fieldValue =  queryparams.get(el).split(',');
          this.routedFullAttribute.push(this.routedAttribute);
         });
       }
      // console.log('route attribute', this.routedFullAttribute);
      this.routedFullAttribute.forEach(route => {
        this.attributeModel.forEach(attribute => {
          if (route.fieldName === 'Size' || route.fieldName === 'Color') {
            if (attribute.fieldSetting  === route.fieldName) {
              attribute.fieldValue.forEach(field => {
                route.fieldValue.forEach(ro => {
                  if (field.fieldAttributeValue === ro) {
                    field.correct = true;
                  }
                });
              });
            } else {
              if (attribute.fieldName === 'plusSize') {
                attribute.fieldValue.forEach(field => {
                  route.fieldValue.forEach(ro => {
                    if (field.fieldAttributeValue === ro) {
                      field.correct = true;
                    }
                  });
                });
              }
            }
          } else {
            if (attribute.fieldName === route.fieldName) {
              attribute.fieldValue.forEach(field => {
                route.fieldValue.forEach(ro => {
                  if (field.fieldAttributeValue === ro) {
                    field.correct = true;
                  }
                });
              });
            }
          }
        });
       });
    });
  }

  ngAfterViewInit() {
    this.getWindowSize();
  }

  getWindowSize() {
    if (window.screen.width > 900) {
      this.showMobileView1 = false;
    } else {
      this.showMobileView1 = true;
    }
  }
  readMore() {
    this.showFilter = true;
  }
  readLess() {
    this.showFilter = false;
  }
  selectedValues(e){
    console.log(e,'values');
    this.selectedValue = [];
    this.fieldAttributePush.forEach(element =>{
      element.fieldValue.forEach(el => {
        this.selectedValue.push(el)
      });;
    })
     console.log(this.selectedValue,'selected valuesss');
     this.getColor();
   }
  toggleDropdown() {
    this.showMobileView = !this.showMobileView;
  }
  filterOptionsDataEvent(data) {
    
    this.commonResult.discount = data.discount;
    this.commonResult.dispatch = data.dispatch;
    this.commonResult.maxPrice = data.maxPrice;
    this.commonResult.minPrice = data.minPrice;
    /* this.commonResult.dispatch = data.dispatch === undefined ? {} : data.dispatch;
    this.commonResult.dis = data.dis === undefined ? {} : data.dis;
    this.commonResult.maxPrice = data.maxPrice === undefined ? {} : data.maxPrice;
    this.commonResult.minPrice = data.minPrice === undefined ? {} : data.minPrice; */
    this.result = this.result === undefined ? {} : this.result;
    var resultMerge = Object.assign(this.result, this.commonResult);
    this.displayAttribute=[];
    this.displayAttribute.push(resultMerge);
    console.log('displayAttribute',this.displayAttribute);
    this.displayAttributeList=[];
    this.displayAttributeList.push(this.displayAttribute);
    this.router.navigate([], {
      relativeTo: this.activatedRoute, queryParams:
        resultMerge
    });
    // console.log('check1',this.result,this.commonResult,resultMerge);
  }
  get sortData() {
    return this.commonResult.dispatch.sort((a, b) => a-b);
  }
  toggleDropdown1() {
    this.showMobileView2 = !this.showMobileView2;
  }
  removeFabric(fabric){
    const index = this.displayAttribute[0].fabric.indexOf(fabric);
    if (index >= -1) {
      this.displayAttribute[0].fabric.splice(index, [1]);
    }
    if(this.displayAttribute[0].fabric.length === 0){
      delete this.displayAttribute[0].fabric;
    }
    console.log(this.displayAttribute);
    this.attributeModel.forEach(element=>{
      if(element.fieldName === "fabric"){
       element.fieldValue.forEach(el => {
         if(el.fieldAttributeValue === fabric)
         el.correct = false;
       });
      }
   }) 
    this.router.navigate([], {
      relativeTo: this.activatedRoute, queryParams:
      this.displayAttribute[0]
    });
  }
  removeWorkType(workType){
    const index = this.displayAttribute[0].workType.indexOf(workType);
    if (index >= -1) {
      this.displayAttribute[0].workType.splice(index, [1]);
    }
    if(this.displayAttribute[0].workType.length === 0){
      delete this.displayAttribute[0].workType;
    }
    console.log(this.displayAttribute);
     this.attributeModel.forEach(element=>{
       if(element.fieldName === "workType"){
        element.fieldValue.forEach(el => {
          if(el.fieldAttributeValue === workType)
          el.correct = false;
        });
       }
    }) 
    this.router.navigate([], {
      relativeTo: this.activatedRoute, queryParams:
      this.displayAttribute[0]
    });
  }
  removeOccasion(occasion){
    console.log('occasion',this.displayAttribute)
    const index = this.displayAttribute[0].occasion.indexOf(occasion);
    if (index >= -1) {
      this.displayAttribute[0].occasion.splice(index, [1]);
    }
    if(this.displayAttribute[0].occasion.length === 0){
      delete this.displayAttribute[0].occasion;
    }
    console.log(this.displayAttribute);
    this.attributeModel.forEach(element=>{
      if(element.fieldName === "occasion"){
       element.fieldValue.forEach(el => {
         if(el.fieldAttributeValue === occasion)
         el.correct = false;
       });
      }
   }) 
    this.router.navigate([], {
      relativeTo: this.activatedRoute, queryParams:
      this.displayAttribute[0]
    });
  }
  removeColor(Color){
    const index = this.displayAttribute[0].Color.indexOf(Color);
    if (index >= -1) {
      this.displayAttribute[0].Color.splice(index, [1]);
    }
    if(this.displayAttribute[0].Color.length === 0){
      delete this.displayAttribute[0].Color;
    }
    console.log(this.displayAttribute);
    this.attributeModel.forEach(element=>{
      if(element.fieldName === "Color"){
       element.fieldValue.forEach(el => {
         if(el.fieldAttributeValue === Color)
         el.correct = false;
       });
      }
   }) 
    this.router.navigate([], {
      relativeTo: this.activatedRoute, queryParams:
      this.displayAttribute[0]
    });
  }
  removeMaxPrice(){
    delete this.commonResult.maxPrice;
    delete this.commonResult.minPrice;
    delete this.displayAttribute[0].maxPrice;
    delete this.displayAttribute[0].minPrice;
    console.log(this.displayAttribute);
    this.router.navigate([], {
      relativeTo: this.activatedRoute, queryParams:
      this.displayAttribute[0]
    });
  }
  removeDiscount(){
   delete this.commonResult.discount;
    delete this.displayAttribute[0].discount;
    console.log(this.displayAttribute);
    this.router.navigate([], {
      relativeTo: this.activatedRoute, queryParams:
      this.displayAttribute[0]
    });
  }
  removeDispatch(){
    delete this.commonResult.dispatch;
    const index = this.displayAttribute.dispatch;
    if (index >= -1) {
      this.displayAttribute.dispatch.splice(index, [1]);
    }
     delete this.displayAttribute[0].dispatch;
    console.log('delete Dispatch',this.displayAttribute);
    this.router.navigate([], {
      
      relativeTo: this.activatedRoute, queryParams:
      this.displayAttribute[0]
    });
  }
  removeWork(workType){
    const index = this.displayAttribute[0].work.indexOf(workType);
    if (index >= -1) {
      this.displayAttribute[0].workType.splice(index, [1]);
    }
    if(this.displayAttribute[0].workType.length === 0){
      delete this.displayAttribute[0].workType;
    }
    console.log(this.displayAttribute);
    this.router.navigate([], {
      relativeTo: this.activatedRoute, queryParams:
      this.displayAttribute[0]
    });
  }
  // removeWorkType(workType){
  //   const index = this.displayAttribute[0].workType.indexOf(workType);
  //   if (index >= -1) {
  //     this.displayAttribute[0].workType.splice(index, [1]);
  //   }
  //   console.log(this.displayAttribute);
  //   this.router.navigate([], {
  //     relativeTo: this.activatedRoute, queryParams:
  //     this.displayAttribute[0]
  //   });
  // }
  remove() {
    // this.result = {}; 
    // delete this.result;
    // delete this.displayAttribute[0].fabric;
    // delete this.displayAttribute[0].workType;
    // delete this.displayAttribute[0].occasion;
    // delete this.displayAttribute[0].Color;
    // delete this.displayAttribute[0].fabric;
    delete this.displayAttribute[0];
    
    // delete this.displayAttribute;
    
    // delete this.fieldAttributePush;
    // this.displayAttribute=[];
    console.log('filter push',this.fieldAttributePush)
    this.fieldAttributePush = [];
    delete this.commonResult.discount;
    delete this.commonResult.dispatch;
    delete this.commonResult.maxPrice;
    delete this.commonResult.minPrice;
    this.attributeModel.forEach(element=>{
  
    element.fieldValue.forEach(el => {
   
    el.correct = false;
  });
  this.router.navigate([], {
       relativeTo: this.activatedRoute, queryParams:
        this.displayAttribute[0]
   });
})
  }

  getQueryParams(): void {
    this.activatedRoute.queryParamMap.subscribe((queryparams: ParamMap): void => {
      this.nullColor = queryparams.get('color');
      this.nullColor = this.nullColor === null ? [] : this.nullColor.split(',');
      this.colorSchema.forEach(item => {
        this.nullColor.find(test => {
          if (test === item.name) {
            item.nameChecked = true;
          }
        });
      });
    });
  }
 
  filterMultiAll(colorData) {
    if (colorData.color.length === 0 || colorData.size.length === 0 || colorData.price.maxPrice === undefined
      || colorData.price.minPrice === null) {
      const nullColor = colorData.color.length === 0 ? null : colorData.color.toString(',');
      const nullSize = colorData.size.length === 0 ? null : colorData.size.toString(',');
      var dataMerge = {
        color: nullColor,
        size: nullSize,
        maxPrice: colorData.price.maxPrice,
        minPrice: colorData.price.minPrice,
      }
      
      /* if(colorData.attribute.length > 0) {
        for (var i = 0; i < colorData.attribute.length; i++) {
          result[colorData.attribute[i].fieldName] = colorData.attribute[i].fieldValue;
        }
      } */
      /*  let params = new HttpParams();
       colorData.attribute.forEach((test) =>{
       params = params.append(test.type, test.correct);
       }); */
      /* console.log(params); */
      var resultMerge = Object.assign(dataMerge, colorData.attribute);
      this.router.navigate([], {
        relativeTo: this.activatedRoute,
        queryParams: resultMerge
      });
    } else {
      this.router.navigate([], {
        relativeTo: this.activatedRoute, queryParams: {
          color: colorData.color.toString(','),
          size: colorData.size.toString(','),
          maxPrice: colorData.price.maxPrice,
          minPrice: colorData.price.minPrice
        }
      });
    }
  }
  viewAllBrand() {
    this.productService.getAllBrand().subscribe(data => {
      this.brandModel = data;
      this.getAllProductOption();
    }, err => {
      console.log(err);
    });
  }

  viewFilterOptions() {
    this.productService.getFilterOptions().subscribe(data => {
      this.filterOptions = data;
    }, err => {
      console.log(err);
    });
  }
  viewAllCategory() {
    this.productService.getAllCategory().subscribe(data => {
      this.superCategory = data;
    }, err => {
      console.log(err);
    });
  }
  getAllProductOption() {
    this.productService.allProductOption().subscribe(data => {
      this.productOptionModel = data;
    }, error => {
      console.log(error);
    });
  }
  viewSingleCategory() {
    /*   if (this.cat.promotionId) {
        this.promotionData = {
          title: this.activatedRoute.snapshot.queryParams.name,
          description: this.activatedRoute.snapshot.queryParams.desc,
          promotion: true
        };
        this.recentProduct.recent = false;
        this.filter.categoryFilter = false;
        this.filter.subCategoryFilter = false;
      } else {
        this.promotionData = {
          promotion: false,
          title: 'string',
          description: 'string'
        };
      }
      if (this.cat.recentProductId) {
        this.recentProduct = {
          title: 'Recently Viewed',
          recent: true
        };
      } else {
        this.recentProduct = {
          title: 'Recently Viewed',
          recent: false
        };
      } */
      
    this.productService.getSingleCategory(this.id).subscribe(data => {
      this.singleSuperCategory = data;
      /* this.commonCategory = this.superCategory.filter(el => el._id === this.id); */
      this.commonCategory = [];
      this.category = this.superCategory;
      this.commonCategory.push(this.superCategory);
      this.commonCategory.forEach((ele) => {
          if (this.cat.catId && this.cat.mainCatId && this.cat.subCatId) {
            ele.superCatData = false;
            ele.mainCatData = false;
            ele.brandCatData = false;
            ele.collectionCatData = false;
            ele.promotionCatData = false;
            ele.subCatData = true;
          } else if (this.cat.catId && this.cat.mainCatId) {
            ele.superCatData = false;
            ele.mainCatData = true;
            ele.brandCatData = false;
            ele.collectionCatData = false;
            ele.promotionCatData = false;
            ele.subCatData = false;
          } else if (this.cat.brandId) {
            ele.brandCatData = true;
            ele.superCatData = false;
            ele.mainCatData = false;
            ele.subCatData = false;
            ele.collectionCatData = false;
            ele.promotionCatData = false;
            ele.brandId = this.cat.brandId;
          } else if (this.cat.collectionId) {
            ele.collectionCatData = true;
            ele.brandCatData = false;
            ele.superCatData = false;
            ele.mainCatData = false;
            ele.subCatData = false;
            ele.promotionCatData = false;
            ele.collectionId = this.cat.collectionId;
          } else if (this.cat.catId) {
            ele.promotionCatData = false;
            ele.superCatData = true;
            ele.mainCatData = false;
            ele.subCatData = false;
            ele.brandCatData = false;
            ele.collectionCatData = false;
          }
          this.breadCrumbDetails = ele.categoryName;
        });
      if (this.cat.mainCatId) {
        this.viewSingleMainCategory();
      } else {

      }
      this.checkDispatch();
      /* this.commonCategory = new SuperCategory();
      this.commonCategory.categoryName = this.superCategory.categoryName;
      this.commonCategory.categoryDescription = this.superCategory.categoryDescription;
      this.commonCategory = this.commonCategory; */
    }, err => {
      console.log(err);
    });
  }
  checkDispatch() {
    if (this.cat.subCatId) {
      if (this.category.mainCategory.subCategory.isDispatch === true) {
        this.isDispatch = true;
      }
    } else {
      if (this.category.isDispatch === true) {
        this.isDispatch = true;
      }
    }
    // console.log('category check', this.category);
    // console.log('dispatch', this.isDispatch);
  }
  filterAttributeMultiAll(colorData) {
    /* this.result = {};
    if (colorData.attribute.length > 0) {
      for (var i = 0; i < colorData.attribute.length; i++) {
        if (colorData.attribute[i].fieldValue.length > 0) {
          this.result[colorData.attribute[i].fieldName] = colorData.attribute[i].fieldValue.toString(',');
        }
      }
    }
    if(!this.commonResult){
      this.commonResult = new CommonFilter(); 
  } else {
    
    this.commonResult.price = this.commonResult.price === undefined ? {} : this.commonResult.price;
    this.commonResult.dis = this.commonResult.dis === undefined ? {} : this.commonResult.dis;
    this.commonResult.dispatch = this.commonResult.dispatch === undefined ? {} : this.commonResult.dispatch;
  }
    var resultMerge = Object.assign(this.commonResult, this.result);
    this.router.navigate([], {
      relativeTo: this.activatedRoute, queryParams:
        resultMerge
    });
     */
    
    this.result = {}; 
    if(colorData.attribute.length > 0) {
      for (var i = 0; i < colorData.attribute.length; i++) {
        if(colorData.attribute[i].fieldValue.length > 0){
        this.result[colorData.attribute[i].fieldName] = colorData.attribute[i].fieldValue.toString(',');
       }
      }
    }
  
    var resultMerge = Object.assign(this.result, this.commonResult);
    this.displayAttribute = [];
    if (resultMerge.plusSize) {
      if (resultMerge.Size) {
        resultMerge.Size = resultMerge.Size + ',' + resultMerge.plusSize;
        delete resultMerge.plusSize;
      } else {
        resultMerge.Size = resultMerge.plusSize;
        delete resultMerge.plusSize;
      }
    }
    console.log(resultMerge);
    this.displayAttribute.push(resultMerge);
     this.router.navigate([], {
       relativeTo: this.activatedRoute, queryParams: 
       resultMerge
     });
    this.getColor();
    //  console.log('check2',this.result,this.commonResult,resultMerge);
  }
  subCategoryAttribute()   { 
    this.productService.getSubCategoryAttribute(this.cat.subCatId).subscribe(data => {
    this.attributeModel = data;
  }, err => {
    console.log(err);
  });
}
  superCategoryAttribute() { 
      this.productService.getSuperCategoryAttribute(this.cat.catId).subscribe(data => {
      this.attributeModel = data;
      console.log('super',this.attributeModel);
    }, err => {
      console.log(err);
    });
  }
  viewAllSubCategory() {
    this.productService.getAllSubCategory(this.id).subscribe(data => {
    this.mainCategory = data;
    }, err => {
      console.log(err);
    });
  }
  viewSingleMainCategory() {
    this.productService.getAllMainCategory(this.id, this.cat.mainCatId).subscribe(data => {
      this.mainCategory = data;
    }, err => {
      console.log(err);
    });
  }
  getColor() {
    this.productService.getColors().subscribe(data => {
      this.colorModel = data;
      this.attributeModel.forEach(element => {
        if(element.fieldName === "color"){
          element.fieldValue.forEach(el => {
            this.colorModel.forEach(e => {
             if(el.fieldAttributeValue.toLowerCase() === e.colorName.toLowerCase()){
               el.colorChoose = e.colorChoose
           }
            });
          });
        }
     })
      console.log('color',this.attributeModel);
    }, err => {
      console.log(err);
    });
    
  }
  viewAllColors() {
    this.productService.getFilterColors().subscribe(data => {
      this.color = data;
      // console.log('filter colors' ,this.color);
      /* this.getQueryParams(); */
    }, err => {
      console.log(err);
    });
  }
  toggleFilter() {
    this.showMobileView = !this.showMobileView;
  }

  openDialog() {
    // const dialogConfig = new MatDialogConfig();
    // this.dialog.open(MobileFilterComponent, {
    //   panelClass: 'c1',
    //   data: []
    // });

    this.isOpen = true;
  }

   openModal(){

          this.display='block';
    
       }
       onCloseHandled(){

            this.display='none';
      
         }
}

