import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SubCategory } from './../../../shared/model/sub-category.model';
import { Filter } from './../all-product/filter.model';
import { BrandModel } from 'src/app/shared/model/brand.model';
import { FieldAttribute } from './../side-bar/filterAtribute.model';
import { Product } from './../../../shared/model/product.model';
import { ProductOption } from './../../../shared/model/product-option.model';
import { MainCategory } from 'src/app/shared/model/mainCategory.model';
import { SuperCategory } from 'src/app/shared/model/superCategory.model';
import { ActivatedRoute, Router, NavigationEnd, PRIMARY_OUTLET, RoutesRecognized, Params } from '@angular/router';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css']
})
export class SideBarComponent implements OnInit {
  @Input() subCategoryData: MainCategory;
  @Input() productModel: Product;
  @Input() attributeModel: any;
  @Input() productOption: ProductOption;
  @Input() brandModel: BrandModel;
  @Input() mainCategory: MainCategory;
  @Input() cat: any;
  @Input() superCategory: SuperCategory;
  @Input() filter: Filter;
  @Input() color: any;
  @Output() categoryFilterData = new EventEmitter<any>();
  @Output() brandFilterData = new EventEmitter<any>();
  @Output() allFilterData = new EventEmitter<any>();
  @Output() sizeFilterData = new EventEmitter<any>();
  @Output() priceFilterData = new EventEmitter<any>();
  @Output() close = new EventEmitter<any>();
  colorFormArray: Array<any> = [];
  attributeFormArray: Array<any> = [];
  sizeFormArray: Array<any> = [];
  subMainCategory: SubCategory;
  selectMinValue = 0;
  selectMaxValue = 3000;
  filterDisplayData: string;
  passAttribute:  Array<any> = [];
  fieldAttribute: FieldAttribute;
  fieldAttributePush: Array<any> = [];
  startPrice: any[] = [
    { id: 0, name:  0 },
    { id: 1, name:  500 },
    { id: 2, name: 1000 },
    { id: 3, name: 1500 },
    { id: 4, name: 2000 },
    { id: 5, name: 2500 },
    { id: 6, name: 3000 }
  ];
  endPrice: any[] = [
    { id: 0, name:  0 },
    { id: 1, name:  500 },
    { id: 2, name: 1000 },
    { id: 3, name: 1500 },
    { id: 4, name: 2000 },
    { id: 5, name: 2500 },
    { id: 6, name: 3000 }
  ];
  
  constructor(private router: Router) { }

  ngOnInit() {
  }

  onChange(color: any, isChecked: boolean) {
    const priceData =  {
      minPrice: this.selectMinValue, maxPrice: this.selectMaxValue
    };
    if (isChecked) {
      this.colorFormArray.push(color);
      this.allFilterData.emit({color: this.colorFormArray, size: this.sizeFormArray, price: priceData, attribute: this.passAttribute });
    } else {
      const index: number = this.colorFormArray.indexOf(color);
      this.colorFormArray.splice(index, 1);
      this.allFilterData.emit({color: this.colorFormArray, size: this.sizeFormArray, price: priceData, attribute: this.passAttribute });
    }
}
selectStartPrice(start) {
  this.selectMinValue = start;
  this.endPrice.forEach(ele => {
    if (ele.name <= start) {
      ele.show =  true;
    } else {
      ele.show =  false;
    }
  });
  this.selectSliderStartPrice(this.selectMinValue);
}
selectEndPrice(end) {
  this.selectMaxValue = end;
  this.startPrice.forEach(ele => {
    if (end <= ele.name) {
      ele.show =  true;
    } else {
      ele.show =  false;
    }
  });
  this.selectSliderEndPrice(this.selectMaxValue);
}
selectSliderStartPrice(sstart) {
  this.selectMinValue = sstart;
  this.endPrice.forEach(ele => {
    if (ele.name <= sstart) {
      ele.show =  true;
    } else {
      ele.show =  false;
    }
  });
  
  const priceData =  {
    minPrice: this.selectMinValue, maxPrice: this.selectMaxValue
  };
  this.allFilterData.emit({color: this.colorFormArray, size: this.sizeFormArray, price: priceData, attribute: this.passAttribute });
}
selectSliderEndPrice(send) {
  this.selectMaxValue = send;
  console.log(this.selectMaxValue,  this.selectMinValue);
  this.startPrice.forEach(ele => {
    if (send <= ele.name) {
      ele.show =  true;
    } else {
      ele.show =  false;
    }
  });
  const priceData =  {
    minPrice: this.selectMinValue, maxPrice: this.selectMaxValue
  };
  this.allFilterData.emit({color: this.colorFormArray, size: this.sizeFormArray, price: priceData, attribute: this.passAttribute });
}
onChangeSize(size: any, isChecked: boolean) {
  const priceData =  {
    minPrice: this.selectMinValue, maxPrice: this.selectMaxValue
  };
  if (isChecked) {
    this.sizeFormArray.push(size);
    this.allFilterData.emit({color: this.colorFormArray, size: this.sizeFormArray, price: priceData, attribute: this.passAttribute });
  } else {
    const index: number = this.sizeFormArray.indexOf(size);
    this.sizeFormArray.splice(index, 1);
    this.allFilterData.emit({color: this.colorFormArray, size: this.sizeFormArray, price: priceData, attribute: this.passAttribute });
  }
  this.filterDisplayData = 'filterDisplayClass';
}
onChangePrice(value) {
  const startPrice = 1;
  const endPrice = value;
  console.log(startPrice, endPrice);
}
  viewCategories(filter) {
    if (filter.categoryFilter) {
      filter.categoryFilter = false;
      filter.sizeFilter = true;
      filter.subCategoryFilter  = true;
      filter.brandFilter = true;
      filter.colorFilter = true;
    } else {
      filter.categoryFilter = true;
    }
  }
  
  viewBrand(filter) {
    if (filter.brandFilter) {
      filter.brandFilter = false;
      filter.sizeFilter = true;
      filter.subCategoryFilter  = true;
      filter.categoryFilter = true;
      filter.colorFilter = true;
    } else {
      filter.brandFilter = true;
    }
  }
  viewColor(filter) {
    if (filter.colorFilter) {
      filter.colorFilter = false;
      filter.sizeFilter = true;
      filter.subCategoryFilter  = true;
      filter.categoryFilter = true;
      filter.brandFilter = true;
    } else {
      filter.colorFilter = true;
    }
  }
  viewSize(filter) {
    if (filter.sizeFilter) {
      filter.sizeFilter = false;
      filter.colorFilter = true;
      filter.subCategoryFilter  = true;
      filter.categoryFilter = true;
      filter.brandFilter = true;
    } else {
      filter.sizeFilter = true;
    }
  }
  viewSubCategories(filter) {
    if (filter.subCategoryFilter ) {
      filter.subCategoryFilter  = false;
      filter.sizeFilter = true;
      filter.categoryFilter = true;
      filter.brandFilter = true;
      filter.colorFilter = true;
    } else {
      filter.subCategoryFilter  = true;
    }
  }
  brandFilter(brandId) {
    const urlTree = this.router.createUrlTree([], {
      queryParams: { search: brandId },
      queryParamsHandling: 'merge',
      preserveFragment: true });
    this.router.navigateByUrl(urlTree);
  }
  closeFunc(){
    this.close.emit();
  }
  
  onChangeVariant(typeId, attributeId: any, name, isChecked) {
    const priceData =  {
      minPrice: this.selectMinValue, maxPrice: this.selectMaxValue
    };
  this.fieldAttribute = new FieldAttribute();
   const filter: any = this.attributeModel.find(element => element._id === typeId);
   filter.fieldValue.forEach(item => {
      if(item._id === attributeId) {
         item.correct = true;
         this.fieldAttribute.fieldName = filter.fieldName;
         this.fieldAttribute.fieldValue = item.fieldAttributeValue;
       } else {
         item.correct = false;
     }});

     if(this.fieldAttributePush.length > 0) {
      this.fieldAttributePush.forEach(el => {
        if(el.fieldName === this.fieldAttribute.fieldName) {
            el.fieldValue = this.fieldAttribute.fieldValue;
        } else {
          this.fieldAttributePush.push(this.fieldAttribute);
        }
       });
     } else {
      this.fieldAttributePush.push(this.fieldAttribute);
     }
     console.log(this.fieldAttributePush);
     const obj = {};
     for (const item of this.fieldAttributePush) {
      if (!obj[item.fieldName]) {
        const element = item.fieldName;
        obj[element] = item.fieldValue;
      }
    }
    /* this.passAttribute = this.attributeModel.map(function(e) {
      return e;
  }).filter(function(x) {
      return typeof x.correct !== 'undefined';
  }); */
  this.allFilterData.emit({color: this.colorFormArray, size: this.sizeFormArray, price: priceData, attribute: obj });
}
  /* colorFilter(color, productModel) {
    const product = productModel.filter(data => data.color === color);
  } */
}
