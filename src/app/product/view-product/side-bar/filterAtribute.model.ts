
export class FieldAttribute {
    _id: string;
    fieldName: string;
    fieldValue: Array<any>;
    fieldSetting: string;
  }