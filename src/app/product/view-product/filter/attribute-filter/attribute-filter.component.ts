import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SubCategory } from './../../../../shared/model/sub-category.model';
import { Filter } from './../../all-product/filter.model';
import { BrandModel } from 'src/app/shared/model/brand.model';
import { FieldAttribute } from './../../side-bar/filterAtribute.model';
import { Product } from './../../../../shared/model/product.model';
import { ProductOption } from './../../../../shared/model/product-option.model';
import { MainCategory } from 'src/app/shared/model/mainCategory.model';
import { SuperCategory } from 'src/app/shared/model/superCategory.model';
import { ProductService } from '../../../../product/product.service';

@Component({
  selector: 'app-attribute-filter',
  templateUrl: './attribute-filter.component.html',
  styleUrls: ['./attribute-filter.component.css']
})
export class AttributeFilterComponent implements OnInit {
  
  @Input() productModel: Product;
  @Input() attributeModel: any;
  @Input() filter: Filter;
  @Output() attributeFilterData = new EventEmitter<any>();
  colorFormArray: Array<any> = [];
  attributeFormArray: Array<any> = [];
  sizeFormArray: Array<any> = [];
  subMainCategory: SubCategory;
  selectMinValue = 0;
  colorModel:any;
  selectMaxValue = 3000;
  filterDisplayData: string;
  passAttribute:  Array<any> = [];
  fieldAttribute: FieldAttribute;
  @Input() fieldAttributePush: Array<any>;
  openDiv = false;
  constructor(private productService: ProductService) { }

  ngOnInit() {
    
  }

  onChangeVariant(typeId, attributeId: any, isChecked) {
    const filter: any = this.attributeModel.find(element => element._id === typeId);
    filter.fieldValue.forEach(item => {
      if(item._id === attributeId) {
         
         this.fieldAttribute = new FieldAttribute();
         if(filter.fieldSetting === 'Size' || filter.fieldSetting === 'Color'){
          item.correct = isChecked;
            this.fieldAttribute.fieldName = filter.fieldSetting;
            filter.fieldName = filter.fieldSetting;
         } else {
          item.correct = isChecked;
          this.fieldAttribute.fieldName = filter.fieldName;
         }
          this.fieldAttribute.fieldValue = filter.fieldValue.filter(att => att.correct === true).map(test => test.fieldAttributeValue);
         this.attributeCheck(filter, this.fieldAttribute);
       } });
      
    
     /* 
     const obj = {};
     for (const item of this.fieldAttributePush) {
      if (!obj[item.fieldName]) {
        const element = item.fieldName;
        obj[element] = item.fieldValue;
      }
    } */
    this.getColor();
    console.log('attpush', this.fieldAttributePush);
    this.attributeFilterData.emit({attribute: this.fieldAttributePush });
}
getColor() {
  this.productService.getColors().subscribe(data => {
    this.colorModel = data;
    console.log('color',this.colorModel);
  }, err => {
    console.log(err);
  });
  
}
openAccordian(openAccordian, select) {
  openAccordian.forEach(et=> {
    if(et.openDiv){
      et.openDiv = false;
    } else if(et._id === select) {
      et.openDiv = true;
    }
  })
  
}
 attributeCheck(filter, fieldAttribute){
      
      if(this.fieldAttributePush.length > 0) {
        const test = this.fieldAttributePush.find(el => el.fieldName === filter.fieldName)
        if(test){
          test.fieldValue = filter.fieldValue.filter(att => att.correct === true).map(tes => tes.fieldAttributeValue);  
        } else{
          this.fieldAttributePush.push(fieldAttribute);
        }
       } else {
        this.fieldAttributePush.push(fieldAttribute);
       }
    }
}