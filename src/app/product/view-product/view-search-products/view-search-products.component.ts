import { Component, OnInit } from '@angular/core';
import { AppSetting } from './../../../config/appSetting';
import { ProductService } from './../../../product/product.service';
import { MatSnackBar, MatPaginator } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { Product } from './../../../shared/model/product.model';
import { MatIconRegistry } from '@angular/material/icon';
import { FieldAttribute } from './../side-bar/filterAtribute.model';
import { SearchModel } from '../../../shared/model/search.model';
import { ActivatedRoute, Router, NavigationEnd, PRIMARY_OUTLET, RoutesRecognized, Params, ParamMap, Data } from '@angular/router';
@Component({
  selector: 'app-view-search-products',
  templateUrl: './view-search-products.component.html',
  styleUrls: ['./view-search-products.component.css']
})
export class ViewSearchProductsComponent implements OnInit {
  productModel:any;
  showMobileView: boolean;
  imageLoader: boolean;
  noOfPages: any;
  public array: any;
  filterAttributepush: Array<any> = [];
  product: Product;
  totalPages:any;
  catId: string;
  public totalSize = 0;
  productCommonModel: Product[];
  mainCatId: string;
  subCatId: string;
  searchId: string;
  attributeId: any;
  fieldAttribute: FieldAttribute;
  attributefieldId: any;
  queryParams:{
    pageNo:number,
    
  }
  // fieldAttribute: any;

  constructor(private productService: ProductService,  private route: ActivatedRoute,
    private snackBar: MatSnackBar,private activatedRoute: ActivatedRoute,private router: Router,
    iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
      iconRegistry.addSvgIcon(
        'hearts',
        sanitizer.bypassSecurityTrustResourceUrl('assets/images/hearts.svg'));
        this.activatedRoute.parent.paramMap.subscribe((params: ParamMap) => {
          this.catId = params.get('catid');
        });
      this.activatedRoute.paramMap.subscribe((params: ParamMap) => {
        this.subCatId = params.get('subid');
         console.log('searchID',params.get('searchID'));
        this.searchId =params.get('searchID');
        this.mainCatId = params.get('maincatid');
        this.attributeId = params.get('attributeId');
        this.attributefieldId = params.get('attributefieldId');
      });
    }
  ngOnInit() {

    // const value = route.paramMap.get('searchID');
    this.activatedRoute.queryParamMap.subscribe((queryparams: ParamMap): void => {
     
      const data: any = Object.values({...queryparams.keys});
      const localTest = data.filter(el => el);
  
      this.filterAttributepush = new Array();
      if (localTest.length > 0) {
        localTest.forEach(el => {
         
          this.fieldAttribute = new FieldAttribute();
          this.fieldAttribute.fieldName =  el;
          this.fieldAttribute.fieldValue =  queryparams.get(el).split(',');
          this.filterAttributepush.push(this.fieldAttribute);
         });
       }
       console.log('filterattributepush',this.filterAttributepush);
  
      if (this.filterAttributepush.length === 0) {
        this.activatedRoute.data.subscribe((data: Data) => {
          // this.productModel = data.product;
         
          this.productModel = data.product[0].data;
          this.totalPages = data.product[0].metadata[0].total;
          console.log('total pages',this.totalPages);
          var totalData =  Math.ceil(this.totalPages / 30);
          this.noOfPages =[];
          console.log('product model',this.productModel);
          for(let i = 1; i <= totalData; i++){
            this.noOfPages.push(i);  
          }
      
          this.productCommonModel = data.product[0].data;
          // this.productCommonModel = data.product;
          this.array = this.productModel;
          /* this.ilterPlainArray(this.productModel, this.fieldAttributePush) */
          this.totalSize = this.array.length;
          this.getDiscount();
          
          // this.checkLogin();
          // this.iterator();
          // this.fetchMoreItems();
        });
      } else {
        this.viewSearchFilter();
      }
    });






  //   this.activatedRoute.queryParamMap.subscribe((queryparams: ParamMap): void => {
    
  //     const data: any = Object.values({...queryparams.keys});
  //     const localTest = data.filter(el => el);
     
  //     this.filterAttributepush = new Array();
  //     if (localTest.length > 0) {
  //       localTest.forEach(el => {
         
  //         this.fieldAttribute = new FieldAttribute();
  //         this.fieldAttribute.fieldName =  el;
  //         this.fieldAttribute.fieldValue =  queryparams.get(el).split(',');
  //         this.filterAttributepush.push(this.fieldAttribute);
  //        });
  //      }
  //      if (this.filterAttributepush.length === 0) {
  //     this.activatedRoute.data.subscribe((data: Data) => {
    
  //     this.productModel = data.product[0].data;
     
  //     this.totalPages = data.product[0].metadata[0].total;
  //     console.log('total pages',this.totalPages);
  //     var totalData =  Math.ceil(this.totalPages / 30);
  //     this.noOfPages =[];
  //     console.log('product model',this.productModel);
  //     for(let i = 1; i <= totalData; i++){
  //       this.noOfPages.push(i);  
  //     }
  //     this.productCommonModel = data.product[0].data;
  //     this.getDiscount();
  //   })
  // }else{
  //   this.viewCategoryFitler();
    
  // }
   
  // })
  
   
}
    ngAfterViewInit() {
      this.getWindowSize();
    }
    getWindowSize() {
      if (window.screen.width > 900) {
        this.showMobileView = false;
      } else {
        this.showMobileView = true;
      }
    }
    onLoad(){
      this.imageLoader = false;
    }
    getPage(item){
      console.log('item',item);
 
      this.queryParams={
        pageNo:item,
       
      }
      if(item!=1){
      this.router.navigate([], {
        relativeTo: this.activatedRoute, queryParams:
        this.queryParams,
        queryParamsHandling:'merge'
      });
    }else{
      var snapshot = this.activatedRoute.snapshot;
      const params= {...snapshot.queryParams};
      delete params.pageNo
      this.router.navigate([],{ relativeTo: this.activatedRoute,queryParams:params})
     
    }
    console.log('activate route',this.activatedRoute);
  }
  getProduct(product) {
    console.log('productss',product);
    if (product.superCategoryId && product.mainCategoryId && product.subCategoryId) {
      this.router.navigate(['/product/viewsingle/', product.subCategoryId, product._id]);
    } else if (product.superCategoryId && product.mainCategoryId) {
      this.router.navigate(['/product/viewsingle/', product.mainCategoryId, product._id]);
    } else if (product.superCategoryId) {
      this.router.navigate(['/product/viewsingle/', product.superCategoryId, product._id]);
    }
  }
  getDiscount() {
    for (let i = 0; i <= this.productModel.length - 1; i++) {
      const sp = this.productModel[i].sp * 110 / 100;
      this.productModel[i].sp = sp;
      this.productModel[i].price = sp;
      const discount = 100 - this.productModel[i].discount;
      const totalPrice = this.productModel[i].sp * (100 / discount);
      const savePrice = totalPrice - this.productModel[i].sp;
      this.productModel[i].savePrice = savePrice;
      this.productModel[i].totalPrice = totalPrice;
    }
    this.checkQty();
  }
  checkQty() {
    this.productModel.forEach(element => {
      element.quantityCheck = 0;
      this.assingQtyCheck(element);
    });
    console.log(this.productModel);
  }
  assingQtyCheck(product) {
    product.child.forEach(element => {
      product.quantityCheck += element.quantity;
    });
  }
  // viewCategoryFitler() {
  //   // if (this.catId && this.mainCatId && this.subCatId) {
  //     this.viewSearchFilter();
  //   // } 
  // }
  viewSearchFilter() {
    this.product = new Product();
    // const value = route.paramMap.get('searchID');
    // this.product.superCategoryId = this.catId;
    // const store = new SearchModel();
    // store.searchID = this.searchId;
     this.product.searchId = this.searchId;
    this.product.attribute = this.filterAttributepush;
    console.log('filterpush',this.filterAttributepush);
    this.productService.getSearchProductFilter(this.product).subscribe(data => {
      this.productModel = data[0].data;
      console.log('filtervalue',data);
      this.productCommonModel = data[0].data;
      this.totalPages = data[0].metadata[0].total;
      var totalData =  Math.ceil(this.totalPages / 30);
      this.noOfPages =[];
      console.log('product model',this.productModel);
      for(let i = 1; i <= totalData; i++){
        this.noOfPages.push(i);  
      }
      this.array = this.productModel;
      this.totalSize = this.productModel.length;
      this.getDiscount();
     
    }, err => {
      console.log(err);
    });
  }

}
