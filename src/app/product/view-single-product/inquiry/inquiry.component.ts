import { Component, OnInit, Input } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { InquiryFormComponent } from '../inquiry-form/inquiry-form.component';
import { ProductService } from '../../product.service';

@Component({
  selector: 'app-inquiry',
  templateUrl: './inquiry.component.html',
  styleUrls: ['./inquiry.component.css']
})
export class InquiryComponent implements OnInit {
  @Input() note: any;
 
  // note:any;
  constructor(private dialog: MatDialog, private productService: ProductService) { 
    this.productService.getCatNote().subscribe( data =>{
      // this.note = data;
      console.log(this.note,'note');
    })
    // console.log(this.subId,this.supId);
  }

  ngOnInit() {
  }
  openDialog() {
    const dialogConfig = new MatDialogConfig();
    this.dialog.open(InquiryFormComponent, {
      panelClass: 'c5',

    });
  }
}
