import { Component, OnInit, Optional, Inject } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { AppSetting } from 'src/app/config/appSetting';

@Component({
  selector: 'app-how-to-measure',
  templateUrl: './how-to-measure.component.html',
  styleUrls: ['./how-to-measure.component.css']
})
export class HowToMeasureComponent implements OnInit {
  howToMeasureUrl: any

  constructor(private fb: FormBuilder,
              @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
              public dialogRef: MatDialogRef<HowToMeasureComponent>) { }

  ngOnInit() {
    console.log(this.data);
    this.howToMeasureUrl = AppSetting.howToMeasureImageUrl;
  }
  close() {
    this.dialogRef.close(true);
  }


}
