import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Product } from './../../../shared/model/product.model';
import { Child } from './../../../shared/model/child.model';
import { Cart } from './../../../shared/model/cart.model';
import { Color } from './../../../shared/model/colorSetting.model';
import { ProductService } from './../../../product/product.service';
import { MatSnackBar } from '@angular/material';
import { AppSetting } from './../../../config/appSetting';
import {
  ActivatedRoute, Router, NavigationEnd, NavigationStart,
  PRIMARY_OUTLET, RoutesRecognized, Params, ParamMap, Data, Event
} from '@angular/router';
import { Zoom } from './../product-zoom/zoom.model';
import { WindowScrollingService } from './../product-zoom/window-scrolling.service';
import { WishList } from './../../../shared/model/wishList.model';
import { Attribute } from './../dynamic-attribute/attribute';
import { AttributeService } from './../dynamic-attribute/attribute.service';
import { ProductAttributeComponent } from './../dynamic-attribute/product-attribute/product-attribute.component';
import { RecentProduct } from '../../../shared/model/recentProduct.model';
import { Subscription } from 'rxjs';
import { SharedService } from '../../../shared/shared.service';
import { LoadResolverService } from '../../../shared/load-resolver.service';
import { TestBed } from '@angular/core/testing';
@Component({
  selector: 'app-single-product',
  templateUrl: './single-product.component.html',
  styleUrls: ['./single-product.component.css']
})
export class SingleProductComponent implements OnInit, AfterViewInit {
  productModel: any;
  zoom: Zoom;
  wishList: WishList[];
  wish: WishList;
  productImageUrl: string = AppSetting.productImageUrl;
  productModelImage = [];
  id: string;
  sildeIndex = 0;
  color: Color;
  showRelatedProducts;
  productId;
  relatedProducts = [];
  primeHide: boolean;
  showImages: boolean;
  selectedSmallImg: any;
  selectedImg;
  cartModel: Cart;
  shopModel: any = [];
  message;
  noPrductAdd = false;
  selectedItem: Child;
  selectedSize: boolean;
  count = 1;
  reviewData: any;
  totalRating: number;
  product: Product;
  text = 'block';
  action: string;
  discountStore: any;
  tempDiscount: any;
  userId: string;
  variationType: string;
  selectedColor: string;
  attribute: Attribute[];
  attributeLength: number;
  addedCart: any;
  isCart = false;
  isLocalCart = false;
  isRecentProduct = false;
  isMeasurement = false;
  isUnstitched = true;
  service: {
    serviceId: string
  };
  heightHolder: {
    bodyHeight: string,
  };
  selectProduct;
  recentPrdouctData;
  brandProductData;
  showRecent = false;
  isSizeGuide = false;
  showLike = false;
  isBlouse = false;
  isKameez = false;
  isLehenga = false;
  isLowQuantity = false;
  isEmptyQty = false;
  breadCrumbDetails;
  categorySelected: Array<any>;
  selectedSizeCategory: Array<any>;
  selectedColorCategory: Array<any>;
  sizeColorVariantColors: Array<any> = [];
  RecentProduct: { productId: [string] };
  sizeData: any;
  howToMeasureData: any;
  showMobileView1 = false;
  alsoLike: {
    sp: number,
    catalogueName: string,
    fabric: string
  };
  alsoModel: any;
  attributeEnableData:  Array<any> = new Array();
  emptyCostInculdes:  Array<any> = new Array();
  readyToWearData: any;
  readyToWearholder: any = [];
  selectedService: any;
  setColor: {
    attributeId: string,
    color: string
  };
  attId: any[];
  isMeasurementApply = false;
  measurementData: any;
  readyToShip: any;
  isNotAvailable = false;
  isNonZero = false;
  isBodyHeight = false;
  isApplyHeight = false;
  rateModel: any;
  supId: string;
  subId: string;
  note: any;
  noteData: any;
  bodyModel: any;
  availableBodyHeight: any;
  
  constructor(private router: Router, private route: ActivatedRoute, private productService: ProductService,
              private sharedService: SharedService, private snackBar: MatSnackBar,
              private windowScrollingService: WindowScrollingService, private attributeService: AttributeService) {
                const temp = this.sharedService.getPriceRate();
  }

  ngOnInit() {
    this.route.data.subscribe((data: Data) => {
      this.breadCrumbDetails = data.product;
      this.product = data.product;
     /*  this.product.quantity = 4; */
      if (this.product.publish === false) {
        this.isNotAvailable = true;
      }
     /*  for (let i = 0; i <= this.product.child.length - 1 ; i++) {
        this.product.child[i].quantity = 0;
      } */
      this.product.child.forEach(el => {
        if (el.quantity !== 0) {
          this.isNonZero = true;
        }
      });
      console.log('product', this.product);
      this.supId = this.product.superCategoryId;
      this.subId = this.product.subCategoryId;
      this.categorySelected = data.category;
      console.log(this.supId);
     /*  if (this.product.colorId !== 'None') {
        const attId = this.categorySelected.filter(element => element.fieldSetting === 'Color').map(el => el._id);
        this.setColor = {
          attributeId: attId[0],
          color: this.product.color
        };
      } */
      this.attId = this.categorySelected.filter(element => element.fieldSetting === 'Color');
      const filterAttributeData = this.categorySelected.filter(enable=> /* (enable.fieldSetting !== 'Color') && */ (enable.fieldSetting !== 'Size'));
      this.attributeEnableData = filterAttributeData.filter(enable => enable.fieldEnableValue === true && enable.sortOrder !== undefined).sort((a, b) => 
        a.sortOrder - b.sortOrder
      );
      console.log(filterAttributeData, 'sort');
      console.log('attid', this.attId);
      console.log('attribute sort', this.attributeEnableData);
      this.variationType = this.product.variationType;
      this.checkVariation(this.variationType);
      
    
      /* this.getDiscount();
      this.getProductReview();
      this.checkLogin(); */
      this.checkLogin();
      this.zoom = new Zoom();
      this.zoom.displayClass = 'displayNone';
     
    });
    this.checkBodyHeight();
    this.getNote();
  }
//   getReadyToMeasure() {
//     this.productService.getAllShipmentSetting().subscribe(data =>{
//      this.readyToShip =data[0];
//      console.log('readytoship',this.readyToShip);
//   });
// }
checkBodyHeight() {
  this.productService.getAllBodyHeight().subscribe(data => {
    this.bodyModel = data;
    if (this.productModel.subCategoryId) {
      this.bodyModel.forEach(a => {
        if (a.subCategoryId === this.productModel.subCategoryId) {
          this.isBodyHeight = true;
          this.availableBodyHeight = a;
        }
      });
    } else {
      this.bodyModel.forEach(b => {
        if (b.superCategoryId === this.productModel.superCategoryId) {
          this.isBodyHeight = true;
          this.availableBodyHeight = b;
        }
      });
    }

    console.log('body height', this.availableBodyHeight);
  }, error => {
    console.log(error);
  });
}
getReadyToMeasure() {
  this.productService.getAllShipmentSetting().subscribe(data =>{
   
   this.readyToShip =data[0];
   console.log('88888',this.readyToShip);
   this.productModel.child.forEach(element => {
   
       if(element.ttsPortol<=this.readyToShip.daysCount){
        this.productModel.ttsPortalenable = true;
       }
       else{
        this.productModel.ttsPortalenable = false;
       }
     
   });
   console.log('productModel',this.productModel);
   
});
}

getTtsEnable(element){
if(element.ttsPortol <= this.readyToShip.daysCount){
  return true;
}
}


  checkMeasurementApply(product) {
    if (sessionStorage.getItem('measurment')) {
      const measurementData = JSON.parse(sessionStorage.getItem('measurment'));
      if (product._id === measurementData.productId) {
        this.isMeasurementApply = true;
        console.log('------', measurementData);
        /* this.checkMadeToMeasurement(product); */
        this.getMeasurementByUser(measurementData.measurementId);
      } else {
        sessionStorage.removeItem('measurment');
      }
    }
  }
  getMeasurementByUser(id) {
    this.productService.getSelctedMeasurementByUser(id).subscribe(data => {
      console.log('/////////', data);
      if (this.isMeasurementApply) {
        this.isMeasurement = true;
        
        this.service = {
          serviceId: data._id
        };
      } else {
        this.service = {
          serviceId: 'None'
        };
      }
    }, error => {
      console.log(error);
    });
  }
  ngAfterViewInit() {
    this.getWindowSize();
  }
  getWindowSize() {
    if (window.screen.width > 900) {
      this.showMobileView1 = false;
    } else {
      this.showMobileView1 = true;
    }
  }

  getIncRate() {
    this.productService.getIncrementRate().subscribe(data => {
      this.rateModel = data;
      this.discountCalculation();
    }, error => {
      console.log(error);
    });
  }
  discountCalculation() {
    const discount = 100 - this.productModel.discount;
    const sp = this.productModel.sp * (100 + this.rateModel[0].incRate) / 100;
    this.productModel.price = sp;
    this.productModel.sp = sp;

    const totalPrice = this.productModel.sp * (100 / discount);
    const savePrice = totalPrice - this.productModel.sp;
    this.productModel.savePrice = savePrice;
    this.productModel.totalPrice = totalPrice;
  /*   this.productModel.tailoringService = 'Yes'; */
    this.checkMeasurementApply(this.productModel);
    this.checkMadeToMeasurement(this.productModel);
    this.getReadyToMeasure();
    this.checkQuantity();
    /* this.getReadyToMeasure(this.productModel); */
    /* this.productModel.readyToWear = true; */
   /*  this.checkReadyToWear(); */
    /* this.getReadyToWear(); */
   /*  for (let i = 0; i <= this.productModel.length - 1; i++) {
      this.productModel[i].savePrice = this.productModel[i].sp * (this.productModel[i].discount / 100);
      this.productModel[i].totalPrice = this.productModel[i].sp + this.productModel[i].savePrice;
    } */
    this.getAlsolike();
  }
  checkQuantity() {
    this.isEmptyQty = false;
    console.log('quantity', this.productModel);
    if (this.productModel.quantity <= 3 && this.productModel.quantity !== 0) {
      this.isLowQuantity = true;
      this.isEmptyQty = false;
    } else if (this.productModel.quantity === 0) {
      this.isEmptyQty = true;
      this.isLowQuantity = false;
    } else {
      this.isEmptyQty = false;
      this.isLowQuantity = false;
    }
  }
  /* checkReadyToWear() {
    if (this.productModel.readyToWear === true) {
      this.readyToWearData.forEach(element => {
        if (element.selectedCategoryID === this.productModel.superCategoryId) {
          this.readyToWearholder = element;
        }
      });
    }
  } */
  firstImageSlider() {
    this.productModel.productImage.forEach((element, index) => {
      if (index === 0) {
        element.display = 'displayBlock';
      } else {
        element.display = 'displayNone';
      }
    });
  }
  checkMadeToMeasurement(product) {
    if (product.subCategoryId) {

    } else if (product.mainCategoryId) {

    } else if (product.superCategoryId) {
      this.getSuperCategoryMeaurement(product.superCategoryId);
    }
  }
  getSuperCategoryMeaurement(id) {
    this.productService.getSelectedSuperCategory(id).subscribe(data => {
      console.log('superCatProduc', data);
      this.measurementData = data;
      if (this.measurementData.length === 0) {
        this.getSuperCategoryKameez(id);
      } else {
        this.isBlouse = true;
      }
    }, error => {
      console.log(error);
    });
  }
  getSuperCategoryKameez(id) {
    this.productService.getSelectedSuperCategoryForKameez(id).subscribe(data => {
      this.measurementData = data;
      if (this.measurementData.length === 0) {
        this.getSuperCateogryLehenga(id);
      } else {
        this.isKameez = true;
      }
    }, error => {
      console.log(error);
    });
  }
  getSuperCateogryLehenga(id) {
    this.productService.getLehengaMeasurement(id).subscribe(data => {
      this.measurementData = data;
      this.isLehenga = true;
    }, error => {
      console.log(error);
    });
  }
  checkVariation(variation) {
    switch (variation) {
      case 'None': {
        /* this.productModel = this.product; */
        this.parentContentChange();
        break;
      }
      case 'Color': {
        this.color = new Color();
        this.color.ids = this.product.child.map(color => color.colorId);
        this.categorySelected.filter(select => select.fieldSetting === 'Color');
        /* this.viewAllColors(this.color) */
        this.viewSelectedColors();
        break;
      }
      case 'SizeColor': {
        this.color = new Color();
        const a = this.categorySelected.reduce((selectSize, item) => {
          if (item.fieldSetting === 'Size') {
            selectSize = item;
           }
          return selectSize;
        }, null);

        const b = this.categorySelected.reduce((selectColor, item) => {
          if (item.fieldSetting === 'Color') {
            selectColor = item;
           }
          return selectColor;
        }, null);
        /* this.color.ids = this.product.child.map(color => color.colorId); */
        /* this.color.ids = */
        this.product.child.forEach(che => {
          if (this.sizeColorVariantColors.map(ch => ch.colorId).indexOf(che.colorId) === -1) {
            this.sizeColorVariantColors.push({colorId: che.colorId, headChild: che.headChild});
          }
        });
        this.selectedSizeCategory = a.fieldValue;
        this.selectedColorCategory = b.fieldValue;
        this.product.colorFields = this.selectedColorCategory.filter(e => this.sizeColorVariantColors.map(e => e.colorId).indexOf(e._id) !== -1);
        const headChildId = this.product.child.find(el => el.headChild === true);
        /* this.product.child.map(size => size.sizeVariantId);
        console.log(this.product); */
        /* this.color.ids = this.product.child.map(color => color.colorId);
        this.color.ids = this.product.child.map(color => color.sizeVariantId); */
        /* this.viewAllColors(this.color) */
        this.viewSelectedColorsSize(headChildId);
        break;
      }
      case 'Size': {
        this.parentContentChange();
        break;
      }
      default: {
        // statements;
        break;
      }
    }
  }
  viewSelectedColorsSize(headChildId) {
    this.productModel = this.product;
    this.productModel.child.forEach(element => {
      element.headChild = false;
      if (element.colorId === headChildId.colorId) {
        element.headChild = true;
        this.productModel.selectedColorMain = headChildId.colorId;
        this.productModel.productName = element.productName;
        this.productModel.productDescription = element.productDescription;
        this.productModel.price = element.price;
        this.productModel.INTsku = element.INTsku;
        this.productModel.quentity = element.quentity;
        this.productModel.productImage = element.productImage;
        this.productModel.sp = element.sp;
        this.productModel.discount = element.discount;
        // element.attribute.push(this.makeColor(element));
      /*   element.attribute = element.attribute.filter(item => {
          if(this.attributeEnableData.indexOf(item.attributeId) !== -1){
            console.log(item, 'indexOf item');
          }
        }); */
        element.attribute = this.fieldSorting(element.attribute);
        
        const keysValue = this.getKeys(element.attribute);
        this.productModel.attribute = this.attributeService.getAds(keysValue);
        this.attribute = this.productModel.attribute;
        this.productModel.costIncludes = element.costIncludes !== undefined ? element.costIncludes.split('.') : [];
        this.firstImageSlider();
      }
    });
    this.getIncRate();
  }
  viewSelectedColors() {
    const headChildId = this.product.child.find(el => el.headChild === true);
    this.productModel = this.product;
    this.productModel.child.forEach(element => {
      element.headChild = false;
      if (element.colorId === headChildId.colorId) {
        element.headChild = true;
        this.productModel.productName = element.productName;
        this.productModel.ttsPortal  = new Date(new Date().getTime()+(element.ttsPortal*24*60*60*1000));
        this.productModel.productDescription = element.productDescription;
        this.productModel.price = element.price;
        this.productModel.INTsku = element.INTsku;
        this.productModel.quentity = element.quentity;
        this.productModel.productImage = element.productImage;
        this.productModel.sp = element.sp;
        this.productModel.discount = element.discount;
        // element.attribute.push(this.makeColor(element));
        /* element.attribute = element.attribute.filter(item => {
          return this.attributeEnableData.indexOf(item.attributeId) !== -1;
        }); */
        element.attribute = this.fieldSorting(element.attribute);
        const keysValue = this.getKeys(element.attribute.sort((a,b) => { a.sortOrder - b.sortOrder} ));
        this.productModel.attribute = this.attributeService.getAds(keysValue);
        this.attribute = this.productModel.attribute;
        this.productModel.costIncludes = element.costIncludes !== undefined ? element.costIncludes.split('.') : [];
        this.firstImageSlider();
      }
    });
    this.getIncRate();


  }
  // makeColor(product) {
  //   return this.setColor = {
  //     attributeId: this.attId[0]._id,
  //     color: product.color
  //   };
  // }
 
  public getKeys = (arr) => {
    var key, keys = [];
    for (let i = 0; i < arr.length; i++) {
      for (key in arr[i]) {
        if (arr[i].hasOwnProperty(key)) {
          keys.push({ name: key, value: arr[i][key] });
        }
      }
    }
    this.attributeLength = keys.length;
    return keys.filter(na => (na.name !== '_id') && (na.name !== 'attributeId') &&(na.name !== 'attributeFieldId') );
}
  clickSelectedImgeZoom(selectIndex) {
    this.productModel.productImage.forEach((element, index) => {
      if (index === selectIndex) {
        this.sildeIndex = index;
        element.display = 'displayBlock';
      } else {
        element.display = 'displayNone';
      }
    });
    this.zoom = new Zoom();
    this.zoom.displayClass = 'displayBlock';
    this.zoom.imageData = this.productModel.productImage;
    this.windowScrollingService.disable();
  }
  selectData(event) {
    this.selectedItem = event;
    this.selectedContentChange(event);
  }
  selectSizeColorData(event) {
    this.selectedItem = event;
    const headChildId = event;
    this.viewSelectedColorsSize(headChildId);
  }
  parentContentChange() {
    /* this.productModel.productImage = event.productImage; */
    this.productModel = this.product;
    this.productModel.child.forEach((element) => {
      if (element.headChild === true) {
        this.productModel.productName = element.productName;
        this.productModel.productDescription = element.productDescription;
        this.productModel.price = element.price;
        this.productModel.INTsku = element.INTsku;
        this.productModel.productImage = element.productImage;
        this.productModel.quantity = element.quantity;
        this.productModel.sp = element.sp;
        this.productModel.discount = element.discount;
        // element.attribute.push(this.makeColor(element));
        /* element.attribute = element.attribute.filter(item => {
          
            return this.attributeEnableData.indexOf(item.attributeId) !== -1;
        }); */
        
        element.attribute = this.fieldSorting(element.attribute);
        console.log('element attribute', element.attribute);
        /* this.productModel.ttsPortalenable = this.getTtsEnable(element);
        element.attribute.push(this.makeColor(element));
        element.attribute = element.attribute.filter(item => {
          return this.attributeEnableData.indexOf(item.attributeId) !== -1;
        }); */
        const keysValue = this.getKeys(element.attribute);
        this.productModel.attribute = this.attributeService.getAds(keysValue);
        this.productModel.costIncludes  =  element.costIncludes !== undefined ? element.costIncludes.split('.') : [];
        this.attribute = this.productModel.attribute;
      }
    });
    this.getIncRate();

    this.firstImageSlider();
  }
  fieldSorting(attribute) {
    const arr = [];
    this.attributeEnableData.forEach(el => {
      attribute.forEach(element => {
        if(el._id === element.attributeId) {
          arr.push(element);
        }
        
      });
    })
    return arr;
  }
  selectedContentChange(selected) {
    this.productModel.child.forEach((element) => {
      element.headChild = false;
      if (element._id === selected._id) {
        element.headChild = true;
        this.productModel.productName = element.productName;
        this.productModel.productDescription = element.productDescription;
        this.productModel.price = element.price;
        this.productModel.INTsku = element.INTsku;
        this.productModel.sp = element.sp;
        this.productModel.discount = element.discount;
        this.productModel.quantity = element.quantity;
        this.productModel.productImage = element.productImage;
        // element.attribute.push(this.makeColor(element));
        /* element.attribute = element.attribute.filter(item => {
          return this.attributeEnableData.indexOf(item.attributeId) !== -1;
        }); */
        element.attribute = this.fieldSorting(element.attribute);
        /* this.productModel.ttsPortalenable = this.getTtsEnable(element);
        element.attribute.push(this.makeColor(element));
        element.attribute = element.attribute.filter(item => {
          return this.attributeEnableData.indexOf(item.attributeId) !== -1;
        }); */
        const keysValue = this.getKeys(element.attribute);
        this.productModel.attribute = this.attributeService.getAds(keysValue);
        this.productModel.costIncludes = element.costIncludes !== undefined ? element.costIncludes.split('.') : [];
        this.attribute = this.productModel.attribute;
      }
    });
    this.getIncRate();
    this.firstImageSlider();
  }
  selectedItems(productId) {
    console.log(this.productModel);
    this.selectedItem = this.variationType === 'None' ? this.productModel.child.find(pro => pro.headChild === true) : this.selectedItem;
    if (!this.selectedItem) {
      this.selectedSize = true;
    } else {
      this.selectedSize = false;
      this.skuProductAddToCart(productId, this.selectedItem);
    }
  }
  skuProductAddToCart(productId, skuItem) {
    const userId = sessionStorage.getItem('userId');
    if (JSON.parse(sessionStorage.getItem('login'))) {
      this.addToCartServer(userId, productId, skuItem);
    } else {
      this.addLocalCart(productId, skuItem);
    }
  }
  addLocalCart(productId, skuItem) {
    if (this.productModel.tailoringService === 'Yes') {
      this.addToCartLocalOnTailoringService(productId, skuItem);
    } else {
      this.addToCartLocal(productId, skuItem);
    }
  }
  addToCartLocalOnTailoringService(product, skuItem) {
    if (this.isMeasurement) {
      this.isUnstitched = false;
    } else {
      this.service = {
        serviceId: 'None'
      };
    }
    if (this.isApplyHeight) {
      this.heightHolder = {
        bodyHeight: this.selectedHeight.toString()
      };
    } else {
      this.heightHolder = {
        bodyHeight: 'None'
      };
    }
    const cartLocal = JSON.parse(sessionStorage.getItem('cart')) || [];
    if (cartLocal.length === 0) {
      const totalItem: any = [];
      const currentProduct: any = [];
      currentProduct.push(this.productModel);
      const item = {
        productId: product,
        INTsku: skuItem.INTsku,
        qty: this.count,
        bodyHeight: this.heightHolder.bodyHeight,
        tailoringService: true,
        isMeasurement: this.isMeasurement,
        isUnstitched: this.isUnstitched,
        serviceId: this.service.serviceId
      };
      const cart = {
        items: item,
        cart_product: currentProduct
      };
      totalItem.push(cart);
      this.message = 'Product Added To Cart';
      sessionStorage.setItem('cart', JSON.stringify(totalItem));
      this.snackBar.open(this.message, this.action, {
        duration: 3000,
      });
    } else {
      const totalItem: any = [];
      const currentProduct: any = [];
      currentProduct.push(this.productModel);
      const item = {
        productId: product,
        INTsku: skuItem.INTsku,
        qty: this.count,
        tailoringService: true,
        bodyHeight: this.heightHolder.bodyHeight,
        isMeasurement: this.isMeasurement,
        isUnstitched: this.isUnstitched,
        serviceId: this.service.serviceId
      };
      const cart = {
        items: item,
        cart_product: currentProduct
      };
      totalItem.push(cart);
      totalItem.map(element => {
        if (cartLocal.find(s => s.items.INTsku === element.items.INTsku) ) {
          if (cartLocal.find(s => s.items.tailoringService === element.items.tailoringService && s.items.INTsku === element.items.INTsku)) {
            if (cartLocal.find(s => s.items.tailoringService === element.items.tailoringService && s.items.INTsku === element.items.INTsku && s.items.serviceId === element.items.serviceId)) {
              const localSame = cartLocal.find(s => s.items.tailoringService === element.items.tailoringService && s.items.INTsku === element.items.INTsku && s.items.serviceId === element.items.serviceId);
                localSame.items.qty += element.items.qty;
            } else {
              cartLocal.push(element);  
            }
          } else {
            cartLocal.push(element);
          }
        } else {
          cartLocal.push(element);
        }
       /*  if (cartLocal.find(s => s.items.INTsku === element.items.INTsku) ) {
          if (cartLocal.find(s => s.items.tailoringService === true && element.items.tailoringService === true)) {
            if (cartLocal.find(s => s.items.serviceId === element.items.serviceId)) {
              if (cartLocal.find(s => s.items.selectedSize === element.items.selectedSize)) {
                const localSame = cartLocal.find(s => s.items.INTsku === element.items.INTsku);
                localSame.items.qty += element.items.qty;
              } else {
                cartLocal.push(element);
              }
            } else {
              cartLocal.push(element);
            }
        } else if (cartLocal.find(s => s.items.tailoringService !== true && element.items.tailoringService === true)) {
          cartLocal.push(element);
        } else if (cartLocal.find(s => s.items.tailoringService === false && element.items.tailoringService === false)) {
          const localSame = cartLocal.find(s => s.items.INTsku === element.items.INTsku && element.items.tailoringService === false);
          localSame.items.qty += element.items.qty;
        } else if (cartLocal.find(s => s.items.tailoringService !== false && element.items.tailoringService === false)) {
          cartLocal.push(element);
        }
        } else {
          cartLocal.push(element);
        } */
      });
      this.message = 'Product Added To Cart';
      sessionStorage.setItem('cart', JSON.stringify(cartLocal));
      this.snackBar.open(this.message, this.action, {
        duration: 2000,
      });
    }
    this.router.navigate(['/cart/shopping']);
  }
  /* addToCartLocalOnReadyToWear(product, skuItem) {
    const cartLocal = JSON.parse(sessionStorage.getItem('cart')) || [];
    if (cartLocal.length === 0) {
      const totalItem: any = [];
      const currentProduct: any = [];
      currentProduct.push(this.productModel);
      const item = {
        productId: product,
        INTsku: skuItem.INTsku,
        qty: this.count,
        tailoringService: true,
        selectedSize: this.selectedService.selectedSize,
        serviceId: this.selectedService.serviceId
      };
      const cart = {
        items: item,
        cart_product: currentProduct
      };
      totalItem.push(cart);
      this.message = 'Product Added To Cart';
      sessionStorage.setItem('cart', JSON.stringify(totalItem));
      this.snackBar.open(this.message, this.action, {
        duration: 3000,
      });
    } else {
      const totalItem: any = [];
      const currentProduct: any = [];
      currentProduct.push(this.productModel);
      const item = {
        productId: product,
        INTsku: skuItem.INTsku,
        qty: this.count,
        tailoringService: true,
        selectedSize: this.selectedService.selectedSize,
        serviceId: this.selectedService.serviceId
      };
      const cart = {
        items: item,
        cart_product: currentProduct
      };
      totalItem.push(cart);
      totalItem.map(element => {
        if (cartLocal.find(s => s.items.INTsku === element.items.INTsku) ) {
          if (cartLocal.find(s => s.items.tailoringService === true && element.items.tailoringService === true)) {
            if (cartLocal.find(s => s.items.serviceId === element.items.serviceId)) {
              if (cartLocal.find(s => s.items.selectedSize === element.items.selectedSize)) {
                const localSame = cartLocal.find(s => s.items.INTsku === element.items.INTsku);
                localSame.items.qty += element.items.qty;
              } else {
                cartLocal.push(element);
              }
            } else {
              cartLocal.push(element);
            }
        } else if (cartLocal.find(s => s.items.tailoringService !== true && element.items.tailoringService === true)) {
          cartLocal.push(element);
        } else if (cartLocal.find(s => s.items.tailoringService === false && element.items.tailoringService === false)) {
          const localSame = cartLocal.find(s => s.items.INTsku === element.items.INTsku && element.items.tailoringService === false);
          localSame.items.qty += element.items.qty;
        } else if (cartLocal.find(s => s.items.tailoringService !== false && element.items.tailoringService === false)) {
          cartLocal.push(element);
        }
        } else {
          cartLocal.push(element);
        }
      });
      this.message = 'Product Added To Cart';
      sessionStorage.setItem('cart', JSON.stringify(cartLocal));
      this.snackBar.open(this.message, this.action, {
        duration: 2000,
      });
    }
    this.router.navigate(['/cart/shopping']);
  } */
  addToCartLocal(product, skuItem) {
    if (this.isApplyHeight) {
      this.heightHolder = {
        bodyHeight: this.selectedHeight.toString()
      };
    } else {
      this.heightHolder = {
        bodyHeight: 'None'
      };
    }
    const cartLocal = JSON.parse(sessionStorage.getItem('cart')) || [];
    if (cartLocal.length === 0) {
      const totalItem: any = [];
      const currentProduct: any = [];
      currentProduct.push(this.productModel);
      const item = {
        productId: product,
        INTsku: skuItem.INTsku,
        bodyHeight: this.heightHolder.bodyHeight,
        qty: this.count,
        tailoringService: false
      };
      const cart = {
        items: item,
        cart_product: currentProduct
      };
      totalItem.push(cart);
      this.message = 'Product Added To Cart';
      sessionStorage.setItem('cart', JSON.stringify(totalItem));
      this.snackBar.open(this.message, this.action, {
        duration: 3000,
      });
    } else {
      const totalItem: any = [];
      const currentProduct: any = [];
      currentProduct.push(this.productModel);
      const item = {
        productId: product,
        INTsku: skuItem.INTsku,
        bodyHeight: this.heightHolder.bodyHeight,
        qty: this.count,
        tailoringService: false
      };
      const cart = {
        items: item,
        cart_product: currentProduct
      };
      totalItem.push(cart);
      totalItem.map(element => {
        if (cartLocal.find(s => s.items.INTsku === element.items.INTsku && s.items.tailoringService === element.items.tailoringService && s.items.bodyHeight === element.items.bodyHeight)) {
          const localSame = cartLocal.find(s => s.items.INTsku === element.items.INTsku && s.items.tailoringService === element.items.tailoringService && s.items.bodyHeight === element.items.bodyHeight);
          localSame.items.qty += element.items.qty;
        } else {
          cartLocal.push(element);
        }
      });
      this.message = 'Product Added To Cart';
      sessionStorage.setItem('cart', JSON.stringify(cartLocal));
      this.snackBar.open(this.message, this.action, {
        duration: 2000,
      });
    }
    this.router.navigate(['/cart/shopping']);
  }
  addToCartServer(userId, product, skuItem) {
    if (this.isMeasurement) {
      this.isUnstitched = false;
    } else {
      this.service = {
        serviceId: 'None'
      };
    }
    if (this.isApplyHeight) {
      this.heightHolder = {
        bodyHeight: this.selectedHeight.toString()
      };
    } else {
      this.heightHolder = {
        bodyHeight: 'None'
      };
    }
    console.log(this.selectedService);
    console.log(this.isMeasurement, this.isUnstitched, this.productModel.tailoringService);
    if (this.productModel.tailoringService === 'Yes') {
      const totalItem: any = [];
      const cart = {
        productId: product,
        INTsku: skuItem.INTsku,
        qty: this.count,
        bodyHeight: this.heightHolder.bodyHeight,
        tailoringService: true,
        isMeasurement: this.isMeasurement,
        isUnstitched: this.isUnstitched,
        serviceId: this.service.serviceId
      };
      totalItem.push(cart);
      this.cartModel = new Cart();
      this.cartModel.userId = userId;
      this.cartModel.items = totalItem;
    } else {
      const totalItem: any = [];
      const cart = {
        productId: product,
        INTsku: skuItem.INTsku,
        bodyHeight: this.heightHolder.bodyHeight,
        qty: this.count,
        tailoringService: false,
      };
      totalItem.push(cart);
      this.cartModel = new Cart();
      this.cartModel.userId = userId;
      this.cartModel.items = totalItem;
    }
    this.productService.addToCart(this.cartModel).subscribe(data => {
      this.shopModel = data;
      sessionStorage.setItem('cartqty', this.shopModel.length);
      this.message = 'Product Added To Cart';
      this.snackBar.open(this.message, this.action, {
        duration: 3000,
      });
      this.router.navigate(['/cart/shopping']);
    }, error => {
      console.log(error);
    });
  }
  // addToCartServer(userId, product, skuItem) {
  //   console.log(this.selectedService);
  //   if (this.selectedService !== undefined) {
  //     const totalItem: any = [];
  //     const cart = {
  //       productId: product,
  //       INTsku: skuItem.INTsku,
  //       qty: this.count,
  //       tailoringService: true,
  //       selectedSize: this.selectedService.selectedSize,
  //       serviceId: this.selectedService.serviceId
  //     };
  //     totalItem.push(cart);
  //     this.cartModel = new Cart();
  //     this.cartModel.userId = userId;
  //     this.cartModel.items = totalItem;
  //     /* this.cartModel.tailoringService = this.selectedService; */
  //     this.productService.addToCart(this.cartModel).subscribe(data => {
  //       this.shopModel = data;
  //       sessionStorage.setItem('cartqty', this.shopModel.length);
  //       this.message = 'Product Added To Cart';
  //       this.snackBar.open(this.message, this.action, {
  //         duration: 3000,
  //       });
  //     }, error => {
  //       console.log(error);
  //     });
  //   } else {
  //     const totalItem: any = [];
  //     const cart = {
  //       productId: product,
  //       INTsku: skuItem.INTsku,
  //       qty: this.count,
  //       tailoringService: false,
  //     };
  //     totalItem.push(cart);
  //     this.cartModel = new Cart();
  //     this.cartModel.userId = userId;
  //     this.cartModel.items = totalItem;
  //     /* this.cartModel.tailoringService = this.selectedService; */
  //     this.productService.addToCart(this.cartModel).subscribe(data => {
  //       this.shopModel = data;
  //       sessionStorage.setItem('cartqty', this.shopModel.length);
  //       this.message = 'Product Added To Cart';
  //       this.snackBar.open(this.message, this.action, {
  //         duration: 3000,
  //       });
  //     }, error => {
  //       console.log(error);
  //     });
  //   }
  // }
  addToWishList(product) {
    this.wish = new WishList();
    this.wish.userId = this.userId;
    this.wish.productId = product._id;
    this.productService.addToWishList(this.wish).subscribe(data => {
      this.wishList = data;
      const wishlist: any = this.wishList.map(a => a.productIds);
      sessionStorage.setItem('wislistLength', wishlist.length);
      this.wishList.forEach( d => {
        if(d.productIds.proId == product._id) {
          this.productModel.wishList = true;
          } else {
            this.productModel.wishList = false;
          }
      });
    }, err => {
      console.log(err);
    });
  }
  actionPlus(plus) {
    this.count = ++plus;
  }
  actionMinus(minus) {
    this.count = --minus;
  }
  getProductReview() {
    this.productService.getSingleProductReview(this.id).subscribe(data => {
      this.reviewData = data;
      let sum = 0;
      for (let i = 0; i <= data.length - 1; i++) {
        sum += data[i].rating;
      }
      this.totalRating = Math.round(sum / data.length);
    }, error => {
      console.log(error);
    });
  }
  getDiscount() {
    this.productService.getAllDiscount().subscribe(data => {
      this.discountStore = data;
      this.productModel.discountStyle = 'discountNone';    // set all discountStyle field 'discountNone'
      for (let i = 0; i <= this.discountStore.length - 1; i++) {
        for (let j = 0; j <= this.discountStore[i].conditions.length - 1; j++) {
          for (let k = 0; k <= this.discountStore[i].conditions[j].value.length - 1; k++) {


            if (this.discountStore[i].conditions[j].field === 'Product Name') {  // check discount field

              if (this.productModel._id === this.discountStore[i].conditions[j].value[k]) {

                if (this.productModel.discountStyle === 'discountStyle') {   // check offer already applied or not
                  if (this.discountStore[i].amountType === 'Flat') {          // check amount type
                    const temp = this.productModel.oldPrice - this.discountStore[i].typeValue;

                    /* check amount greaterthan or lessthan ----- start ---------*/

                    if (this.productModel.price > temp) {    // new price is lesserthan previous price
                      this.tempDiscount = this.discountStore[i];
                      this.productModel.price = this.productModel.oldPrice - this.discountStore[i].typeValue;
                      this.productModel.discount = 'Flat ' + this.discountStore[i].typeValue;
                      console.log(this.productModel.price);
                    } else {      // new price is smallerthan previous price
                      continue;
                    }

                    /* check amount greaterthan or lessthan ----- end ---------*/

                  } else {          // amount type percentage
                    const temp = this.productModel.oldPrice - Math.round((this.productModel.oldPrice / 100 * this.discountStore[i].typeValue));


                    /* check amount greaterthan or lessthan ----- start ---------*/

                    if (this.productModel.price > temp) {   // new price is lesserthan previous price
                      this.tempDiscount = this.discountStore[i];
                      this.productModel.price = this.productModel.oldPrice - Math.round((this.productModel.oldPrice / 100 * this.discountStore[i].typeValue));
                      this.productModel.discount = this.discountStore[i].typeValue + '%';
                      console.log(this.productModel.price);
                    } else {     // new price is smallerthan previous price
                      continue;
                    }
                  }

                  /* check amount greaterthan or lessthan ----- end ---------*/

                  /* previous offer not applied product-------------- start ------------- */


                } else {
                  this.productModel.discountStyle = 'discountStyle';
                  this.productModel.oldPrice = this.productModel.price;
                  if (this.discountStore[i].amountType === 'Flat') {
                    this.tempDiscount = this.discountStore[i];
                    this.productModel.price = this.productModel.oldPrice - this.discountStore[i].typeValue;
                    this.productModel.discount = 'Flat ' + this.discountStore[i].typeValue;
                    console.log(this.productModel.price);
                  } else {
                    this.tempDiscount = this.discountStore[i];
                    this.productModel.price = this.productModel.oldPrice - Math.round((this.productModel.oldPrice / 100 * this.discountStore[i].typeValue));
                    this.productModel.discount = this.discountStore[i].typeValue + '%';
                    console.log(this.productModel.price);
                  }
                }

                /* previous offer not applied product-------------- end ------------- */

              }
            } else if (this.discountStore[i].conditions[j].field === 'Product Category') {  // check discount field
              if (this.productModel.superCategoryId === this.discountStore[i].conditions[j].value[k]) {
                if (this.productModel.discountStyle === 'discountStyle') {  // check offer already applied or not
                  if (this.discountStore[i].amountType === 'Flat') {     // check amount type
                    const temp = this.productModel.oldPrice - this.discountStore[i].typeValue;

                    /* check amount greaterthan or lessthan ----- start ---------*/

                    if (this.productModel.price > temp) {     // new price is lesserthan previous price
                      this.tempDiscount = this.discountStore[i];
                      this.productModel.price = this.productModel.oldPrice - this.discountStore[i].typeValue;
                      this.productModel.discount = 'Flat ' + this.discountStore[i].typeValue;
                      console.log(this.productModel.price);
                    } else {     // new price is lesserthan previous price
                      continue;
                    }
                    /* check amount greaterthan or lessthan ----- end ---------*/

                  } else {     // amount type percentage
                    const temp = this.productModel.oldPrice - Math.round((this.productModel.oldPrice / 100 * this.discountStore[i].typeValue));

                    /* check amount greaterthan or lessthan ----- start ---------*/

                    if (this.productModel.price > temp) {  // new price is lesserthan previous price
                      this.tempDiscount = this.discountStore[i];
                      this.productModel.price = this.productModel.oldPrice - Math.round((this.productModel.oldPrice / 100 * this.discountStore[i].typeValue));
                      this.productModel.discount = this.discountStore[i].typeValue + '%';
                      console.log(this.productModel.price);
                    } else {
                      continue;
                    }
                  }
                } else {
                  this.productModel.discountStyle = 'discountStyle';
                  this.productModel.oldPrice = this.productModel.price;
                  if (this.discountStore[i].amountType === 'Flat') {
                    this.tempDiscount = this.discountStore[i];
                    this.productModel.price = this.productModel.oldPrice - this.discountStore[i].typeValue;
                    this.productModel.discount = 'Flat ' + this.discountStore[i].typeValue;
                    console.log(this.productModel.price);
                  } else {
                    this.tempDiscount = this.discountStore[i];
                    this.productModel.price = this.productModel.oldPrice - Math.round((this.productModel.oldPrice / 100 * this.discountStore[i].typeValue));
                    this.productModel.discount = this.discountStore[i].typeValue + '%';
                    console.log(this.productModel.price);
                  }
                }
              }
            }

          }
        }
      }
    }, error => {
      console.log(error);
    });
  }
  /* selectSizeData(event) {
    this.selectedItem = event;
    if (this.productModel.discount !== 0 && this.productModel.discount !== undefined) {
      this.productModel.discountStyle = 'discountStyle';
      this.productModel.oldPrice = event.price;
      if (this.tempDiscount.amountType === 'Flat') {
        this.productModel.price = this.productModel.oldPrice - this.tempDiscount.typeValue;
      } else {
        this.productModel.price = this.productModel.oldPrice - Math.round((this.productModel.oldPrice / 100 * this.tempDiscount.typeValue));
      }
      this.selectedItem = event;
    } else {
      this.productModel.price = event.price;
      this.productModel.discountStyle = 'discountNone';
      this.selectedItem = event;
    }
  } */

  /*  checkLogin() {
     if (JSON.parse(sessionStorage.getItem('login'))) {
       this.userId = sessionStorage.getItem('userId');
       this.getwishList();
     } else {
     }
   } */
  getwishList() {
    this.wish = new WishList();
    this.wish.userId = this.userId;
    this.productService.getWishList(this.wish).subscribe(data => {
      this.wishList = data;
      this.checkWishListEnable();
    }, err => {
      console.log(err);
    });
  }

  checkWishListEnable() {
    const wishlist: any = this.wishList.map(a => a.productIds);
    sessionStorage.setItem('wislistLength', wishlist.length);
    for (const item of wishlist) {
      if (item.proId === this.productModel._id) {
        this.productModel.wishList = true;
      }
    }

    /*  for (const prod of this.productModel) {
       if (obj[prod._id]) {
         prod.wishList = true;
       } else {
         prod.wishList = false;
       }
     } */
  }
  addRecentProductLocalStorage(product) {
    const storedValue = JSON.parse(localStorage.getItem('recent')) || [];
    console.log(storedValue);
    const array: any = [];
    let isStored = false;
    array.push(product._id);
    if (storedValue.length === 0) {
      localStorage.setItem('recent', JSON.stringify(array));
    } else {
      storedValue.forEach(element => {
        if (element === product._id) {
          isStored = true;
        } else {
          isStored = false;
          array.push(element);
        }
      });
      if (isStored === false) {
        localStorage.setItem('recent', JSON.stringify(array));
      }
    }
    this.getRecentProductfromLocalStorage();
  }
  addRecentProductServer(userId, product, recentProduct) {
    if (recentProduct.length === 0) {
      this.addDirectRecentProduct(userId, product);
      this.showRecent = true;
    } else {
      this.isRecentProduct = false;
      recentProduct[0].productId.forEach(element => {
        if (element === product._id) {
          this.isRecentProduct = true;
        }
      });
      if (!this.isRecentProduct) {
        this.addDirectRecentProduct(userId, product);
      }
    }
  }
  /* addUploadRecentProduct(userId, product) {
    const temp = new RecentProduct();
    temp.userId = userId;
    temp.productId = product._id;

  } */
  addDirectRecentProduct(userId, product) {
    const temp = new RecentProduct();
    temp.userId = userId;
    temp.productId = product._id;
    this.productService.addRecentProductID(temp).subscribe(data => {
      console.log(data);
    }, error => {
      console.log(error);
    });
  }
  getRecentProduct(userId, product) {
    this.productService.getRecentProductID(userId).subscribe(data => {
      console.log('recent', data);
      this.addRecentProductServer(userId, product, data);
    }, error => {
      console.log(error);
    });
  }
  checkLoginUser(product) {
   
  }
  checkLogin() {
    if (JSON.parse(sessionStorage.getItem('login'))) {
      this.userId = sessionStorage.getItem('userId');
      this.getwishList();
      this.getRecentProduct(this.userId, this.productModel);
      this.viewRecentProduct(this.userId);
    } else {
      this.addRecentProductLocalStorage(this.productModel);
    }
  }
  getRecentProductfromLocalStorage() {
    const recentId = JSON.parse(localStorage.getItem('recent'));
    this.RecentProduct = {
      productId: recentId
    };
    this.productService.getRecentProductByLocalStorage(this.RecentProduct).subscribe(data => {
      this.recentPrdouctData = data;
      this.recentPrdouctData = this.recentPrdouctData.filter(element => element._id !== this.productModel._id);
     /*  this.sharedService.multipleProductDiscount(this.discountStore, this.recentPrdouctData); */
      this.recentPrdouctData.forEach(el => {
      // el.price = el.price * ((100 + this.rateModel[0].incRate) / 100);
      // el.sp = el.sp * ((100 + this.rateModel[0].incRate) / 100);
    });
      console.log('reent', this.recentPrdouctData);
      if (this.recentPrdouctData.length !== 0) {
        this.showRecent = false;
      } else {
        this.showRecent = true;
      }
    }, error => {
      console.log(error);
    });
  }
  viewRecentProduct(userId) {
    this.productService.viewRecentProduct(userId).subscribe(data => {
      this.recentPrdouctData = data;
      this.recentPrdouctData = this.recentPrdouctData.filter(element => element._id !== this.productModel._id);
      this.recentPrdouctData.forEach(el => {
        el.price = el.price * ((100 + this.rateModel[0].incRate) / 100);
        el.sp = el.sp * ((100 + this.rateModel[0].incRate) / 100);
      });
     /*  this.sharedService.multipleProductDiscount(this.discountStore, this.recentPrdouctData); */
     /*  this.checkTotalWishlist(); */
    }, error => {
      console.log(error);
    });
  }
  checkTotalWishlist() {
    const wish = new WishList();
    wish.userId = this.userId;
    this.productService.getWishList(wish).subscribe(data => {
      this.wishList = data;
      this.checkWishListEnable();
      this.checkWishListEnableSimilarProduct();
    }, error => {
      console.log(error);
    });
  }
  checkWishListEnableSimilarProduct() {
    const wishlist: any = this.wishList.map(a => a.productIds);
    const obj = {};
    for (const item of wishlist) {
      if (!obj[item.proId]) {
        const element = item.proId;
        obj[element] = true;
      }
    }
    for (const prod of this.recentPrdouctData) {
      if (obj[prod._id]) {
        prod.wishList = true;
      } else {
        prod.wishList = false;
      }
    }
    for (const prod of this.brandProductData) {
      if (obj[prod._id]) {
        prod.wishList = true;
      } else {
        prod.wishList = false;
      }
    }
  }
  getBrandedProduct(brandId) {
    this.productService.getAllBrandProduct(brandId).subscribe(data => {
      this.brandProductData = data;
      this.brandProductData = this.brandProductData.filter(e => e._id !== this.id);
      this.sharedService.multipleProductDiscount(this.discountStore, this.brandProductData);
    }, error => {
      console.log(error);
    });
  }
  getAlsolike() {
    this.alsoLike = {
      sp: this.productModel.sp,
      catalogueName: this.productModel.catalogueName,
      fabric: this.productModel.fabric
    };
    this.productService.getAlsoLike(this.alsoLike).subscribe(data => {
      this.alsoModel = data;
      this.alsoModel = this.alsoModel.filter(element => element._id !== this.productModel._id);
      this.alsoModel.forEach(el => {
        el.price = el.price * ((100 + this.rateModel[0].incRate) / 100);
        el.sp = el.sp * ((100 + this.rateModel[0].incRate) / 100);
      });
      console.log('aslomodel', data);
    }, error => {
      console.log(error);
    });
  }

  getSelectedService(element) {
    this.selectedService = element;
 
  }
  selectedHeight(height) {
    console.log(height);
    this.isApplyHeight = true;
    this.selectedHeight = height;
  }
  getNote(){
    this.productService.getCatNote().subscribe( data => {
      this.noteData = data;
      console.log(this.noteData,"all note")
      this.noteData.forEach(d => {
        console.log(d,"n");
        if(d.superCategoryId == this.supId){
          if(this.subId === undefined){
            this.note = d.note;
            console.log(d.note,"note")
          } else if(this.subId == d.subCategoryId){
            this.note = d.note;
            console.log(d.note,"note")
          } else {
            this.note = " ";
          }
        }
      });
    });
  }
  }
