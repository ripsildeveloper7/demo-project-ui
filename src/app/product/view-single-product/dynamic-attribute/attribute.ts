import { Type } from '@angular/core';

export class Attribute {
  constructor(public component: Type<any>, public data: any) {}
}
