import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Product } from './../../../shared/model/product.model';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { WishList } from './../../../shared/model/wishList.model';
import { LoadResolverService } from '../../../shared/load-resolver.service';

@Component({
  selector: 'app-add-to-cart',
  templateUrl: './add-to-cart.component.html',
  styleUrls: ['./add-to-cart.component.css']
})
export class AddToCartComponent implements OnInit {
  date: any;
  day: any;
  month: any;
  year: any;
  wish: WishList;
  wishList: WishList[];
  userId;
  @Output() addTocart = new EventEmitter<any>();
  @Output() moveToWishlist = new EventEmitter<any>();
  @Input() productModel: Product;
  @Input() isNotAvailable: boolean;
  @Input() isLowQuantity: boolean;
  @Input() isEmptyQty: boolean;
  @Input() isNonZero: boolean;
  displayMobile = false;
  myDate;
  shippingDate;
  months = ["January","February","March","April","May","June","July","August","September","October","November","December"];
  constructor(private dialog: MatDialog, private router: Router,  private loadResolverService: LoadResolverService,) { 

  }

  ngOnInit() {
    console.log(this.productModel)
    if (window.screen.width > 900) {
      this.displayMobile = false;
    } else {
      this.displayMobile = true;
    }
    this.deliveryDate();
  }
  ngAfterViewInit() {
    this.getWindowSize();
  }
  getWindowSize() {
    if (window.screen.width > 900) {
      this.displayMobile = false;
    } else {
      this.displayMobile = true;
    }
  }
  addToCartItem(proId) {
    this.addTocart.emit(proId);
  
  }
  deliveryDate(){
    // this.date = new Date().toJSON().slice(0,10).split('-').reverse().join('/') ;
    // this.date = moment.format('D,MMM,YYYY');
    this.shippingDate = this.productModel.ttsPortol;
    console.log('tts portal', this.productModel.ttsPortol);
    this.myDate = new Date(new Date().getTime()+(this.shippingDate*24*60*60*1000));
    console.log('current date' , this.myDate);
   /*  this.date = new Date();
    this.day = this.date.getDate() + this.productModel.ttsPortol;
    this.month = this.months[this.date.getMonth()];
    this.year = this.date.getFullYear();
    console.log(this.day, this.month, this.year); */
  }

  
  checkLoginUser(product) {
    if (JSON.parse(sessionStorage.getItem('login'))) {
      this.userId = sessionStorage.getItem('userId');
      this.moveToWishlist.emit(product);
    } else {
      this.loadResolverService.openLogin();
      /* this.router.navigate(['/account/acc/signin']); */
    }
  }
}
