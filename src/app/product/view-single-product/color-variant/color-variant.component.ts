import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Child } from '../../../shared/model/child.model';
import { Color } from '../../../shared/model/colorSetting.model';

@Component({
  selector: 'app-color-variant',
  templateUrl: './color-variant.component.html',
  styleUrls: ['./color-variant.component.css']
})
export class ColorVariantComponent implements OnInit {
  @Input() productModel: any;
  @Input() selectedSize: any;
  @Input() sizeColor: Color;
  @Input() selectedColorMain: string;
  @Output() selectedVariantColor = new EventEmitter<any>();
  @Output() selectedImage = new EventEmitter<any>();
  selectedItem: Child;
  constructor() { }

  ngOnInit() {
  }
  /* colorSelect(itemselect) {
    this.selectedItem = itemselect;
    this.selectedVariantColor.emit(this.selectedItem);
  } */
  selectedColor(colorProduct) {
    this.selectedVariantColor.emit(colorProduct);
  }
}
