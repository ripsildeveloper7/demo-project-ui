import { Component, OnInit, AfterViewInit, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductCart } from '../../../shared/model/productCart.model';
@Component({
  selector: 'app-similar-product',
  templateUrl: './similar-product.component.html',
  styleUrls: ['./similar-product.component.css']
})
export class SimilarProductComponent implements OnInit {
  @Input() recentPrdouctData: any;
  @Input() productImageUrl: any;
  @Output() selectProduct = new EventEmitter<any>();
  @Input() brandProductData: any;
  @Input() productModel: any;
  @Output() selectedProductcart = new EventEmitter<any>();
  @Output() wishListData = new EventEmitter<any>();
  @Input() showRecent: boolean;
  promotion = [
    {img: '../../../../assets/images/product/1.JPG', price: 400 , name: 'Regular Fit Straight Leg Jean'},
    {img: '../../../../assets/images/product/2.JPG', price: 450, name: 'Modern Series Straight Leg Jean'},
    {img: '../../../../assets/images/product/3.JPG', price: 500, name: 'Modern Series Slim Straight Leg Jean'},
    {img: '../../../../assets/images/product/4.JPG', price: 400, name: 'Modern Series Relaxed Bootcut Jean'},
    {img: '../../../../assets/images/product/5.JPG', price: 700, name: 'Modern Series Skinny Jean'},
    {img: '../../../../assets/images/product/1.JPG', price: 700, name: 'Regular Fit Straight Leg Jean'},
    {img: '../../../../assets/images/product/1.JPG', price: 600, name: 'Extreme Motion Bootcut Jean'},
    {img: '../../../../assets/images/product/1.JPG', price: 500, name: 'Regular Fit Bootcut Jean'}

  ];
  displayMobile = false;
  userId: string;
  constructor(private router: Router) {
    if (window.screen.width > 900) {
      this.displayMobile = false;
    } else {
      this.displayMobile = true;
    }
   }
  ngOnInit() {
  }
  ngAfterViewInit() {
    this.getWindowSize();
  }
  getWindowSize() {
    if (window.screen.width > 900) {
      this.displayMobile = false;
    } else {
      this.displayMobile = true;
    }
  }
  skuProductAddToCart(product, size) {
    const temp = new ProductCart();
    temp.productId = product;
    temp.sizeValue = size;
    this.selectedProductcart.emit(temp);
  }
  getProduct(product) {
    /* console.log('check', product); */
    /* this.router.navigate(['/product/viewsingle/', product._id]); */
    this.selectProduct.emit(product._id);
  }
  closeToBagRecentProduct(id, recentPrdouctData) {
    recentPrdouctData.forEach(element => {
      if (element._id === id) {
        element.display = false;
      }
    });
  }
  closeToBagLikedProduct(id, likedProduct) {
    likedProduct.forEach(element => {
      if (element._id === id) {
        element.display = false;
      }
    });
  }
  checkLoginUser(product) {
    if (JSON.parse(sessionStorage.getItem('login'))) {
      this.userId = sessionStorage.getItem('userId');
      this.wishListData.emit(product);
    } else {
      this.router.navigate(['/account/acc/signin']);
    }
  }
  addToBagRecentProduct(id, recentPrdouctData) {
    recentPrdouctData.forEach(element => {
      if (element._id === id) {
        element.display = true;
      } else {
        element.display = false;
      }
    });
  }
  addToBagLikedProduct(id, likedProduct) {
    likedProduct.forEach(element => {
      if (element._id === id) {
        element.display = true;
      } else {
        element.display = false;
      }
    });
  }
  getViewAll() {
    this.userId = sessionStorage.getItem('userId');
    this.router.navigate(['/product/supercategory/recentlyviewed/', this.userId]);
  }
  getBrandedViewAll(product) {
    this.router.navigate(['']);
  }
}

