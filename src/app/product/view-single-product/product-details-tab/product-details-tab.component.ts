import { Component, OnInit, Input } from '@angular/core';
import { Product } from './../../../shared/model/product.model';
@Component({
  selector: 'app-product-details-tab',
  templateUrl: './product-details-tab.component.html',
  styleUrls: ['./product-details-tab.component.css']
})
export class ProductDetailsTabComponent implements OnInit {
  tabItems = [{item: 'Description'}, {item: 'Fabric & Care'}, {item: 'Brand Bio'}, {item: 'Delivery'}];
  @Input() productModel: Product[];
  selectedItemTab = this.tabItems[0].item;
  @Input() productImageUrl: string;
  constructor() { }

  ngOnInit() {
  }
  selectedTab(tab) {
    this.selectedItemTab = tab;
  }
}
