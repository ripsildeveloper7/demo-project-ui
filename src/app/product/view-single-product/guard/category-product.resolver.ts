import { Observable, of } from 'rxjs';
import { ProductService } from './../../../product/product.service';
import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from '@angular/router';
import { Product } from '../../../shared/model/product.model';
import { SuperCategory } from 'src/app/shared/model/superCategory.model';

@Injectable()
export class CategoryProductResolver implements Resolve<SuperCategory> {
  constructor(
    private productService: ProductService,
    private router: Router
  ) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<SuperCategory> {
    const selectcatid = route.paramMap.get('selectcatid');
    return this.productService.getSingleMatchCategory(selectcatid); /* .pipe(
      catchError(_ => {
        this.router.navigate(['']);
        return of(new Product());
      })
    ); */
  }
}

