import { Component, OnInit, Inject, Optional, Input  } from '@angular/core';
import { FormControl, FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { WishList } from './../../shared/model/wishList.model';
import { WishlistService } from './../wishlist.service';
import { SelectService } from '../view-wishlist/select.service';

@Component({
  selector: 'app-color-variant',
  templateUrl: './color-variant.component.html',
  styleUrls: ['./color-variant.component.css']
})
export class ColorVariantComponent implements OnInit {
  constructor(@Optional() @Inject(MAT_DIALOG_DATA) public data: any, private dialogRef: MatDialogRef<ColorVariantComponent>, private wishlistService: WishlistService,  private fb: FormBuilder) { }
  wishForm: FormGroup;
  wishList: WishList[];
  wish: WishList;
  userId: string;
  selectedSize: boolean = false;
  
  ngOnInit() {
    this.wishForm = this.fb.group({
      seletedSku: ['']
    });
  }
 
  selectedWishSize(data, wish) {
    wish.productIds.INTsku = data;
   /*  wish.forEach(mat => {
      if (mat.productIds.proId === wish.productIds.proId) {
          mat.productIds.sku = data;
      }
     }); */
  }
  
  selectedWishColor(data, wish) {
    wish.productIds.INTsku = data;
   /*  wish.forEach(mat => {
      if (mat.productIds.proId === wish.productIds.proId) {
          mat.productIds.sku = data;
      }
     }); */
  }
  moveToBag(proId, pro){
    if (JSON.parse(sessionStorage.getItem('login')) && pro.INTsku) {
      this.selectedSize = false;
      this.userId = sessionStorage.getItem('userId');
      this.wish = new WishList();
      this.wish.userId = this.userId;
      this.wish.productId = proId;
      this.wish.INTsku = pro.INTsku;
      this.wishlistService.moveToAddtoCart(this.wish).subscribe(data => {
        this.wishList = data;
        this.dialogRef.close(true);
        const wishlist: any = this.wishList.map(a => a.productIds);
        sessionStorage.setItem('wislistLength', wishlist.length);
        }, err => {
          console.log(err);
        });
      }
     else {
      this.selectedSize = true;
    }
}
selectedColor(allproduct, selectColor) {
  allproduct.child.forEach(element => {
      element.headChild = false;
    if(element._id === selectColor._id) {
      element.headChild = true;
      /* wish.productIds.INTsku = element.INTsku; */
    }
  });
  /* this.selectedVariantColor.emit(colorProduct); */
}
}
