import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewWishlistComponent } from './view-wishlist/view-wishlist.component';

const routes: Routes = [{path: 'user', component: ViewWishlistComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WishlistRoutingModule { }
