import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewWishlistComponent } from './view-wishlist/view-wishlist.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { WishlistRoutingModule } from './wishlist-routing.module';
import {
  MatSidenavModule,
  MatListModule,
  MatTooltipModule,
  MatOptionModule,
  MatSelectModule,
  MatMenuModule,
  MatSnackBarModule,
  MatGridListModule,
  MatToolbarModule,
  MatIconModule,
  MatButtonModule,
  MatRadioModule,
  MatCheckboxModule,
  MatCardModule,
  MatProgressSpinnerModule,
  MatExpansionModule,
  MatRippleModule,
  MatDialogModule,
  MatChipsModule,
  MatInputModule,
  MatFormFieldModule,
  MatStepperModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatTableModule,
  MatSortModule,
  MatTabsModule,
  MatSliderModule
} from '@angular/material';
import { SharedModule } from './../shared/shared.module';
import { SelectWishlistComponent } from './select-wishlist/select-wishlist.component';
import { WishlistService } from './wishlist.service';
import { SelectService } from './view-wishlist/select.service';
import { ColorVariantComponent } from './color-variant/color-variant.component';
import { SizeVariantComponent } from './size-variant/size-variant.component';
import { SizeColorComponent } from './size-color/size-color.component';

@NgModule({
  declarations: [ViewWishlistComponent, SelectWishlistComponent, ColorVariantComponent, SizeVariantComponent, SizeColorComponent],
  imports: [
    CommonModule,
    WishlistRoutingModule,
    CommonModule,
    MatNativeDateModule,
    MatOptionModule,
    MatGridListModule,
    MatCardModule,
    MatIconModule,
    MatTooltipModule,
    MatToolbarModule,
    MatButtonModule,
    MatRippleModule,
    MatChipsModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule,
    MatSelectModule,
    MatSnackBarModule,
    MatSidenavModule,
    MatListModule,
    MatExpansionModule,
    MatMenuModule,
    MatStepperModule,
    MatDatepickerModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatTableModule,
    FormsModule,
    ReactiveFormsModule,
    MatSortModule,
    SharedModule
  ],
  providers: [WishlistService, SelectService],
  entryComponents: [SelectWishlistComponent, ColorVariantComponent, SizeVariantComponent, SizeColorComponent]
})
export class WishlistModule { }
