import { Component, OnInit } from '@angular/core';
import { InfoService } from '../info.service';

@Component({
  selector: 'app-terms-conditions',
  templateUrl: './terms-conditions.component.html',
  styleUrls: ['./terms-conditions.component.css']
})
export class TermsConditionsComponent implements OnInit {

  terms: any;
  constructor(private infoservice : InfoService) { 
    this.infoservice.getTerms()
    .subscribe( data => {
      this.terms = data
      console.log(data);
    })
  }

  ngOnInit() {
  }

}
