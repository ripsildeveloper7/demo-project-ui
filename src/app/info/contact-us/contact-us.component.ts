import { Component, OnInit, AfterViewInit } from '@angular/core';
import {
  MatSidenavModule,
  MatListModule,
  MatTooltipModule,
  MatOptionModule,
  MatSelectModule,
  MatMenuModule,
  MatSnackBarModule,
  MatGridListModule,
  MatToolbarModule,
  MatIconModule,
  MatButtonModule,
  MatRadioModule,
  MatCheckboxModule,
  MatCardModule,
  MatProgressSpinnerModule,
  MatExpansionModule,
  MatRippleModule,
  MatDialogModule,
  MatChipsModule,
  MatInputModule,
  MatFormFieldModule,
  MatStepperModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatTableModule,
  MatSortModule,
  MatTabsModule,
  MatSliderModule,
  MatDialog,
  MatDialogConfig
} from '@angular/material';
import {  FormControl, FormGroup, FormBuilder} from '@angular/forms';
import {MatSnackBar,MatSnackBarConfig} from '@angular/material';
import { contactUs } from '../../shared/model/contactus.model';
import { InfoService } from '../info.service';
import { DailogComponent } from '../../info/dailog/dailog.component';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit, AfterViewInit {
  showMobileView = false;
  contactUsForm : contactUs;
  contactForm: FormGroup;
  // _snackBar: any;
  constructor(private dialog: MatDialog,private fb: FormBuilder, private infoService: InfoService,private _snackBar: MatSnackBar ) { 
    if (window.screen.width > 900) {
      this.showMobileView = false;
    } else {
      this.showMobileView = true;
    }
  }

  ngOnInit() {
    this.contactForm = this.fb.group({
      Name: [''],
      email: [''],
      mobile: [''],
      Message:['']
    });
  }
  ngAfterViewInit() {
    this.getWindowSize();
  }
  getWindowSize() {
    if (window.screen.width > 900) {
      this.showMobileView = false;
    } else {
      this.showMobileView = true;
    }
  }
  openSnackBar(message: string, action: string) {
    // const config = new MatSnackBarConfig();
    // config.panelClass = ['snackbar'];
    this._snackBar.open(message, action, {
      duration: 2000,
      panelClass : ['snackbar']
    });
  }
  onSubmit(){
    this.openDialog();
    this.contactUsForm = new contactUs;
    
    this.contactUsForm.mobileNumber = this.contactForm.controls.mobile.value;
    this.contactUsForm.emailId = this.contactForm.controls.email.value;
    this.contactUsForm.customerName = this.contactForm.controls.Name.value;
    this.contactUsForm.description = this.contactForm.controls.Message.value;
    this.infoService.contactSubmit(this.contactUsForm).subscribe(data =>{
      console.log(data);
    }, error => {
      console.log(error);
    })
  //   this.openSnackBar("We will get back to you shortly","");
   }
  openDialog() {
    const dialogConfig = new MatDialogConfig();
    this.dialog.open(DailogComponent, dialogConfig);
  }
  

}
