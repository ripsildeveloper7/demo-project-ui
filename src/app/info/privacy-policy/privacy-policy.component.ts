import { Component, OnInit } from '@angular/core';
import { InfoService } from '../info.service';

@Component({
  selector: 'app-privacy-policy',
  templateUrl: './privacy-policy.component.html',
  styleUrls: ['./privacy-policy.component.css']
})
export class PrivacyPolicyComponent implements OnInit {

  policy: any;
  constructor( private infoservice : InfoService) { 
    this.infoservice.getPrivacyPolicy()
    .subscribe( data => {
      this.policy = data;
      console.log(data);
    })
   }


  ngOnInit() {
  }

}
