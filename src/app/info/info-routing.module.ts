import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContactUsComponent } from './contact-us/contact-us.component'
import { AboutUsComponent } from './about-us/about-us.component';
import { TermsConditionsComponent } from './terms-conditions/terms-conditions.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { FaqComponent } from './faq/faq.component';
import { PaymentPolicyComponent } from './payment-policy/payment-policy.component';
import { DisclaimerComponent } from './disclaimer/disclaimer.component';
import { ReturnRefundComponent } from './return-refund/return-refund.component';
import { ShippingPolicyComponent } from './shipping-policy/shipping-policy.component';

const routes: Routes = [
  {
    path: 'contactus', component:ContactUsComponent
  },
  {
    path: 'aboutus', component:AboutUsComponent
  },
  {
    path: 'termsconditions', component:TermsConditionsComponent
  },
  {
    path: 'privacypolicy', component:PrivacyPolicyComponent
  },
  {
    path: 'paymentpolicy', component:PaymentPolicyComponent
  },
  {
    path: 'disclaimer', component:DisclaimerComponent
  },
  {
    path: 'returnrefund', component:ReturnRefundComponent
  },
  {
    path: 'shipping', component:ShippingPolicyComponent
  },
  {
    path: 'faq', component:FaqComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InfoRoutingModule { }
