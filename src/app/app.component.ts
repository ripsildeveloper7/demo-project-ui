import { Component, ViewChild, ElementRef, AfterViewInit, HostListener, Inject, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { AppSetting } from './config/appSetting';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SharedService } from './shared/shared.service';
import { ConverterPipe } from './shared/nav/converter.pipe';
import { SwPush, SwUpdate } from '@angular/service-worker';
import { HomeService } from '../app/home/home.service';

// import { SwUpdate } from '@angular/service-worker';

export let price;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'etailx';
  priceModel: any;
  subscribeNotification:any;
  readonly VAPID_PUBLIC_KEY = 'BEe66AvTCe_qowysFNV2QsGWzgEDnUWAJq1ytVSXxtwqjcf0bnc6d5USXmZOnIu6glj1BFcj87jIR5eqF2WJFEY';
  subscribeData: any;
  constructor(private route: Router,private HomeService:HomeService, private swPush: SwPush, private sharedService: SharedService, private convertPipe: ConverterPipe, private swUpdate: SwUpdate) {
    this.route.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        (window as any).ga('set', 'page', event.urlAfterRedirects);
        (window as any).ga('send', 'pageview');
        (window as any).fbq('track', 'PageView');
      }
    });
   
  }
  getPrice() {
    this.sharedService.getPriceRate().subscribe(data => {
      this.priceModel = data;
      let holder;
      for (const model of this.priceModel) {
        if (model.currencyCode === 'USD') {
          holder = model.amount;
        } else {
          if (model.currencyCode === 'USD') {
            holder = model.amount;
          }
        }
      }
      price = holder;
      this.convertPipe.setRate(this.priceModel);
      console.log('USD PRICE', this.priceModel);
      }, error => {
        console.log(error);
      });
  }

  ngOnInit() {
    this.subscribe();
  if (this.swUpdate.isEnabled) {
    this.swUpdate.available.subscribe((evt) => {
        console.log('service worker updated');
    });
    this.swUpdate.checkForUpdate().then(() => {
        // noop
    }).catch((err) => {
        console.error('error when checking for update', err);
    });
}}

subscribe() {
 
  this.swPush.requestSubscription({
    serverPublicKey: this.VAPID_PUBLIC_KEY
  })
    .then(sub => {
      console.log(sub, 'sub');
      this.subscribeNotification = sub;
     /*  this.subscribeModel = new Subscribe();
    
      this.subscribeModel.userSubscriptions = sub;*/
      this.HomeService.addPushSubscriberOperation(this.subscribeNotification).subscribe(data =>{
        this.subscribeData = data;
        console.log ('subscribeNotification',this.subscribeData);
      },error => {
       console.log(error);
     });
    })
    .catch(err => console.error('Could not subscribe to notifications', err));
}

}

