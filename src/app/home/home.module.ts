import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BannerComponent } from './banner/banner.component';
import { CategoryContentComponent } from './category-content/category-content.component';
import { ProductComponent } from './product/product.component';
import { Promotion1Component } from './promotion1/promotion1.component';
import { CarouselItemComponent } from './carousel-item/carousel-item.component';
import { Promotion2Component } from './promotion2/promotion2.component';
import { ReviewComponent } from './review/review.component';
import { StripComponent } from './strip/strip.component';
import { SubscribeComponent } from './subscribe/subscribe.component';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home/home.component';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { CarouselItemDirective } from './carousel-item/carousel-item.directive';
import { AdsComponent } from './ads/ads.component';
import { Promotion3Component } from './promotion3/promotion3.component';
import { Promotion4Component } from './promotion4/promotion4.component';
import { Promotion5Component } from './promotion5/promotion5.component';
import { HoverDirective } from './promotion1/hover.directive';
import { SharedModule } from './../shared/shared.module';
import { SmallBannerComponent } from './small-banner/small-banner.component';
import { VideoComponent } from './video/video.component';
import { VideoPipe } from './video/video.pipe';
import { ProductReviewComponent } from './product-review/product-review.component';
import { SubscribeNofificationComponent } from './subscribe-nofification/subscribe-nofification.component';
@NgModule({
  declarations: [BannerComponent,
    CategoryContentComponent, ProductComponent,
    Promotion1Component, Promotion2Component,
    ReviewComponent, StripComponent, SubscribeComponent,
    HomeComponent, CarouselItemComponent, CarouselItemDirective,
    AdsComponent, Promotion3Component, Promotion4Component, Promotion5Component, HoverDirective, SmallBannerComponent, VideoComponent, ProductReviewComponent,VideoPipe, SubscribeNofificationComponent],
  imports: [
    CommonModule,
    SharedModule,
    HomeRoutingModule,
    MatButtonModule,
    MatMenuModule,
    MatCardModule,
    MatFormFieldModule,
    MatIconModule
  ]
})
export class HomeModule { }
