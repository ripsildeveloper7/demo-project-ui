import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HomeService } from '../home.service';
import { AppSetting } from '../../config/appSetting';
import { MatSnackBar, MatDialog, MatDialogConfig } from '@angular/material';
import { Cart } from '../../shared/model/cart.model';
import { WishList } from '../../shared/model/wishList.model';
import { SigninDailogComponent } from 'src/app/shared/signin-dailog/signin-dailog.component';
@Component({
  selector: 'app-promotion5',
  templateUrl: './promotion5.component.html',
  styleUrls: ['./promotion5.component.css']
})
export class Promotion5Component implements OnInit {
  public names = 'flowers';
  public redClass = 'text-danger';
  public greenClass = 'text-success';

  productModel: any;
  title: any;
  description: any;
  productImageUrl: string;
  firstFrame: any;
  secondFrame: any;
  thirdFrame: any;
  forthFrame: any;
  productValue: any;
  productLength: void;
  wishListLength;
  wishList: WishList[];
  wish: WishList;
  userId: string;
  showMobileView = false;
  productStore;
  count = 1;
  isLocalCart = false;
  message;
  isCart = false;
  action;
  addedCart;
  shopModel;
  cartModel;
  constructor(private dialog: MatDialog,private router: Router, private homeService: HomeService, private snackBar: MatSnackBar) {
    this.productImageUrl = AppSetting.productImageUrl;
  }

  ngOnInit() {
    this.getPromotionFivethProduct();
    this.getAllProductTag();
    this.checkLogin();
    this.getProduct();
  }
  submit() {
    this.router.navigate(['/home/subscribe']);
  }
  getPromotionFivethProduct() {
    this.homeService.getSeventhRow().subscribe(data => {
      this.productValue = data;
      this.productLength = data[0].productDetails.length;
      this.productModel = data[0].productDetails;
    }, error => {
      console.log(error);
    });
  }
  getView(id) {
    this.router.navigate(['product/viewsingle/', id]);
  }
  getAllProductTag() {
    this.homeService.getAllProductTag().subscribe(data => {
    }, error => {
      console.log(error);
    });
  }
  checkLoginUser(product) {
    console.log('click', product);
    if (JSON.parse(sessionStorage.getItem('login'))) {
      this.userId = sessionStorage.getItem('userId');
      this.addToWishList(product);
    } else {
      // this.router.navigate(['/account/acc/signin']);
      this.openDialog();
    }
  }
  openDialog() {
    const dialogConfig = new MatDialogConfig();
    this.dialog.open(SigninDailogComponent, {
      panelClass: 'c1',

    });
  }
  addToWishList(product) {
    this.wish = new WishList();
    this.wish.userId = this.userId;
    this.wish.productId = product.productId;
    this.homeService.addToWishList(this.wish).subscribe(data => {
      this.wishList = data;
      this.checkWishListEnable();
    }, err => {
      console.log(err);
    });
  }
  checkLogin() {
    if (JSON.parse(sessionStorage.getItem('login'))) {
      this.userId = sessionStorage.getItem('userId');
      this.getwishList();
    } else {

    }
  }
  getwishList() {
    this.wish = new WishList();
    this.wish.userId = this.userId;
    this.homeService.getWishList(this.wish).subscribe(data => {
      this.wishList = data;
      this.wishListLength = data;
      sessionStorage.setItem('wishqty', this.wishListLength.length);
      this.checkWishListEnable();
    }, err => {
      console.log(err);
    });
  }
  checkWishListEnable() {
    const wishlist: any = this.wishList.map(a => a.productIds);
    sessionStorage.setItem('wislistLength', wishlist.length);
    const obj = {};
    for (const item of wishlist) {
      if (!obj[item.proId]) {
        const element = item.proId;
        obj[element] = true;
      }
    }
    for (const prod of this.productModel) {
      if (obj[prod.productId]) {
        prod.wishList = true;
      } else {
        prod.wishList = false;
      }
    }
  }
  /*
  getProduct(id) {
    this.homeService.getProductPosition(id).subscribe(data => {
      this.productModel = data;
    }, error => {
      console.log(error);
    });
  } */
  getProduct() {
    this.homeService.getAllProduct().subscribe(data => {
      this.productStore = data;
      console.log('all product', this.productStore);
    }, error => {
      console.log(error);
    });
  }
  addToBag(product) {
    this.productModel.forEach(element => {
      if (element.productId === product) {
        element.display = true;
      } else {
        element.display = false;
      }
    });
    console.log('afterProduct', this.productModel);
  }
  closeToBag(product) {
    this.productModel.forEach(element => {
      if (element.productId === product) {
        element.display = false;
      }
    });
  }
  skuProductAddToCart(productId, skuItem) {
    const userId = sessionStorage.getItem('userId');
    if (JSON.parse(sessionStorage.getItem('login'))) {
      this.getUserCart(userId, productId, skuItem);
      /* this.addToCartServer(userId, productId, skuItem); */
    } else {
      this.addToCartLocal(productId, skuItem);
      this.closeToBag(productId._id);
    }
  }
  addToCartLocal(product, skuItem) {
    const cartLocal = JSON.parse(sessionStorage.getItem('cart')) || [];
    if (cartLocal.length === 0) {
      const totalItem: any = [];
      const currentProduct: any = [];
      currentProduct.push(product);
      console.log('checkCArtLength', currentProduct);
      const item = {
        productId: product._id,
        sku: skuItem.sku,
        qty: this.count
      };
      const cart = {
        items: item,
        cart_product: currentProduct
      };
      console.log('chel2', cart);
      totalItem.push(cart);
      this.message = 'Product Added To Cart';
      console.log(totalItem);
      sessionStorage.setItem('cart', JSON.stringify(totalItem));
      this.snackBar.open(this.message, this.action, {
        duration: 500,
      });
    } else {
      const totalItem: any = [];
      const currentProduct: any = [];
      currentProduct.push(product);
      const item = {
        productId: product._id,
        sku: skuItem.sku,
        qty: this.count
      };
      const cart = {
        items: item,
        cart_product: currentProduct
      };
      totalItem.push(cart);
      this.isLocalCart = false;
      totalItem.map(element => {
        if (cartLocal.find(s => s.items.sku === element.items.sku)) {
          /* const localSame = cartLocal.find(s => s.items.sku === element.items.sku);
          localSame.items.qty += element.items.qty; */
          this.isLocalCart = true;
        }
        if (this.isLocalCart !== true) {
          cartLocal.push(element);
        }
      });
      if (this.isLocalCart) {
        this.message = 'Already Added To Your Cart';
        this.snackBar.open(this.message, this.action, {
          duration: 500,
        });
      } else {
      this.message = 'Product Added To Cart';
      sessionStorage.setItem('cart', JSON.stringify(cartLocal));
      this.snackBar.open(this.message, this.action, {
        duration: 500,
      });
    }
    }
  }
  getUserCart(userId, productId, skuItem) {
    this.homeService.shoppingUser(userId).subscribe(data => {
      this.isCart = false;
      this.addedCart = data;
      console.log(data);
      this.addedCart.forEach(element => {
        if (element.items.sku === skuItem.sku) {
          this.isCart = true;
        }
      });
      if (this.isCart) {
        this.message = 'Already Added To Your Cart';
        this.snackBar.open(this.message, this.action, {
          duration: 500,
        });
      } else {
        this.addToCartServer(userId, productId, skuItem);
      }
      console.log(this.addedCart);
    }, error => {
      console.log(error);
    });

  }
  addToCartServer(userId, product, skuItem) {
    const totalItem: any = [];
    const cart = {
      productId: product._id,
      sku: skuItem.sku,
      qty: 1
    };
    totalItem.push(cart);
    this.cartModel = new Cart();
    this.cartModel.userId = userId;
    this.cartModel.items = totalItem;
    this.homeService.addToCart(this.cartModel).subscribe(data => {
      this.shopModel = data;
      sessionStorage.setItem('cartqty', this.shopModel.length);
      this.message = 'Product Added To Cart';
      this.snackBar.open(this.message, this.action, {
        duration: 500,
      });
      this.closeToBag(product._id);
    }, error => {
      console.log(error);
    });
  }
  getViewAll() {
    const tempData = this.productModel.map(e => e.productId);
    this.router.navigate(['/product/supercategory/promotion/', this.productValue[0]._id], {queryParams: {'array': tempData, 'name': this.productValue[0].title, 'desc': this.productValue[0].description}});
  }
}
