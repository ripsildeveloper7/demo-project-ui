import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BannerComponent } from './banner/banner.component';
import { HomeComponent } from './home/home.component';
import { SubscribeComponent } from './subscribe/subscribe.component';
import { SmallBannerComponent } from './small-banner/small-banner.component';
import {SubscribeNofificationComponent} from './subscribe-nofification/subscribe-nofification.component';

const routes: Routes = [
  {
    path: 'banner', component: BannerComponent,
  },
  {
    path: 'welcome', component: HomeComponent,
  },
  {
    path: 'subscribe', component: SubscribeComponent
  },
  {
    path: 'smallbanner', component:SmallBannerComponent
  },
  {
    path: 'subscribeNotification', component:SubscribeNofificationComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
