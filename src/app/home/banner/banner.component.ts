import { Component, OnInit, AfterViewInit, HostListener } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Banner } from './banner.model';
import { HomeService } from '../home.service'; 
import { NavComponent } from '../../shared/nav/nav.component';
import { SharedService } from 'src/app/shared/shared.service';
import { animate, query, transition, style, trigger, group, state } from '@angular/animations';
// import 'bootstrap';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css'],
  animations: [
    trigger('slideAnimation', [
      transition(':increment , :decrement', group([
        query(':enter', [
          style({
            transform: 'translateX(100%)'
          }),
          animate('0.5s ease-out',)
        ])
      ])
    )
  ])
]
})
       
      
      
      
export class BannerComponent implements OnInit, AfterViewInit {
  banner;
  bannerModel: any;
  showMobileView= false;
  interval : any;
  constructor(private router: Router, private homeService: HomeService,private sharedservice: SharedService) { }
  ngOnInit() {
    this.getBannersDetails();
    this.repeat();
  }
  ngAfterViewInit() {
    this.getWindowSize();
  }
  getWindowSize() {
    if (window.screen.width > 1200) {
      this.showMobileView = false;
    } else {
      this.showMobileView = true;
    }
  }
  swiper(){
    this.sharedservice.callMethod();
  }
  getBannersDetails() {
    this.homeService.getAllBanner().subscribe(data => {
      this.bannerModel = data;
      console.log(this.bannerModel)
    }, err => {
      console.log(err);
    });
  }


  currentIndex = 0;
  
  setCurrentSlideIndex(index) {
      this.currentIndex = index;
    }
  isCurrentSlideIndex(index) {
      return this.currentIndex === index;
  }
  
  
    slides = [
      
      {image: '../../assets/images/Launch Banner (2).jpg', description: 'Image 00'},
      // {image: '../../assets/images/Dushara & Navrathri Banner (1).jpg', description: 'Image 01'},
      {image: '../../assets/images/Banner.jpg', description: 'Image 02'}
    ];
  
    prevSlide() {
      this.currentIndex = (this.currentIndex > 0) ? --this.currentIndex : this.bannerModel.length - 1;
    }
  nextSlide() {
      this.currentIndex = (this.currentIndex < this.bannerModel.length - 1) ? ++this.currentIndex : 0;
  }
  repeat(){
    this.interval = setInterval(() =>{
      this.nextSlide();
    }, 4000)
  }


  defaultTouch = { x: 0, y: 0, time: 0 };

  @HostListener('touchstart', ['$event'])
  //@HostListener('touchmove', ['$event'])
  @HostListener('touchend', ['$event'])
  @HostListener('touchcancel', ['$event'])
  handleTouch(event) {
      let touch = event.touches[0] || event.changedTouches[0];

      // check the events
      if (event.type === 'touchstart') {
          this.defaultTouch.x = touch.pageX;
          this.defaultTouch.y = touch.pageY;
          this.defaultTouch.time = event.timeStamp;
      } else if (event.type === 'touchend') {
          let deltaX = touch.pageX - this.defaultTouch.x;
          let deltaY = touch.pageY - this.defaultTouch.y;
          let deltaTime = event.timeStamp - this.defaultTouch.time;

          // simulte a swipe -> less than 500 ms and more than 60 px
          if (deltaTime < 500) {
              // touch movement lasted less than 500 ms
              if (Math.abs(deltaX) > 60) {
                  // delta x is at least 60 pixels
                  if (deltaX > 0) {
                      this.doSwipeRight(event);
                  } else {
                      this.doSwipeLeft(event);
                  }
              }

              if (Math.abs(deltaY) > 60) {
                  // delta y is at least 60 pixels
                  if (deltaY > 0) {
                      this.doSwipeDown(event);
                  } else {
                      this.doSwipeUp(event);
                  }
              }
          }
      }
  }

  doSwipeLeft(event) {
      console.log('swipe left', event);
      this.prevSlide();
  }

  doSwipeRight(event) {
      console.log('swipe right', event);
      // this.nav.toggleNavbar();
      this.nextSlide();
  }

  doSwipeUp(event) {
      console.log('swipe up', event);
  }

  doSwipeDown(event) {
      console.log('swipe down', event);
  }
}
