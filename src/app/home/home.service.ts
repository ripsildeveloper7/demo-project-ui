import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { AppSetting } from '../config/appSetting';
import { Banner } from './banner/banner.model';
import { SuperCategory } from './category-content/category.model';
import { Promotion } from './promotion1/promotion.model';
import { ADSModel } from './ads/ads.model';
import { WishList } from '../shared/model/wishList.model';
import { Cart } from '../shared/model/cart.model';
@Injectable({
  providedIn: 'root'
})
export class HomeService {
  serviceUrl: string = AppSetting.productServiceUrl;
  contentUrl: string = AppSetting.contentServiceUrl;
  instagramUrl: string = AppSetting.instagramUrl;
  marketingServiceUrl: string = AppSetting.marketingServiceUrl;
  productServiceUrl: string = AppSetting.productServiceUrl;
  customerServiceUrl :string =AppSetting.customerServiceUrl
  constructor(private httpClient: HttpClient) { }

  // save push notification

  addPushSubscriberOperation(data):Observable<any> {
    const customerServiceUrl = 'pushnotificationsubscribe';
    const url: string = this.customerServiceUrl + customerServiceUrl;
    return this.httpClient.post<any>(url,data);
  }
  // get banners
  getAllBanner(): Observable<any> {
    const categoryUrl = 'getbannerforui';
    const url: string = this.contentUrl + categoryUrl;
    return this.httpClient.get<Banner>(url);
  }
  getSuperCategory(): Observable<any> {
    const categoryUrl = 'categoryDetails';
    const url: string = this.serviceUrl + categoryUrl;
    return this.httpClient.get<SuperCategory>(url);
  }

  getNewProduct(): Observable<any> {
    const categoryUrl = 'getnewproduct';
    const url: string = this.serviceUrl + categoryUrl;
    return this.httpClient.get<any>(url);
  }

  getAds(): Observable<any> {
    const categoryUrl = 'getpublishedads';
    const url: string = this.contentUrl + categoryUrl;
    return this.httpClient.get<ADSModel>(url);
  }
  getCategory(): Observable<any> {
    const categoryUrl = 'getsupercategorybystatus';
    const url: string = this.serviceUrl + categoryUrl;
    return this.httpClient.get<SuperCategory>(url);
  }
  getAllProductTag(): Observable<any> {
    const categoryUrl = 'getproducttag';
    const url: string = this.serviceUrl + categoryUrl;
    return this.httpClient.get<any>(url);
  }
  getFirstTag(): Observable<any> {
    const categoryUrl = 'getfirsttag';
    const url: string = this.serviceUrl + categoryUrl;
    return this.httpClient.get<any>(url);
  }
  getSecondTag(): Observable<any> {
    const categoryUrl = 'getsecondtag';
    const url: string = this.serviceUrl + categoryUrl;
    return this.httpClient.get<any>(url);
  }
  getProductPosition(id): Observable<any> {
    const categoryUrl = 'getproductposition/';
    const url: string = this.serviceUrl + categoryUrl + id;
    return this.httpClient.get<any>(url);
  }
  getSecondRow(): Observable<any> {
    const categoryUrl = 'getpublishedsecondrow';
    const url: string = this.contentUrl + categoryUrl;
    return this.httpClient.get<any>(url);
  }
  getThirdRow(): Observable<any> {
    const categoryUrl = 'getpublishedthirdrow';
    const url: string = this.contentUrl + categoryUrl;
    return this.httpClient.get<any>(url);
  }
  getFifthRow(): Observable<any> {
    const productUrl = 'getpublishedfifthrow';
    const url: string = this.contentUrl + productUrl;
    return this.httpClient.get<any>(url);
  }
  getSixthRow(): Observable<any> {
    const productUrl = 'getpublishedsixthrow';
    const url: string = this.contentUrl + productUrl;
    return this.httpClient.get<any>(url);
  }
  getSeventhRow(): Observable<any> {
    const productUrl = 'getpublishedseventhrow';
    const url: string = this.contentUrl + productUrl;
    return this.httpClient.get<any>(url);
  }
  gethomepageVideo():Observable<any>{
    const contentUrl = 'gethomepageVideo';
    const url:string = this.contentUrl + contentUrl;
    return this.httpClient.get<any>(url);

  }

  gethompagePublishedVideo():Observable<any>{
    const contentUrl = 'getPublishedhomepageVideo';
    const url:string = this.contentUrl + contentUrl;
    return this.httpClient.get<any>(url);
  }

  getBlogDetails(): Observable<any> {
    const url='http://blog.ucchalfashion.com/wp-json/wp/v2/posts?per_page=100';
   // const url='http://blog.rinteger.com/wp-json/';
    return this.httpClient.get<any>(url);
       }
       getBlogMedia(): Observable<any> {
        const url='http://blog.ucchalfashion.com/wp-json/wp/v2/media?per_page=100';
       // const url='http://blog.rinteger.com/wp-json/';
        return this.httpClient.get<any>(url);
           }

           getSingleProductsWithBrand(id): Observable<any> {
            const categoryUrl = 'singleproductwithbarnd/';
            const url: string = this.productServiceUrl + categoryUrl + id;
            return this.httpClient.get<any>(url);
          }
        
  // get footer
  /*   getFooterDetails(): Observable<any> {
      const categoryUrl = 'footerDetails';
      const url: string = this.serviceUrl + categoryUrl;
      return this.httpClient.get<Footer>(url);
    }
    // get hot products
    getHotProducts(): Observable<any> {
      const categoryUrl = 'ads';
      const url: string = this.serviceUrl + categoryUrl;
      return this.httpClient.get<Footer>(url);
    }
    // promotions
    getPromotions(): Observable<any> {
      const categoryUrl = 'promotions';
      const url: string = this.serviceUrl + categoryUrl;
      return this.httpClient.get<Footer>(url);
    } */

    // instagram pics
    getInstagramPics(): Observable<any> {
 return this.httpClient.jsonp<any>(this.instagramUrl, 'callback');
    }
    // wishList
    addToWishList(wish): Observable<WishList[]> {
      const pathUrl = 'wishlist/';
      const url: string = this.serviceUrl + pathUrl + wish.userId;
      return this.httpClient.put<WishList[]>(url, wish);
    }
    getWishList(wish): Observable<WishList[]> {
      const pathUrl = 'getwishlist/';
      const url: string = this.serviceUrl + pathUrl + wish.userId;
      return this.httpClient.get<WishList[]>(url);
    }
    getAllProduct(): Observable<any> {
      const categoryUrl = 'product';
      const url: string = this.serviceUrl + categoryUrl;
      return this.httpClient.get<any>(url);
    }
    shoppingUser(userId) {
      const shoppingUrl = 'findcart/';
      const url: string = this.serviceUrl + shoppingUrl + userId;
      return this.httpClient.get<Cart>(url);
    }
    addToCart(cart): Observable<Cart> {
      const cartUrl = 'cart';
      const url: string = this.serviceUrl + cartUrl;
      return this.httpClient.post<Cart>(url, cart);
    }
    getAllDiscount(): Observable<any> {
      const pathUrl = 'getdiscount';
      const url: string = this.marketingServiceUrl + pathUrl;
      return this.httpClient.get<any>(url);
    }
    getAllCategoryBanner(): Observable<any> {
      const pathUrl = 'getcategorybanner';
      const url: string = this.productServiceUrl + pathUrl;
      return this.httpClient.get<any>(url);
    }
    getIncrementRate(): Observable<any> {
      const productUrl = 'getincrementrate';
      const url: string = this.productServiceUrl + productUrl;
      return this.httpClient.get<any>(url);
    }
    getPublishedReview(): Observable<any> {
      const productUrl = 'getpublishedreview';
      const url: string = this.productServiceUrl + productUrl;
      return this.httpClient.get<any>(url);
    }
}
