import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubscribeNofificationComponent } from './subscribe-nofification.component';

describe('SubscribeNofificationComponent', () => {
  let component: SubscribeNofificationComponent;
  let fixture: ComponentFixture<SubscribeNofificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubscribeNofificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubscribeNofificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
