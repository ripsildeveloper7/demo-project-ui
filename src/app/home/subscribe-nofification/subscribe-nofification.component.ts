import { Component, OnInit , Inject} from '@angular/core';
import { SwPush, SwUpdate } from '@angular/service-worker';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { HomeService } from '../home.service';
// import {SubscribeService} from './subscribe.service';
// import {Subscribe} from './subscribe.model';
@Component({
  selector: 'app-subscribe-nofification',
  templateUrl: './subscribe-nofification.component.html',
  styleUrls: ['./subscribe-nofification.component.css']
})
export class SubscribeNofificationComponent implements OnInit {

  // subscribeModel: Subscribe;
  mobNum: number;
  mobNo;
  mobileNo;
  subscribeForm: FormGroup;
  userName;
  subscribeNotification:any;
  readonly VAPID_PUBLIC_KEY = 'BEe66AvTCe_qowysFNV2QsGWzgEDnUWAJq1ytVSXxtwqjcf0bnc6d5USXmZOnIu6glj1BFcj87jIR5eqF2WJFEY';
  subscribeData: any;
  constructor( private fb: FormBuilder, private router: Router,private HomeService: HomeService,
    private swUpdate: SwUpdate, private swPush: SwPush) { }

  ngOnInit() {
    this.createForm();
    this.subscribe();
  }
  createForm() {
    this.subscribeForm = this.fb.group({
      // mobileNumber: [ mobileNumber],
      name: ['']
    });
  }

  subscribe() {
 
     this.swPush.requestSubscription({
       serverPublicKey: this.VAPID_PUBLIC_KEY
     })
       .then(sub => {
         console.log(sub, 'sub');
         this.subscribeNotification = sub;
        /*  this.subscribeModel = new Subscribe();
       
         this.subscribeModel.userSubscriptions = sub;*/
         this.HomeService.addPushSubscriberOperation(this.subscribeNotification).subscribe(data =>{
           this.subscribeData = data;
           console.log ('subscribeNotification',this.subscribeData);
         },error => {
          console.log(error);
        });
       })
       .catch(err => console.error('Could not subscribe to notifications', err));
   }

   saveSubscribe(){
      
  }
}
