export class LehengaMeasurement {
    userId: string;
    typeName: string;
    typeDescription: string;
    price: number;
    discount: number;
    // Choli Measurement
    aroundBust: number;
    aroundAboveWaist: number;
    choliLength: number;
    frontNeckStyle: string;
    backNeckStyle: string;
    sleeveStyle: string;
    choliClosingSide: string;
    choliClosingWith: string;
    lining: string;
    // Lehenga Measurement
    aroundHip: number;
    aroundWaist: number;
    lehengaLength: number;
    lehengaClosingSide: string;
    lehengaClosingWith: string;
    specialInstruction: string;
    measurementName: string;
    addedDate: string;
    serviceId: string;
    productId: string;
    measurementId: string;
}
