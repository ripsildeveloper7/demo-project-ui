export class WishList {
    _id: string;
    userId: string;
    productId: string;
    productIds: {proId: string, INTsku: string};
    wishListProduct: any;
    INTsku: string;
    price: number
}
