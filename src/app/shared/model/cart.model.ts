import { Size } from './size.model';
export class Cart {
    userId: string;
    items: [{productId: string, sku: string, qty: number,
        INTsku: string,
        bodyHeight: string,
        tailoringService: boolean,
        isMeasurement: boolean,
        isUnstitched: boolean,
        serviceId: string}];
    /* tailoringService: [{type: string,
                        mount: number,
                        selectedSize: string,
                        discount: number,
                        serviceName: string}]; */
}
