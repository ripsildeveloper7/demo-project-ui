import { Size } from '../model/size.model';
export class ProductCart {
    productId: string;
    sizeValue: [Size];
}