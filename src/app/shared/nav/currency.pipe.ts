/* import { Pipe, PipeTransform } from '@angular/core';
import { Rates } from './rates.model';
import { CurrencyPipe } from '@angular/common';

@Pipe({
  name: 'mycurrency'
})

export class MyCurrencyPipe extends CurrencyPipe implements PipeTransform {

  transform(value: any, currencyCode?: string, display?: 'code' | 'symbol' | 'symbol-narrow' | string | boolean, digitsInfo?: string, locale?: string): string | null {
    return super.transform(
      value,
      currencyCode || 'USD',
      display || 'symbol',
      digitsInfo
    );
  }

}
 */
import { Pipe, PipeTransform, LOCALE_ID, Inject } from '@angular/core';
import { formatCurrency, getLocaleCurrencySymbol, formatNumber } from '@angular/common';

@Pipe({
  name: 'localeCurrency'
})
export class MyCurrencyPipe  implements PipeTransform {

  constructor(
    @Inject( LOCALE_ID ) private localeId: string
  ) {}

  transform(value: number, currencyCode?: string, digitInfo?: string): any {
    
      return formatCurrency(value, this.localeId, getLocaleCurrencySymbol( this.localeId ), currencyCode, digitInfo);
    
    
    
  }
}

