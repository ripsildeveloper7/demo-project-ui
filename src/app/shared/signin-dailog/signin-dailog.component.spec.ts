import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SigninDailogComponent } from './signin-dailog.component';

describe('SigninDailogComponent', () => {
  let component: SigninDailogComponent;
  let fixture: ComponentFixture<SigninDailogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SigninDailogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SigninDailogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
