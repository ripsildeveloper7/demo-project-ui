import { Component, OnInit, AfterViewInit, Input, Output, EventEmitter} from '@angular/core';
import { CartService } from './../cart.service';

import { Product } from '../../shared/model/product.model';
import { Cart } from './../../shared/model/cart.model';

import { WishList } from './../../shared/model/wishList.model';
import { Router } from '@angular/router';
import { AppSetting } from '../../config/appSetting';
import { MatSnackBar, MatDialogConfig, MatDialog } from '@angular/material';
import { FormControl, FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { match } from 'minimatch';
import { ProductService } from '../../product/product.service';
import { SigninDailogComponent } from 'src/app/shared/signin-dailog/signin-dailog.component';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit, AfterViewInit {
  @Input() productModel: Product;
  show = false;
  holder:any;
  wish: WishList;
  wishList: WishList[];
  shoppinBagForm: FormGroup;
  shopModel: any = [];
  cartModel: Cart;
  userId;
  // holder:any;
  subTotal = 0;
  action;
  localImageUrlView = true;
  // showMobileView = false;
  totalItems = 0;
  noPrductAdd = false;
  productImageUrl: string = AppSetting.productImageUrl;
  discountStore: any;
  checkPoint1 = false;
  checkPoint2 = false;
  checkPoint3 = false;
  checkPoint4 = false;
  checkPoint5 = false;
  checkPoint6 = false;
  checkPoint7 = false;
  checkPoint8 = false;
  checkPoint9 = false;
  displayMobile = false;
  isFirstTimeCoupon = false;
  isFlexibleCoupon = false;
  checkSoldOutEnable = false;
  firstTimeCoupon: any;
  flexibleCoupon: any;
  message;
  customerData: {
    customerId: string,
    couponCode: string
  };
  couponType;
  couponValue;
  totalAmount = 0;
  readyToShip: any;
  serviceData: any;
  firstPurchase = false;
  totalShipement: any;
  incRateModel: any;
  sizInc: any;
  constructor(private dialog: MatDialog, private cartService: CartService, private router: Router,
              private matSnackBar: MatSnackBar, private productService: ProductService,
              private fb: FormBuilder) {

  }

  ngOnInit() {
    this.createForm();
    if (JSON.parse(sessionStorage.getItem('login'))) {
      this.userId = sessionStorage.getItem('userId');
      this.localImageUrlView = true;
      this.shoppingCartUser(this.userId);
    } else {
      this.localImageUrlView = false;
      this.shopModel = JSON.parse(sessionStorage.getItem('cart')) || [];
      this.shopModel.forEach((val) => {
        val.showDiv = false;
      });
      for (const cart of this.shopModel) {
        for (const product of cart.cart_product[0].child) {
         /*  if (product.vp < 5600) {
            product.shippingCharege = 560;
          } else {
            product.shippingCharege = 0;
          } */
          product.serviceAmount = 0;
          product.serviceDiscount = 0;
        }
      }
      /* console.log('cart', this.shopModel); */
      this.getIncrementRate();
      /* this.getDiscount(); */
     /*  this.total(); */
    }
   /*  this.getFirstTimeSignUpCoupon(); */
    this.getFlexibleCoupon();
    this.getReadyToMeasure();
  }
  ngAfterViewInit() {
    this.getWindowSize();
  }
  getReadyToMeasure() {
    this.productService.getAllShipmentSetting().subscribe(data => {
     this.readyToShip = data[0];
     console.log(this.readyToShip);
     this.shopModel.forEach(element => {
       element.cart_product[0].child.forEach(el => {
         if (el.ttsPortol <= this.readyToShip.daysCount) {
           el.ttsPortalenable = true;
         } else {
          el.ttsPortalenable = false;
         }
       });
     });
     console.log('bb', this.shopModel);
  });
}
getTtsEnable(element) {
  if (element.ttsPortol <= this.readyToShip.daysCount) {
    return true;
  }
}
  getWindowSize() {
    if (window.screen.width > 900) {
      this.displayMobile = false;
    } else {
      this.displayMobile = true;
    }
  }
  getIncrementRate() {
    this.cartService.getIncrementRate().subscribe(data => {
      this.incRateModel = data;
      this.discountCalculation();
    }, error => {
      console.log(error);
    });
  }
  discountCalculation() {
    this.totalShipement = 0;
    for (const cart of this.shopModel) {
      if (typeof cart.items.INTsku === 'number') {
        cart.items.INTsku = cart.items.INTsku.toString();
      }
      for (const product of cart.cart_product[0].child) {
        if (typeof product.INTsku === 'number') {
          product.INTsku =  product.INTsku.toString();
        }
       /*  if (product.vp < 5600) {
          product.shippingCharege = 560;
        } else {
          product.shippingCharege = 0;
        } */
        product.price = (product.sp * ( 100 + this.incRateModel[0].incRate) / 100) ;
        product.subPrice = product.price;
        const discount = 100 - product.discount;
        const totalPrice = (product.sp * ( 100 + this.incRateModel[0].incRate) / 100) * (100 / discount);
        const savePrice = totalPrice - (product.sp * ( 100 + this.incRateModel[0].incRate) / 100);
        product.savePrice = savePrice;
        product.totalPrice = totalPrice;
       /*  this.totalShipement += product.shippingCharege; */
      }
    }
    console.log(this.shopModel);
    if (JSON.parse(sessionStorage.getItem('login'))) {
      this.getAllMeasurmentByUser();
    } else {
      for (const cart of this.shopModel) {
        if (cart.items.tailoringService === true) {
      for (const product of cart.cart_product[0].child) {
        if (cart.items.isUnstitched === true) {
         /*  if (product.vp < 2000) {
            product.shippingCharege = 499.8;
          } else {
            product.shippingCharege = 0;
          } */
          product.serviceActive = true;
          product.serviceType = 'Unstitched Fabric';
          product.serviceDiscount = 0;
          product.serviceAmount = 0;
          product.serviceDiscount = 0;
          product.price = (product.sp * 110 / 100);
        }
      }
    }
  }
      /* this.total(); */
      this.getAllSizeWiseIncrement();
    }
    this.checkQty();
    /* this.getAllReadyToWear(); */
  }
  checkQty() {
    this.shopModel.forEach((element) => {
      element.quantityCheck = 0;
      if (element.cart_product.length > 0) {
        this.eachSizeCheck(element, element.cart_product[0], element.items.INTsku);
      }
    });
  }

  eachSizeCheck(element, product, sku) {
    product.child.forEach((variantElement, index) => {
      if (sku === variantElement.INTsku) {
        element.quantityCheck += variantElement.quantity;
      }
    });
  }
  getAllMeasurmentByUser() {
    this.cartService.getMeasurementbyUser().subscribe(data => {
      this.serviceData = data;

      this.checkService();
      console.log(data);
    }, error => {
      console.log(error);
    });
  }
 /*  getAllReadyToWear() {
    this.cartService.getAllReadyToWear().subscribe(data => {
      this.serviceData = data;
      this.checkService();
    }, error => {
      console.log(error);
    });
  } */
  checkService() {
    for (const cart of this.shopModel) {
      if (cart.items.tailoringService === true) {
        if (this.serviceData.length !== 0) {
        this.serviceData.forEach(element => {
          if (element._id === cart.items.serviceId) {
            for (const product of cart.cart_product[0].child) {
              /* if (product.vp < 2000) {
                product.shippingCharege = 499.8;
              } else {
                product.shippingCharege = 0;
              } */
              product.serviceActive = true;
              if (cart.items.isMeasurement === true) {
                product.serviceType = 'Made to Measure';
                product.serviceId = element._id;
                product.serviceName = element.serviceName;
                product.serviceAmount = element.price;
                product.serviceDiscount = element.discount;
                product.price = (product.sp * 110 / 100) + element.price * (100 - element.discount) / 100 ;
              } else if (cart.items.isUnstitched === true) {
                product.serviceType = 'Unstitched Fabric';
                product.serviceDiscount = 0;
                product.serviceAmount = 0;
                product.serviceDiscount = 0;
                product.price = (product.sp * 110 / 100);
              }
             /*  product.serviceType = 'Ready to Wear'; */
            }
          } else {
            for (const product of cart.cart_product[0].child) {
              if (cart.items.isUnstitched === true) {
                product.serviceActive = true;
                product.serviceType = 'Unstitched Fabric';
                product.serviceDiscount = 0;
                product.serviceAmount = 0;
                product.serviceDiscount = 0;
                product.price = (product.sp * 110 / 100);
              }
            }
          }
        });
      } else {
          for (const product of cart.cart_product[0].child) {
           /*  if (product.vp < 2000) {
              product.shippingCharege = 499.8;
            } else {
              product.shippingCharege = 0;
            } */
            if (cart.items.isUnstitched === true) {
              product.serviceActive = true;
              product.serviceType = 'Unstitched Fabric';
              product.serviceAmount = 0;
              product.serviceDiscount = 0;
              product.price = (product.sp * 110 / 100);
            }
          }
      }
      } else {
        for (const product of cart.cart_product[0].child) {
              product.serviceAmount = 0;
              product.serviceDiscount = 0;
            }
        continue;
      }
    }
    /* this.discountCalculation(); */
    this.getAllSizeWiseIncrement();
  
  }
  createForm() {
    this.shoppinBagForm = this.fb.group({
      promoCode: ['']
    });
  }
  orderPlaced() {
    this.matSnackBar.open('order Placed Successfully', this.action, {
      duration: 2000,
    });
    this.router.navigate(['home/welcome']);
  }

  actionPlus(product, INTsku) {
    if (this.localImageUrlView) {
      this.actionServerPlus(product, INTsku);
    } else {
      this.actionLocalPlus(product, INTsku);
    }
  }
  actionServerPlus(product, selectedCart) {
    this.userId = sessionStorage.getItem('userId');
    if (selectedCart.tailoringService === true) {
      const totalItem: any = [];
      const cart: any = {
        productId: product,
        INTsku: selectedCart.INTsku.toString(),
        qty: 1,
        tailoringService: true,
        bodyHeight: selectedCart.bodyHeight,
        serviceId: selectedCart.serviceId,
        isMeasurement: selectedCart.isMeasurement,
        isUnstitched: selectedCart.isUnstitched
        };
      totalItem.push(cart);
      this.cartModel = new Cart();
      this.cartModel.userId = this.userId;
      this.cartModel.items = totalItem;
    } else {
      const totalItem: any = [];
      const cart: any = {
        productId: product,
        INTsku: selectedCart.INTsku.toString(),
        bodyHeight: selectedCart.bodyHeight,
        qty: 1,
        tailoringService: false
      };
      totalItem.push(cart);
      this.cartModel = new Cart();
      this.cartModel.userId = this.userId;
      this.cartModel.items = totalItem;
    }
    this.cartService.addToCart(this.cartModel).subscribe(data => {
      this.shopModel = data;
      this.getIncrementRate();
      /* this.total(); */
      /* this.getDiscount(); */
    }, error => {
      console.log(error);
    });
  }
  actionLocalPlus(item, selectedCart) {
    if (selectedCart.tailoringService === true) {
      const localSame = this.shopModel.find(s => s.items.INTsku === selectedCart.INTsku && s.items.serviceId === selectedCart.serviceId && s.items.bodyHeight === selectedCart.bodyHeight);
      localSame.items.qty++;
    } else {
      const localSame = this.shopModel.find(s => s.items.INTsku === selectedCart.INTsku && s.items.tailoringService === false && s.items.bodyHeight === selectedCart.bodyHeight);
      localSame.items.qty++;
    }
    /* const localSame = this.shopModel.find(s => s.items.INTsku === selectedCart.INTsku); */
    /* const localSame = this.shopModel.find(s => s.items.INTsku === sku);
    localSame.items.qty++; */
    sessionStorage.setItem('cart', JSON.stringify(this.shopModel));
    this.getIncrementRate();
    /* this.getDiscount(); */
  }

  actionMinus(product, sku) {
    /* console.log(sku); */
    if (JSON.parse(sessionStorage.getItem('login'))) {
      this.actionServerMinus(product, sku);
    } else {
      this.actionLocalMinus(product, sku);
    }
  }
  continueShopping() {
    this.router.navigate(['product/viewproduct']);
  }
  actionServerMinus(product, selectedCart) {
    this.userId = sessionStorage.getItem('userId');
    if (selectedCart.tailoringService === true) {
      const cart: any = {
        productId: product,
        INTsku: selectedCart.INTsku,
        qty: 1,
        bodyHeight: selectedCart.bodyHeight,
        tailoringService: true,
        serviceId: selectedCart.serviceId,
        isMeasurement: selectedCart.isMeasurement,
        isUnstitched: selectedCart.isUnstitched
      };
      this.cartModel = new Cart();
      this.cartModel.userId = this.userId;
      this.cartModel.items = cart;
    } else {
      const cart: any = {
        productId: product,
        INTsku: selectedCart.INTsku,
        bodyHeight: selectedCart.bodyHeight,
        qty: 1,
        tailoringService: false,
      };
      this.cartModel = new Cart();
      this.cartModel.userId = this.userId;
      this.cartModel.items = cart;
    }
    this.cartService.addToCartDecrement(this.cartModel).subscribe(data => {
      this.shopModel = data;
      /* this.getDiscount(); */
      this.getIncrementRate();
      /* this.total(); */
    }, error => {
      console.log(error);
    });
  }

  actionLocalMinus(product, selectedCart) {
    if (selectedCart.tailoringService === true) {
      const localSame = this.shopModel.find(s => s.items.INTsku === selectedCart.INTsku && s.items.serviceId === selectedCart.serviceId && s.items.bodyHeight === selectedCart.bodyHeight );
      localSame.items.qty--;
    } else {
      const localSame = this.shopModel.find(s => s.items.INTsku === selectedCart.INTsku && s.items.tailoringService === false && s.items.bodyHeight === selectedCart.bodyHeight);
      localSame.items.qty--;
    }
    sessionStorage.setItem('cart', JSON.stringify(this.shopModel));
    /* const localSame = this.shopModel.find(s => s.items.INTsku === sku);
    localSame.items.qty--;
    sessionStorage.setItem('cart', JSON.stringify(this.shopModel)); */
    /* this.getDiscount(); */
    this.getIncrementRate();
  }

  shoppingCartUser(userId) {
    this.cartService.shoppingUser(userId).subscribe(data => {
      this.shopModel = data;
     /*  this.shopModel.forEach(element => {
        element
      }) */
      /* this.getDiscount(); */
     /*  this.getAllMeasurmentByUser(); */
      this.getIncrementRate();
      /* this.total(); */
    }, err => {
      console.log(err);
    });
  }
  checkFirstTimeShopping() {
    const userId = sessionStorage.getItem('userId');
    this.cartService.checkCouponForFirstCustomer(userId).subscribe(data => {
      if (data.length === 0) {
       /*  this.couponCalculation(firstCoupon); */
       this.firstPurchase = true;
       this.applyCalculation();
      }
    }, error => {
      console.log(error);
    });
  }
  applyCalculation() {
    this.cartService.getFirstTimeSignUpCoupon().subscribe(data => {
      this.firstTimeCoupon = data;
      if (this.firstTimeCoupon.length !== 0) {
        this.couponCalculation(this.firstTimeCoupon[0]);
      }
      console.log('first time coupon', data);
    }, error => {
      console.log(error);
    });
  }
  removeCart(item, sku) {
    if (JSON.parse(sessionStorage.getItem('login'))) {
      this.removeServerCart(item);
    } else {
      this.removeLocalCart(sku);
    }
  }
  removeLocalCart(selectedCart) {
    const item = this.shopModel.find(ite => {
      return ite.items.INTsku === selectedCart.INTsku && ite.items.serviceId === selectedCart.serviceId && ite.items.tailoringService === selectedCart.tailoringService;
    });
    const index = this.shopModel.indexOf(item);
    this.shopModel.splice(index, 1);
    sessionStorage.setItem('cart', JSON.stringify(this.shopModel));
    this.shopModel = JSON.parse(sessionStorage.getItem('cart')) || [];
    sessionStorage.setItem('cartqty', JSON.stringify(this.shopModel.length));
    /* this.getDiscount(); */
    this.total();
  }
  removeServerCart(item) {
    this.cartService.deleteToCart(this.userId, item).subscribe(data => {
      this.shopModel = data;
      sessionStorage.setItem('cartqty', JSON.stringify(this.shopModel.length));
      /* this.getDiscount(); */
      this.getIncrementRate();
    }, err => {
      console.log(err);
    });
  }
  total() {
    if (sessionStorage.getItem('coupon')) {
      this.subTotal = 0;
      this.totalItems = 0;
      this.totalAmount = 0;
      const totalProduct: any = this.shopModel.map(item => item.cart_product[0]);
      const totalSet = this.shopModel.map(item => item.items);
      /* this.totalItems += totalSet.length; */
      this.shopModel.forEach(e => {
        this.totalItems += e.items.qty;
      });
      totalSet.map(item => {
        const priceSingle = totalProduct.find(test => test._id === item.productId && test.applied !== true);
        priceSingle.applied = true;
        const priceSizeVariant = priceSingle.child;
        const priceSize = priceSizeVariant.find(check => check.INTsku === item.INTsku);
        this.subTotal += item.qty * priceSize.price;
      });
      totalProduct.forEach(element => {
        element.applied = false;
      });
      this.totalAmount = this.subTotal;
      /* console.log(this.shopModel.length); */
      sessionStorage.setItem('cartqty', JSON.stringify(this.shopModel.length));
      this.checkCouponInExisting();
    } else {
      this.subTotal = 0;
      this.totalItems = 0;
      this.totalAmount = 0;
      const totalProduct: any = this.shopModel.map(item => item.cart_product[0]);
      const totalSet = this.shopModel.map(item => item.items);
      /* this.totalItems += totalSet.length; */
      totalProduct.forEach(element => {
        element.applied = false;
      });
      this.shopModel.forEach(e => {
        this.totalItems += e.items.qty;
      });
      totalSet.map(item => {
        const priceSingle = totalProduct.find(test => test._id === item.productId && test.applied !== true);
        priceSingle.applied = true;
        const priceSizeVariant = priceSingle.child;
        const priceSize = priceSizeVariant.find(check => check.INTsku === item.INTsku);
        this.subTotal += item.qty * priceSize.price;
      });
      totalProduct.forEach(element => {
        element.applied = false;
      });
      this.totalAmount = this.subTotal;
      /* console.log(this.shopModel.length); */
      sessionStorage.setItem('cartqty', JSON.stringify(this.shopModel.length));
    }
    /* this.discountCalculation(); */
    this.getShippingFees();
   
    console.log('final check', this.shopModel);
  }
  getShippingFees() {
    this.cartService.getShippingFees().subscribe(data => {
      const temp = data;
      if (temp.length === 0) {
        if (JSON.parse(sessionStorage.getItem('login'))) {
          this.checkFirstTimeShopping();
        }
      } else {
        this.holder = data[0];
        console.log(data,'fees');
        this.shippingChargeApply();
      }
    }, error => {
      console.log(error);
    });
  }
  shippingChargeApply() {
    if (this.totalAmount > this.holder.minimumPrice) {
      this.totalShipement = 0;
    } else {
      this.totalShipement = this.holder.fees;
    }
    this.totalAmount = this.totalAmount + this.totalShipement;
    if (JSON.parse(sessionStorage.getItem('login'))) {
      this.checkFirstTimeShopping();
    }
   
  }
  getAllSizeWiseIncrement() {
    this.cartService.getAllSizeWiseIncrement().subscribe(data => {
      this.sizInc = data;
      console.log(this.sizInc ,"inc");
      if (this.sizInc.length === 0) {
        this.total();
      } else {
        this.applySizeWiseIncrement();
      }
    }, error => {
      console.log(error);
    });
  }
  applySizeWiseIncrement() {
    console.log('sizeInc', this.sizInc);
    for ( const shop of this.shopModel) {
     for (const product of shop.cart_product[0].child) {
       if (shop.items.INTsku === product.INTsku) {
         if (product.variationType === 'Size') {
           for (const size of this.sizInc) {
             if (size.subCategoryId) {
               if (size.subCategoryId === product.subCategoryId) {
                 if (Number(size.size) <= Number(product.sizeVariant)) {
                  product.sizeIncApply = true;
                  product.sizeIncType = 'Additional charges for plus size';
                  product.sizeWisePrice = (product.subPrice * size.incPercentage) / 100;
                  product.price = product.price + (product.subPrice * size.incPercentage) / 100;
                  product.serviceAmount = product.serviceAmount + (product.subPrice * size.incPercentage) / 100;
                 }
               }
             } else {
              if (size.superCategoryId === product.superCategoryId) {
                if (Number(size.size) <= Number(product.sizeVariant)) {
                  product.sizeIncApply = true;
                  product.sizeIncType = 'Additional charges for plus size';
                  product.sizeWisePrice = (product.subPrice * size.incPercentage) / 100;
                  product.price = product.price + (product.subPrice * size.incPercentage) / 100;
                  product.serviceAmount = product.serviceAmount + (product.subPrice * size.incPercentage) / 100;
                }
              }
             }
           }
         }
       }
     }
    }
    this.total();
  }
  placeOrder() {
    if (JSON.parse(sessionStorage.getItem('login'))) {
      this.checkSoldOutEnable = false;
      this.shopModel.forEach(el => {
        if (el.quantityCheck === 0) {
          this.checkSoldOutEnable = true;
        }
      });
      if (!this.checkSoldOutEnable) {
        console.log(this.checkSoldOutEnable);
        this.router.navigate(['cart/checkout']);
      }
    } else {
      // this.router.navigate(['account/acc/signin']);
      this.openDialog();
    }
  }
  openDialog() {
    const dialogConfig = new MatDialogConfig();
    this.dialog.open(SigninDailogComponent, {
      panelClass: 'c1',

    });
  }
  getwishList() {
    this.wish = new WishList();
    this.wish.userId = this.userId;
    this.cartService.getWishList(this.wish).subscribe(data => {
      this.wishList = data;
      const wishlist: any = this.wishList.map(a => a.productIds);
      sessionStorage.setItem('wislistLength', wishlist.length);
    }, err => {
      console.log(err);
    });
  }
  checkLoginUser(proId, sku) {
    if (JSON.parse(sessionStorage.getItem('login'))) {
      this.userId = sessionStorage.getItem('userId');
      this.moveToWish(proId, sku);
    } else {
      this.router.navigate(['/account/acc/signin']);
    }
  }
  moveToWish(proId, sku) {
    this.wish = new WishList();
    this.wish.userId = this.userId;
    this.wish.productId = proId;
    this.wish.INTsku = sku;
    this.cartService.moveWishlist(this.wish).subscribe(data => {
      this.shopModel = data;
      this.getwishList();
      /* console.log(, 'test');  */
      /* this.getDiscount(); */
      this.total();
    }, err => {
      console.log(err);
    });
  }

  getDiscount() {
    this.cartService.getAllDiscount().subscribe(data => {
      this.discountStore = data;

      this.discountTotal();
    }, error => {
      console.log(error);
    });
  }
  discountTotal() {
    this.subTotal = 0;
    this.totalItems = 0;
    this.totalItems = this.shopModel.map(ele => ele.items).length;
    console.log('discount', this.discountStore);
    for (let l = 0; l <= this.shopModel.length - 1; l++) {
      for (let m = 0; m <= this.shopModel[l].cart_product.length - 1; m++) {
        for (let n = 0; n <= this.shopModel[l].cart_product[m].child.length - 1; n++) {

          if (this.shopModel[l].cart_product[m].child[n].INTsku === this.shopModel[l].items.INTsku) {
            if (this.shopModel[l].cart_product[m].discount === undefined || this.shopModel[l].cart_product[m].discount === 0) {
              this.shopModel[l].cart_product[m].child[n].discount = 0;
              this.checkPoint1 = true;
              this.shopModel[l].cart_product[m].displayClass = 'discountNone';
              this.shopModel[l].cart_product[m].child[n].displayClass = 'discountNone';
            }
            for (let i = 0; i <= this.discountStore.length - 1; i++) {
              for (let j = 0; j <= this.discountStore[i].conditions.length - 1; j++) {
                for (let k = 0; k <= this.discountStore[i].conditions[j].value.length - 1; k++) {
                  /* -------------------------------------------------------------------------------------------------------- */
                  if (this.discountStore[i].conditions[j].field === 'Product Name') {
                    if (this.shopModel[l].cart_product[m]._id === this.discountStore[i].conditions[j].value[k]) {
                      if (this.shopModel[l].cart_product[m].child[n].displayClass === 'discountStyle') {
                        if (this.discountStore[i].amountType === 'Flat') {
                          const temp = this.shopModel[l].cart_product[m].child[n].price - this.discountStore[i].typeValue;
                          if (this.shopModel[l].cart_product[m].child[n].discount > temp) {
                            this.shopModel[l].cart_product[m].child[n].discount = temp;
                            this.shopModel[l].cart_product[m].displayClass = 'discountStyle';
                            this.shopModel[l].cart_product[m].child[n].displayClass = 'discountStyle';
                            this.checkPoint4 = true;
                            console.log('check point 8', this.shopModel[l].cart_product[m].child[n].discount);
                          } else {
                            continue;
                          }
                        } else if (this.discountStore[i].amountType === 'Percentage') {
                          const temp = this.shopModel[l].cart_product[m].child[n].price - Math.round(this.shopModel[l].cart_product[m].child[n].price / 100 * this.discountStore[i].typeValue);
                          if (this.shopModel[l].cart_product[m].child[n].discount > temp) {
                            this.shopModel[l].cart_product[m].child[n].discount = temp;
                            this.shopModel[l].cart_product[m].displayClass = 'discountStyle';
                            this.shopModel[l].cart_product[m].child[n].displayClass = 'discountStyle';
                            this.checkPoint5 = true;
                            console.log('check point 6', this.shopModel[l].cart_product[m].child[n].discount);
                          } else {
                            continue;
                          }
                        }
                      } else {
                        if (this.discountStore[i].amountType === 'Percentage') {
                          this.shopModel[l].cart_product[m].child[n].displayClass = 'discountStyle';
                          this.shopModel[l].cart_product[m].displayClass = 'discountStyle';
                          this.checkPoint2 = true;
                          this.shopModel[l].cart_product[m].child[n].discount = this.shopModel[l].cart_product[m].child[n].price - Math.round(this.shopModel[l].cart_product[m].child[n].price / 100 * this.discountStore[i].typeValue);
                          console.log('check point 1', this.shopModel[l].cart_product[m].child[n].discount);
                        } else {
                          this.shopModel[l].cart_product[m].displayClass = 'discountStyle';
                          this.shopModel[l].cart_product[m].child[n].displayClass = 'discountStyle';
                          this.checkPoint3 = true;
                          this.shopModel[l].cart_product[m].child[n].discount = this.shopModel[l].cart_product[m].child[n].price - this.discountStore[i].typeValue;
                          console.log('check point 2', this.shopModel[l].cart_product[m].child[n].discount);
                        }
                      }
                    } else {
                      continue;
                    }
                  } else if (this.discountStore[i].conditions[j].field === 'Product Category') {
                    if (this.shopModel[l].cart_product[m].superCategoryId === this.discountStore[i].conditions[j].value[k]) {
                      if (this.shopModel[l].cart_product[m].child[n].displayClass === 'discountStyle') {
                        if (this.discountStore[i].amountType === 'Flat') {
                          const temp = this.shopModel[l].cart_product[m].child[n].price - this.discountStore[i].typeValue;
                          if (this.shopModel[l].cart_product[m].child[n].discount > temp) {
                            this.shopModel[l].cart_product[m].child[n].discount = temp;
                            this.shopModel[l].cart_product[m].displayClass = 'discountStyle';
                            this.shopModel[l].cart_product[m].child[n].displayClass = 'discountStyle';
                            this.checkPoint8 = true;
                            console.log('check point 4', this.shopModel[l].cart_product[m].child[n].discount);
                          } else {
                            continue;
                          }
                        } else if (this.discountStore[i].amountType === 'Percentage') {
                          const temp = this.shopModel[l].cart_product[m].child[n].price - Math.round(this.shopModel[l].cart_product[m].child[n].price / 100 * this.discountStore[i].typeValue);
                          if (this.shopModel[l].cart_product[m].child[n].discount > temp) {
                            this.shopModel[l].cart_product[m].child[n].discount = temp;
                            this.shopModel[l].cart_product[m].displayClass = 'discountStyle';
                            this.shopModel[l].cart_product[m].child[n].displayClass = 'discountStyle';
                            this.checkPoint9 = true;
                            console.log('check point 3', this.shopModel[l].cart_product[m].child[n].discount);
                          } else {
                            continue;
                          }
                        }
                      } else {
                        if (this.discountStore[i].amountType === 'Percentage') {
                          this.shopModel[l].cart_product[m].child[n].displayClass = 'discountStyle';
                          this.shopModel[l].cart_product[m].displayClass = 'discountStyle';
                          this.checkPoint6 = true;
                          this.shopModel[l].cart_product[m].child[n].discount = this.shopModel[l].cart_product[m].child[n].price - Math.round(this.shopModel[l].cart_product[m].child[n].price / 100 * this.discountStore[i].typeValue);
                          console.log('check point 1', this.shopModel[l].cart_product[m].child[n].discount);
                        } else {
                          this.shopModel[l].cart_product[m].displayClass = 'discountStyle';
                          this.shopModel[l].cart_product[m].child[n].displayClass = 'discountStyle';
                          this.checkPoint7 = true;
                          this.shopModel[l].cart_product[m].child[n].discount = this.shopModel[l].cart_product[m].child[n].price - this.discountStore[i].typeValue;
                          console.log('check point 2', this.shopModel[l].cart_product[m].child[n].discount);
                        }
                      }
                    }
                  }

                  /* ----------------------------------------------------------------------------------------------- */
                }
              }
            }
            if (this.checkPoint1 === true || this.checkPoint2 === true || this.checkPoint3 === true ||
              this.checkPoint6 === true || this.checkPoint7 === true) {
              console.log('check point 3', this.shopModel[l].cart_product[m].child[n].discount === 0 ? this.shopModel[l].cart_product[m].child[n].price * this.shopModel[l].items.qty : this.shopModel[l].cart_product[m].child[n].discount * this.shopModel[l].items.qty);
              this.subTotal += this.shopModel[l].cart_product[m].child[n].discount === 0 ? this.shopModel[l].cart_product[m].child[n].price * this.shopModel[l].items.qty : this.shopModel[l].cart_product[m].child[n].discount * this.shopModel[l].items.qty;
              this.checkPoint1 = false;
              this.checkPoint2 = false;
              this.checkPoint3 = false;
            }
          }


        }
      }
    }
    console.log('subTotal', this.subTotal);
    console.log('totalItem', this.totalItems);
    console.log('updated', this.shopModel);
  }
  /* getFirstTimeSignUpCoupon() {
    this.cartService.getFirstTimeSignUpCoupon().subscribe(data => {
      this.firstTimeCoupon = data;
      console.log('first time coupon', data);
    }, error => {
      console.log(error);
    });
  } */
  getFlexibleCoupon() {
    this.cartService.getFlexibleCoupon().subscribe(data => {
      this.flexibleCoupon = data;
      console.log('flexible coupon', data);
    }, error => {
      console.log(error);
    });
  }
  checkLogInForCoupon(coupon) {
    if (JSON.parse(sessionStorage.getItem('login'))) {
      this.checkFlexibleCoupon(coupon);
    } else {
      this.openDialog();
    }
  }
  checkFirstTimeCoupon(coupon) {
    if (this.firstTimeCoupon.length !== 0) {
      this.isFirstTimeCoupon = false;
      this.firstTimeCoupon.forEach(a => {
        if (a.couponCode === coupon.couponCode) {
          this.isFirstTimeCoupon = true;
          this.checkFirstTimeCouponValidate(a, coupon);
        }
        if (!this.isFirstTimeCoupon) {
          this.checkFlexibleCoupon(coupon);
        }
      });
    } else {
      this.checkFlexibleCoupon(coupon);
    }
  }
  checkFirstTimeCouponValidate(firstCoupon, coupon) {
    firstCoupon.conditions.forEach(element => {
      if (element.field === 'Order total') {
        if (element.operator === 'equal') {
          if (element.value[0] === this.subTotal.toString()) {
            this.checkFirstTimeSignupCouponByCustomer(firstCoupon, coupon);
          } else {
            this.showInValidateCoupon();
          }
        } else if (element.operator === 'greater than') {
          if (element.value[0] <= this.subTotal.toString()) {
            this.checkFirstTimeSignupCouponByCustomer(firstCoupon, coupon);
          } else {
            this.showInValidateCoupon();
          }
        } else if (element.operator === 'less than') {
          if (element.value[0] >= this.subTotal.toString()) {
            this.checkFirstTimeSignupCouponByCustomer(firstCoupon, coupon);
          } else {
            this.showInValidateCoupon();
          }
        }
      } else if (element.field === 'Order quantity') {
        if (element.operator === 'equal') {
          if (element.value[0] === this.totalItems.toString()) {
            this.checkFirstTimeSignupCouponByCustomer(firstCoupon, coupon);
          } else {
            this.showInValidateCoupon();
          }
        } else if (element.operator === 'greater than') {
          if (element.value[0] <= this.totalItems.toString()) {
            this.checkFirstTimeSignupCouponByCustomer(firstCoupon, coupon);
          } else {
            this.showInValidateCoupon();
          }
        } else if (element.operator === 'less than') {
          if (element.value[0] >= this.totalItems.toString()) {
            this.checkFirstTimeSignupCouponByCustomer(firstCoupon, coupon);
          } else {
            this.showInValidateCoupon();
          }
        }
      }
    });
  }
  checkFlexibleCoupon(coupon) {
    if (this.flexibleCoupon.length !== 0) {
      this.isFlexibleCoupon = false;
      this.flexibleCoupon.forEach(a => {
        if (a.couponCode === coupon.couponCode) {
          this.isFlexibleCoupon = true;
          this.checkFlexibleCouponValidation(a, coupon);
        }
      });
      if (!this.isFlexibleCoupon) {
        this.showInValidateCoupon();
      }
    } else {
      this.showInValidateCoupon();
    }
  }
  checkFirstTimeSignupCouponByCustomer(firstCoupon, coupon) {
    const userId = sessionStorage.getItem('userId');
    this.cartService.checkCouponForFirstCustomer(userId).subscribe(data => {
      console.log(data);
      if (data.length === 0) {
        this.couponCalculation(firstCoupon);
      } else {
        this.showInValidateCoupon();
      }
    }, error => {
      console.log(error);
    });
  }
  showInValidateCoupon() {
    sessionStorage.removeItem('coupon');
    this.totalAmount = this.subTotal;
    this.couponType = '';
    this.message = 'Invalid Coupon Code';
    this.matSnackBar.open(this.message, this.action, {
      duration: 1000,
      verticalPosition: 'top',
      panelClass: ['snackbar1']
    });
  }
  checkFlexibleCouponValidation(flexiCoupon, coupon) {
    flexiCoupon.conditions.forEach(element => {
      if (element.field === 'Order total') {
        if (element.operator === 'equal') {
          if (element.value[0] === this.subTotal.toString()) {
            this.checkFlexibleCouponByCustomer(flexiCoupon, coupon);
          } else {
            this.showInValidateCoupon();
          }
        } else if (element.operator === 'greater than') {
          if (element.value[0] <= this.subTotal.toString()) {
            this.checkFlexibleCouponByCustomer(flexiCoupon, coupon);
          } else {
            this.showInValidateCoupon();
          }
        } else if (element.operator === 'less than') {
          if (element.value[0] >= this.subTotal.toString()) {
            this.checkFlexibleCouponByCustomer(flexiCoupon, coupon);
          } else {
            this.showInValidateCoupon();
          }
        }
      } else if (element.field === 'Order quantity') {
        if (element.operator === 'equal') {
          if (element.value[0] === this.totalItems.toString()) {
            this.checkFlexibleCouponByCustomer(flexiCoupon, coupon);
          } else {
            this.showInValidateCoupon();
          }
        } else if (element.operator === 'greater than') {
          if (element.value[0] <= this.totalItems.toString()) {
            this.checkFlexibleCouponByCustomer(flexiCoupon, coupon);
          } else {
            this.showInValidateCoupon();
          }
        } else if (element.operator === 'less than') {
          if (element.value[0] >= this.totalItems.toString()) {
            this.checkFlexibleCouponByCustomer(flexiCoupon, coupon);
          } else {
            this.showInValidateCoupon();
          }
        }
      }
    });
  }
  checkFlexibleCouponByCustomer(flexiCoupon, coupon) {
    const userId = sessionStorage.getItem('userId');
    this.customerData = {
      customerId: userId,
      couponCode: coupon.couponCode
    };
    this.cartService.checkCouponOnCustomer(this.customerData).subscribe(data => {
      if (data.length === 0) {
        this.couponCalculation(flexiCoupon);
      } else {
        this.showInValidateCoupon();
      }
    }, error => {
      console.log(error);
    });
  }
  couponCalculation(selectedCoupon) {
    this.totalAmount = this.subTotal;
    if (selectedCoupon.amountType === 'Flat') {
      this.couponType = 'Flat';
      this.couponValue = selectedCoupon.typeValue;
      this.totalAmount = this.totalAmount - selectedCoupon.typeValue;
    } else if (selectedCoupon.amountType === 'Percentage') {
      this.couponType = 'Percentage';
      this.couponValue = selectedCoupon.typeValue;
      this.totalAmount = this.totalAmount - Math.round(this.totalAmount / 100 * selectedCoupon.typeValue);
    }
    if (!sessionStorage.getItem('coupon')) {
      this.showAppliedCoupon(selectedCoupon);
    }
  }
  showAppliedCoupon(selectedCoupon) {
    sessionStorage.setItem('coupon', selectedCoupon._id);
    this.message = 'Coupon Applied Successfully';
    this.matSnackBar.open(this.message, this.action, {
      duration: 1000,
      verticalPosition: 'top',
      panelClass: ['snackbar1']
    });
  }
  removeCoupon(subTotal) {
    this.totalAmount = subTotal;
    this.couponType = '';
    sessionStorage.removeItem('coupon');
  }
  checkCouponInExisting() {
    const couponId = sessionStorage.getItem('coupon');
    this.cartService.checkCouponForPlaceOrder(couponId).subscribe(data => {
      this.checkFirstTimeCoupon(data[0]);
    }, error => {
      console.log(error);
    });
  }
}
