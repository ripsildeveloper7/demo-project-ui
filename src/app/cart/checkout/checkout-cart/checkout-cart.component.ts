import { Component, OnInit, Input, Output,  EventEmitter } from '@angular/core';
import { Cart } from './../../../shared/model/cart.model';
import { Router } from '@angular/router';
import { AppSetting } from '../../../config/appSetting';
import { MatSnackBar } from '@angular/material';


@Component({
  selector: 'app-checkout-cart',
  templateUrl: './checkout-cart.component.html',
  styleUrls: ['./checkout-cart.component.css']
})
export class CheckoutCartComponent implements OnInit {
@Input() shopModel: any;
@Input() totalItems: any;
@Input() subTotal: any;
@Input() addressSelected: any;
@Input() totalAmount: any;
@Input() couponType: any;
@Input() couponValue: any;
@Input() totalShipement: any;
@Output() addPlus = new EventEmitter<Cart>();
@Output() minusPlus = new EventEmitter<Cart>();
@Output() deleteCart = new EventEmitter<Cart>();
  cartModel: Cart;
  userId;
  
  action;
  
  productImageUrl: string = AppSetting.productImageUrl;
  displayMobile = false;
  constructor() {
    if (window.screen.width > 900) {
      this.displayMobile = false;
    } else {
      this.displayMobile = true;
    }
   }

  ngOnInit() {
  }
  actionPlusData(product, skuCode)   {
    const totalItem: any = [];
    const cart: any = {
      productId: product,
      sku: skuCode,
      qty: 1
      };
    totalItem.push(cart);
    this.addPlus.emit(totalItem);
  }

  ngAfterViewInit() {
    this.getWindowSize();
  }
  getWindowSize() {
    if (window.screen.width > 900) {
      this.displayMobile = false;
    } else {
      this.displayMobile = true;
    }
  }

  actionMinusData(product, skuCode) {
    const totalItem: any = [];
    const cart: any = {
      productId: product,
      sku: skuCode,
      qty: 1
      };
    this.cartModel = new Cart();
    this.cartModel.userId = this.userId;
    this.cartModel.items = cart;
    this.minusPlus.emit(cart);
  }
  removeCartData(item) {
    this.deleteCart.emit(item);
  }

}
