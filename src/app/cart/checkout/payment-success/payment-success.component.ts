import { Component, OnInit } from '@angular/core';
import { CartService } from './../../cart.service';
import { AddressModel } from './../../../shared/model/address.model';
import { RegModel } from './../../../shared/model/registration.model';
import { Order } from './../../../shared/model/order.model';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-payment-success',
  templateUrl: './payment-success.component.html',
  styleUrls: ['./payment-success.component.css']
})
export class PaymentSuccessComponent implements OnInit {
  userId: string;
  shopModel: any = [];
  addressModel: AddressModel[];
  regModel: RegModel;
  addressSelected;
  subTotal: number;
  totalItems: number;
  orderModel: Order;
  orderId;
  paypalOrderId;

  constructor(private cartService: CartService, private router: Router,  private route: ActivatedRoute) { }

  ngOnInit() {
    this.paypalOrderId = this.route.snapshot.params.id;
    if (JSON.parse(sessionStorage.getItem('login'))) {
      this.userId = sessionStorage.getItem('userId');
      this.getCustomerDetails();
      this.shoppingCartUser(this.userId);
      /* this.paypalApply(); */
    } else {
      this.shopModel = JSON.parse(sessionStorage.getItem('cart')) || [];
    }
  }
  selectedAddress(event) {
    if (event) {
      this.addressSelected = event;
    }
  }
  getCustomerDetails() {
    this.cartService.getCustomerDetails(this.userId).subscribe(data => {
      this.regModel = data;
      this.addressModel = data.addressDetails;
      this.addressSelected = this.addressModel[0];
    }, error => {
      console.log(error);
    });
  }
  shoppingCartUser(userId) {
    this.cartService.shoppingUser(userId).subscribe(data => {
      this.shopModel = data;
      console.log('cart', this.shopModel);
      /* this.getDiscount(); */
      this.confirmOrderData();
      this.total();
    }, err => {
      console.log(err);
    });
  }
  confirmOrderData() {
  this.addressSelected = JSON.parse(sessionStorage.getItem('selected address')) ;
  this.total();
    const totalItem = this.shopModel.map(element => element.items);
    const orderedProducts = this.shopModel.map(element => element.cart_product);
    this.orderModel = new Order();
    this.orderModel.customerId = this.userId;
    this.orderModel.addressDetails = this.addressSelected;
    this.orderModel.total = this.subTotal;
    this.orderModel.paypalOrderId = this.paypalOrderId;
    this.orderModel.cart = totalItem;
    this.orderModel.paymentMode = 'paypal';
    this.orderModel.orderedProducts = orderedProducts;
    this.cartService.confirmOrder(this.orderModel).subscribe(data => {
      this.orderModel = data;
      this.orderId = data._id;
      /* sessionStorage.removeItem('selected address'); */
      this.deleteCart(this.userId);
      this.qtyUpdate(this.orderModel);
    }, err => {
      console.log(err);
    });
  }
  total() {
    this.subTotal = 0;
    this.totalItems = 0;
    const totalProduct: any = this.shopModel.map(item => item.cart_product[0]);
    const totalSet = this.shopModel.map(item => item.items);
    this.totalItems += totalSet.length;
    totalSet.map(item => {
      const priceSingle = totalProduct.find(test => test._id === item.productId);
      const priceSizeVariant = priceSingle.child;
      const priceSize = priceSizeVariant.find(check => check.INTsku === item.INTsku);
      this.subTotal += item.qty * priceSize.price;
    });
    sessionStorage.setItem('cartqty', JSON.stringify(this.shopModel.length));
  }
  deleteCart(userId) {
    this.cartService.deleteAllCart(userId).subscribe(data => {
      this.shopModel = data;
      sessionStorage.setItem('cartqty', JSON.stringify(this.shopModel.length));
      /* this.router.navigate(['/account/orders']); */
    }, error => {
      console.log(error);
    });
  }
  qtyUpdate(_order) {
    /*   this.accountService.confirmQtyOrder(order).subscribe(data => {
        this.shopModel = data;
      }, error => {
        console.log(error);
      }); */
  }
}
