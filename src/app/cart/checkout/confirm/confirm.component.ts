import { Component, OnInit, Input, EventEmitter, Output, NgZone, AfterViewInit } from '@angular/core';
import { ConverterPipe } from './../../../shared/nav/converter.pipe';
import {Order} from '../../../shared/model/order.model';
/* import { Order } from './../../../shared/model/order.model'; */
import {  ViewChild, ElementRef} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
declare var paypal;
import { WindowRefService } from './../window-ref.service';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.css']
})
export class ConfirmComponent implements OnInit, AfterViewInit {
  @ViewChild('paypal', {static: true}) paypalElement: ElementRef;
  @ViewChild('text', {static: true}) name1: any;
  @Input() totalItems: number;
  @Input() subTotal: number;
  @Input() totalAmount: number;
  @Input()  addressSelected: any;
  @Output() confirmDetails =  new EventEmitter<any>();
  @Output() paymentType =  new EventEmitter<any>();
  @Output() paymentModel = new EventEmitter<Order>();
  @Output() messageEvent = new EventEmitter<string>();
  paypalDetails;
  orderModelId: any;
  paymentMethod = ['Paypal',  'Razorpay'];
  isDisabled = true;
  progress: number = 0;
  label: string;
  userId;
  displayMobile = false;
  enablePayment: boolean;
  constructor( private converterPipe: ConverterPipe, private router: Router, private winRef: WindowRefService, private _ngZone: NgZone) { 
    
    console.log('constructor total amt', this.totalAmount)
    if (window.screen.width > 900) {
      this.displayMobile = false;
    } else {
      this.displayMobile = true;
    }
  }

  ngOnInit() {
    /* this.paypalApply(); */
    this.userId = sessionStorage.getItem('userId');
  }

  ngAfterViewInit() {
    this.getWindowSize();
  }
  getWindowSize() {
    if (window.screen.width > 900) {
      this.displayMobile = false;
    } else {
      this.displayMobile = true;
    }
  }
  orderConfirmDetails(addressSelected)   {
    this.confirmDetails.emit(addressSelected);
  }
  selectedPayment(i) {
    this.paymentType.emit(i);
  }
  payNow() {
    this.paypalApply();
    this.enablePayment = true;
    
  }
  paypalApply() {
    const sub = this.converterPipe.transform(this.totalAmount);
    console.log('sub value', sub);
    console.log('total value in confirm', this.totalAmount);
    paypal.Buttons({
       createOrder: (data, actions) => {
         return actions.order.create({
           purchase_units: [{
             description: 'Sample',
             amount: {
               currency_code: 'USD',
               value: sub
             }
           }]
         }).then(
         console.log(actions, 'actions'));
       },
       onApprove: (data, approveactions) => {
        return approveactions.order.capture().then(function(details) {
          console.log('paypal details', details);
    window.location = <any>'https://www.ucchalfashion.com/cart/orderid/'+ details.id  ;
        });
      }
     }).render(this.paypalElement.nativeElement);
  }
  payRazorPay() {
    const sub = this.converterPipe.transform(this.subTotal);
    this.messageEvent.emit(sub);
  }
 }




